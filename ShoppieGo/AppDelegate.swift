//
//  AppDelegate.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 02. 14..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//  original bundle =  "com.marquard.shoppiego"

import UIKit
import GoogleMaps
import Firebase
import FirebaseMessaging
import Photos
import UserNotifications
import FBSDKCoreKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        var _ = CouponGoHelper.sharedInstance
        var _ = DataManager.sharedInstance
        GMSServices.provideAPIKey("AIzaSyDANkWefcdmLh5RUJMjOwpzCcXW4v7e1bE")
        
        checkLocationServicePermission()
        checkRemoteNotificationPermission(application)
        
        application.setMinimumBackgroundFetchInterval(1800)

        FirebaseApp.configure()
        
        Messaging.messaging().delegate = self
        UNUserNotificationCenter.current().delegate = self
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let handled = ApplicationDelegate.shared.application(app, open: url, options: options)
        
        // Add any custom logic here.
        
        return handled
    }
    
    private func checkPermissions(){
        
    }
    
    private func checkRemoteNotificationPermission(_ application: UIApplication){
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
    }
    
    private func checkLocationServicePermission(){
        if CLLocationManager.locationServicesEnabled() {
            CouponGoHelper.sharedInstance.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            if CLLocationManager.authorizationStatus() == .notDetermined {
                CouponGoHelper.sharedInstance.locationManager.requestAlwaysAuthorization()
            }
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print(String(format: "fcmToken: %@", fcmToken))
        
        let storedToken = DataManager.sharedInstance.getSessionString(Constants.SESSION_FCM_TOKEN)
        if storedToken != fcmToken {
            DataManager.sharedInstance.setSession(Constants.SESSION_FCM_TOKEN, value: fcmToken)
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        
        let _ = NotificationHandler.processUserinfo(appState: application.applicationState, userInfo: userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // IDE JÖN A NOTIFICATION=0 üzenet, ha előtérben van az app és akkor is, ha háttérben
        
        let _ = NotificationHandler.processUserinfo(appState: application.applicationState, userInfo: userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        // IDE ESIK BE A CHAT ÜZENET, HA ELŐTÉRBEN VAN AZ APP
        
        let userInfo = notification.request.content.userInfo
        
        let ret = NotificationHandler.processUserinfo(appState: .active, userInfo: userInfo)
        var arr: UNNotificationPresentationOptions = []
        if ret is UNNotificationPresentationOptions {
            arr = ret as! UNNotificationPresentationOptions
        }
        completionHandler( arr )
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        // IDE ESIK BE, HA NOTIFICATION-RÓL NYITOM MEG
        
        let userInfo = response.notification.request.content.userInfo
        let dict = userInfo as NSDictionary
        CouponGoHelper.sharedInstance.launchParameters = dict
        
        completionHandler()
    }
}

