//
//  NotificationHandler.swift
//  ShoppieGo
//
//  Created by Kaizer Jeno Kornel on 2019. 04. 20..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import Foundation
import UserNotifications
import AVFoundation

protocol CGNotificationDelegate {
    func notificationReceived(notification: CGNotification)
}

protocol CGChatDelegate{
    func chatMessageReceived(message: CGChatMessage)
    func chatBoxesReceived(userId: Int, ownTradeItems: CGTradeItems?, partnerTradeItems: CGTradeItems?)
}

class DropLocation{
    var dropId = 0
    var lat = 0.0
    var lng = 0.0
}

class NotificationHandler{
    
    static var notificationDelegate: CGNotificationDelegate?
    static var chatDelegate: CGChatDelegate?
    
    private static func processNotification(){
        
    }
    
    private static func getClosestLocationIndex(_ dict: NSDictionary) -> DropLocation?{
        guard let coordLat = DataManager.sharedInstance.getSessionDouble(Constants.SESSION_LAST_LOCATION_LAT),
            let coordLng = DataManager.sharedInstance.getSessionDouble(Constants.SESSION_LAST_LOCATION_LNG) else { return nil }
        
        let usrLoc = CLLocationCoordinate2D(latitude: coordLat, longitude: coordLng)
        
        let radius = CouponGoHelper.sharedInstance.jsonToInt(dict.value(forKey: "radius")) * 1000
        var dropLocations: Array<DropLocation> = []
        
        let drop_type = CouponGoHelper.sharedInstance.jsonToString(dict.value(forKey: "drop_type"))
        if "single" == drop_type {
            let loc = DropLocation()
            loc.dropId = CouponGoHelper.sharedInstance.jsonToInt(dict.value(forKey: "drop_id"))
            loc.lat = CouponGoHelper.sharedInstance.jsonToDouble(dict.value(forKey: "lat"))
            loc.lng = CouponGoHelper.sharedInstance.jsonToDouble(dict.value(forKey: "lng"))
            dropLocations.append(loc)
        }
        else if "more" == drop_type {
            if let drops = dict.value(forKey: "drops") as? NSArray {
                for drop in drops {
                    let obj = drop as! NSDictionary
                    let loc = DropLocation()
                    loc.dropId = CouponGoHelper.sharedInstance.jsonToInt(obj.value(forKey: "drop_id"))
                    loc.lat = CouponGoHelper.sharedInstance.jsonToDouble(obj.value(forKey: "lat"))
                    loc.lng = CouponGoHelper.sharedInstance.jsonToDouble(obj.value(forKey: "lng"))
                    dropLocations.append(loc)
                }
            }
        }
        
        var minDistance = Double(Int.max)
        var closestDropLocation: DropLocation? = nil
        
        for dropLocation in dropLocations {
            let distance = CouponGoHelper.sharedInstance.getDistance(from: CLLocationCoordinate2D(latitude: dropLocation.lat, longitude: dropLocation.lng), to: usrLoc)
            if distance < Double(radius) && distance < minDistance {
                minDistance = distance
                closestDropLocation = dropLocation
            }
        }
        
        return closestDropLocation
    }
    
    static func doVibrate(){
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
    }
    
    static func buildNotification(title: String, body: String, userInfo: [AnyHashable: Any]? = nil, appState: UIApplication.State){
        var playSound = DataManager.sharedInstance.getAppBool(Constants.APP_SOUNDEFFECT)
        var doVibration = DataManager.sharedInstance.getAppBool(Constants.APP_VIBRATION)
        
        if appState == .active {
            playSound = false
            doVibration = false
        }
        
        let content = UNMutableNotificationContent()
        
        //adding title, subtitle, body and badge
        content.title = title
        content.subtitle = ""
        content.body = body
        content.badge = 0
        content.sound = playSound ? .default : .none
        if userInfo != nil {
            content.userInfo = userInfo!
        }
        
        if doVibration {
            doVibrate()
        }
        
        //getting the notification trigger
        //it will be called after 5 seconds
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        
        //getting the notification request
        let request = UNNotificationRequest(identifier: "ShoppieGoIOSNotification", content: content, trigger: trigger)
        
        //adding the notification to notification center
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
    static func processUserinfo(appState: UIApplication.State, userInfo: [AnyHashable: Any]) -> Any?{
        let receiveNotifications = DataManager.sharedInstance.getAppBool(Constants.APP_PUSHNOTIFICATION)
        
        let dict = userInfo as NSDictionary
        let notiType = CouponGoHelper.sharedInstance.jsonToString(dict.value(forKey: "type"))
        switch notiType {
        case "drop_notification":
            guard let closestDropLocation = getClosestLocationIndex(dict) else { return nil }
            
            let notification = CGNotification()
            notification.userId = DataManager.sharedInstance.getSessionInt(Constants.SESSION_USER_ID) ?? 0
            notification.type = notiType
            notification.title = CouponGoHelper.sharedInstance.jsonToString(dict.value(forKey: "title"))
            notification.body = CouponGoHelper.sharedInstance.jsonToString(dict.value(forKey: "body"))
            notification.icon = CouponGoHelper.sharedInstance.jsonToString(dict.value(forKey: "icon"))
            notification.date = CouponGoHelper.sharedInstance.jsonToDateTime(dict.value(forKey: "sent_at"))
            notification.campaignId = CouponGoHelper.sharedInstance.jsonToInt(dict.value(forKey: "campaign_id"))
            notification.dropId = closestDropLocation.dropId
            notification.lat = closestDropLocation.lat
            notification.lng = closestDropLocation.lng
            DataManager.sharedInstance.addNotificationDrop(notification)
            
            if receiveNotifications {
                let ui = ["type" : "cg_drop",
                          "lat" : String(format: "%f", closestDropLocation.lat),
                          "lng" : String(format: "%f", closestDropLocation.lng)]
                buildNotification(title: notification.title, body: notification.body, userInfo: ui, appState: appState)
            }
        case "dm_notification":
            let notification = CGNotification()
            notification.type = notiType
            notification.title = CouponGoHelper.sharedInstance.jsonToString(dict.value(forKey: "title"))
            notification.body = CouponGoHelper.sharedInstance.jsonToString(dict.value(forKey: "body"))
            notification.icon = CouponGoHelper.sharedInstance.jsonToString(dict.value(forKey: "icon"))
            notification.date = CouponGoHelper.sharedInstance.jsonToDateTime(dict.value(forKey: "date"))
            notification.url = CouponGoHelper.sharedInstance.jsonToString(dict.value(forKey: "url"))
            DataManager.sharedInstance.addNotificationDM(notification)
            
            if receiveNotifications {
                buildNotification(title: notification.title, body: notification.body, appState: appState)
            }
        case "chat_message":
            let currentUserId = DataManager.sharedInstance.getSessionInt(Constants.SESSION_USER_ID)
            let targetUserId = CouponGoHelper.sharedInstance.jsonToInt(dict.value(forKey: "target_user_id"))
            if targetUserId == 0 || currentUserId != targetUserId {
                return nil
            }
            
            let msg = CGChatMessage()
            msg.userId = CouponGoHelper.sharedInstance.jsonToInt(dict.value(forKey: "user_id"))
            msg.profilePictureUrl = CouponGoHelper.sharedInstance.jsonToString(dict.value(forKey: "profile_picture_url"))
            msg.nickname = CouponGoHelper.sharedInstance.jsonToString(dict.value(forKey: "nickname"))
            msg.message = CouponGoHelper.sharedInstance.jsonToString(dict.value(forKey: "message"))
            msg.isSystem = CouponGoHelper.sharedInstance.jsonToBool(dict.value(forKey: "system"))
            
            chatDelegate?.chatMessageReceived(message: msg)
        case "chat_boxes":
            let currentUserId = DataManager.sharedInstance.getSessionInt(Constants.SESSION_USER_ID)
            let targetUserId = CouponGoHelper.sharedInstance.jsonToInt(dict.value(forKey: "target_user_id"))
            if targetUserId == 0 || currentUserId != targetUserId {
                return nil
            }
            
            if let boxesData = CouponGoHelper.sharedInstance.jsonToString(dict.value(forKey: "boxes")).data(using: .utf8) {
                let userId = CouponGoHelper.sharedInstance.jsonToInt(dict.value(forKey: "user_id"))
                do {
                    guard let boxes = try JSONSerialization.jsonObject(with: boxesData, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary else { return nil }
                    let ownTradeItems = CouponGoHelper.sharedInstance.getTradeItems(boxes.value(forKey: "own"))
                    let partnerTradeItems = CouponGoHelper.sharedInstance.getTradeItems(boxes.value(forKey: "partner"))
                    
                    chatDelegate?.chatBoxesReceived(userId: userId, ownTradeItems: ownTradeItems, partnerTradeItems: partnerTradeItems)
                }
                catch {
                    
                }
            }
        case "cg_drop":
            if appState == .active {
                let playSound = DataManager.sharedInstance.getAppBool(Constants.APP_SOUNDEFFECT)
                let doVibration = DataManager.sharedInstance.getAppBool(Constants.APP_VIBRATION)
                
                var retVal: UNNotificationPresentationOptions = [.alert]
                if playSound {
                    retVal.insert(.sound)
                }
                if doVibration {
                    doVibrate()
                }
                
                return retVal
            }
        default:
            print("semmi")
        }
        
        return nil
    }
}
