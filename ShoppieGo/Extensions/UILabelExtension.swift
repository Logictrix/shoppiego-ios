//
//  UILabelExtension.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 02. 25..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

extension UILabel{
    func setCorrectLabelFrameHeight(){
        /*let attributedText : NSAttributedString = NSAttributedString.init(string: self.text!, attributes: [NSAttributedString.Key.font: self.font!]);
        let rect : CGRect = attributedText.boundingRect(with: CGSize.init(width: self.frame.size.width, height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil);
        var newFrame : CGRect = self.frame;
        newFrame.size.height = rect.size.height;
        self.frame = newFrame;*/
        self.numberOfLines = 0
        let maximumLabelSize: CGSize = CGSize(width: self.frame.width, height: 9999)
        let expectedLabelSize: CGSize = self.sizeThatFits(maximumLabelSize)
        var newFrame: CGRect = self.frame
        newFrame.size.height = expectedLabelSize.height
        self.frame = newFrame
    }
    
    func setLineSpacing(lineSpacing: CGFloat = 0.0, lineHeightMultiple: CGFloat = 0.0) {
        
        guard let labelText = self.text else { return }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.lineHeightMultiple = lineHeightMultiple
        
        let attributedString:NSMutableAttributedString
        if let labelattributedText = self.attributedText {
            attributedString = NSMutableAttributedString(attributedString: labelattributedText)
        } else {
            attributedString = NSMutableAttributedString(string: labelText)
        }
        
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        
        self.attributedText = attributedString
    }
    
    func setCorrectLabelFrameHeightWithWebView(hight:CGFloat){
        /*let attributedText : NSAttributedString = NSAttributedString.init(string: self.text!, attributes: [NSAttributedString.Key.font: self.font!]);
         let rect : CGRect = attributedText.boundingRect(with: CGSize.init(width: self.frame.size.width, height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil);
         var newFrame : CGRect = self.frame;
         newFrame.size.height = rect.size.height;
         self.frame = newFrame;*/
        self.numberOfLines = 0
        let maximumLabelSize: CGSize = CGSize(width: self.frame.width, height: 9999)
        let expectedLabelSize: CGSize = self.sizeThatFits(maximumLabelSize)
        var newFrame: CGRect = self.frame
        newFrame.size.height = hight
        self.frame = newFrame
    }
}
