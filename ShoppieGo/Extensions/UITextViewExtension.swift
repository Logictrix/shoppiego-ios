//
//  UITextViewExtension.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 02. 20..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

extension UITextView {
    func hyperlink(str: String, args: CVarArg..., font: UIFont? = nil, underline: Bool = false){
        do {
            let regex = try NSRegularExpression(pattern: "\\[(.*?)\\]")
            let results = regex.matches(in: str,
                                        range: NSRange(str.startIndex..., in: str))
            var ranges: Array<NSRange> = []
            if results.count == args.count {
                for i in (0...results.count-1){
                    let range = results[i].range
                    let newRange = NSRange(location: (range.location - (i * 2)), length: range.length - 2)
                    ranges.append(newRange)
                }
                
                let attrText = NSMutableAttributedString(string: str.replacingOccurrences(of: "[", with: "").replacingOccurrences(of: "]", with: ""))
                for i in (0...ranges.count-1){
                    attrText.addAttribute(.link, value: args[i], range: ranges[i])
                    if font != nil {
                        attrText.removeAttribute(NSAttributedString.Key.font, range: ranges[i])
                        attrText.addAttribute(NSAttributedString.Key.font, value: font!, range: ranges[i])
                    }
                }
                
                self.attributedText = attrText
                if underline {
                    self.linkTextAttributes = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
                }
            }
            
            
        } catch let error {
            print(error)
        }
    }
    
}

extension UITextField {
    func changePlaceHolder(placeHolderTxt:String,color:UIColor){
        self.attributedPlaceholder =
            NSAttributedString(string: placeHolderTxt, attributes: [NSAttributedString.Key.foregroundColor: color])
    }
    
}
