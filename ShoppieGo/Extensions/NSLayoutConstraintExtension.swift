//
//  NSLayoutConstraintExtension.swift
//  ShoppieGo
//
//  Created by Kaizer Jeno Kornel on 2019. 04. 19..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import Foundation

extension NSLayoutConstraint{
    
    func getActiveConstant() -> CGFloat {
        return self.isActive ? self.constant : 0.0
    }
    
}
