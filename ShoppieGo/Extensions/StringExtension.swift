//
//  StringExtension.swift
//  ShoppieGo
//
//  Created by Kaizer Jeno Kornel on 2019. 04. 23..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import Foundation

extension String {
    func localized() -> String {
        return NSLocalizedString(self, comment: "")
    }
    
    func localized(args: CVarArg...) -> String{
        return String(format: self.localized(), arguments: args)
    }
}
