//
//  NSMutableDataExtension.swift
//  ShoppieGo
//
//  Created by Kaizer Jeno Kornel on 2019. 04. 21..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import Foundation

extension NSMutableData {
    
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}
