//
//  UIImageExtension.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 20..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

extension UIButton {

    private func setImage(_ image: UIImage, stateFor: UIControl.State, needInvert: Bool){
        var img = image
        if needInvert {
            //img = img.inverseImage()!
            img = img.toWhite()!
        }
        self.setImage(img, for: stateFor)
    }
    
    func download(from link: String?, stateFor: UIControl.State = .normal, needInvert: Bool = false) {
        if !(link ?? "").isEmpty {
            let lnk = link!.replacingOccurrences(of: ".svg", with: ".png")
            guard let url = URL(string: lnk) else { return }
            var fn = (needInvert ? "bw_" : "") + url.lastPathComponent
            let lnkComponents = lnk.components(separatedBy: "/")
            if lnkComponents.count > 1 {
                fn = lnkComponents[lnkComponents.count - 2] + "_" + fn
            }
            let fileManager : FileManager = FileManager.default
            let dir = try? fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            var fileName: String? = nil
            if dir != nil {
                fileName = dir!.appendingPathComponent(fn).path
                if(fileManager.fileExists(atPath: fileName!)){
                    setImage(UIImage(contentsOfFile: fileName!)!, stateFor: stateFor, needInvert: needInvert)
                    return
                }
            }
            
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                guard
                    let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                    let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                    let data = data, error == nil,
                    let image = UIImage(data: data)
                    else { return }
                do {
                    DispatchQueue.main.async() {
                        self.setImage(image, stateFor: stateFor, needInvert: needInvert)
                        if fileName != nil {
                            if let pngData = self.image(for: stateFor)!.pngData() {
                                do {
                                    try pngData.write(to: URL(fileURLWithPath: fileName!))}
                                catch { }
                            }
                        }
                    }
                }
                }.resume()
        }
    }
}


extension UIButton {
    func AddshadowOnButton(){
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 5, height: 5)
        self.layer.shadowRadius = 5
        self.layer.shadowOpacity = 1.0
    }
}
