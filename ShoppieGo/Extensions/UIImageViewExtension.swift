//
//  UIImageViewExtansion.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 19..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

extension UIImageView {
    func download(from link: String?, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        self.contentMode = mode
        
        if !(link ?? "").isEmpty {
            CouponGoHelper.sharedInstance.downloadAndCacheImage(link: link!) { (img) in
                DispatchQueue.main.async {
                    self.image = img
                }
            }
        }
    }
}

extension UIImageView {
    func downloadAndMaskImage(from link: String?, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        self.contentMode = mode
        
        if !(link ?? "").isEmpty {
            CouponGoHelper.sharedInstance.downloadAndCacheImage(link: link!) { (img) in
                DispatchQueue.main.async {
                    let maskingImage = UIImage(named: "top")
                    let finalimg = self.maskImage(image: img, withMask: maskingImage!)
                    self.image = finalimg
                    
                }
            }
        }
    }
    
    func maskImage(image: UIImage, withMask maskImage: UIImage) -> UIImage {
        
        let maskRef = maskImage.cgImage
        
        let mask = CGImage(
            maskWidth: maskRef!.width,
            height: maskRef!.height,
            bitsPerComponent: maskRef!.bitsPerComponent,
            bitsPerPixel: maskRef!.bitsPerPixel,
            bytesPerRow: maskRef!.bytesPerRow,
            provider: maskRef!.dataProvider!,
            decode: nil,
            shouldInterpolate: false)
        
        let masked = image.cgImage!.masking(mask!)
        let maskedImage = UIImage(cgImage: masked!)
        
        
        return maskedImage
        
    }
}


extension UIImageView {
    func applyshadowWithCorner(containerView : UIView, cornerRadious : CGFloat){
        containerView.clipsToBounds = false
        containerView.layer.shadowColor = UIColor.gray.cgColor
        containerView.layer.shadowOpacity = 1
        containerView.layer.shadowOffset = CGSize.zero
        containerView.layer.shadowRadius = 10
        containerView.layer.cornerRadius = cornerRadious
        containerView.layer.shadowPath = UIBezierPath(roundedRect: containerView.bounds, cornerRadius: cornerRadious).cgPath
        self.clipsToBounds = true
        self.layer.cornerRadius = cornerRadious
    }
}
