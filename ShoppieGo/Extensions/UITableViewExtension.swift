//
//  UITableViewExtension.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 15..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

extension UITableView {
    override func scrollToBottom(animated: Bool = true) {
        let sections = self.numberOfSections
        let rows = self.numberOfRows(inSection: sections - 1)
        if (rows > 0){
            self.scrollToRow(at: IndexPath(row: rows - 1, section: sections - 1), at: .bottom, animated: animated)
        }
    }
}
