//
//  UIScrollViewExtension.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 04. 16..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

extension UIScrollView {
    @objc func scrollToBottom(animated: Bool = true){
        let bottomOffset = CGPoint(x: 0, y: self.contentSize.height - self.bounds.size.height)
        self.setContentOffset(bottomOffset, animated: animated)
    }
    
    @objc func scrollToTop(animated: Bool = true) {
        self.setContentOffset(.zero, animated: animated)
    }
}
