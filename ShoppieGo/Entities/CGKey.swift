//
//  CGMapKeyItem.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 19..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import Foundation

class CGKey: CGItem{
    override init() {
        super.init()
        
        self.type = "key"
    }
}
