//
//  CGChange.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 04. 01..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

class CGTradeItems {
    var isAccepted = false
    var keyCount = 0
    var coupons: Array<CGCoupon> = []
    
    func clear(){
        isAccepted = false
        keyCount = 0
        coupons.removeAll()
    }
}
