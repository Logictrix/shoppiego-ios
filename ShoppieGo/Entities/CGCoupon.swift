//
//  CGCoupon.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 02. 28..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

class CGCouponRedeemedItem {
    var id = 0
    var code = ""
    var date: Date?
    var codeImageUrl = ""
}

class CGCouponAvailableItem {
    var userId = 0
    var nickname = ""
    var profilePictureUrl = ""
    var isFree = false
}

class CGCoupon: CGItem{
    var descriptionUrl = ""
    var campaignId = 0
    var title = ""
    var brandName = ""
    var brandImageUrl = ""
    var coverImageurl = ""
    var validDate: Date?
    var openAt: Date?
    var freeCount = 0
    var redeemedCount = 0
    var description = ""
    var webshop = ""
    var isUnlimited = false
    var isFixed = false
    var requiredKeyCount = 0
    var redeemedItems: Array<CGCouponRedeemedItem> = []
    var availableItems: Array<CGCouponAvailableItem> = []
    
    override init() {
        super.init()
        
        self.type = "coupon"
    }
}
