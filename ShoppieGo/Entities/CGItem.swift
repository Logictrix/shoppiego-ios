//
//  CGItem.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 04. 15..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import Foundation

class CGItem{
    var id = 0
    var lat = 0.0
    var lng = 0.0
    var count = 0
    var type = ""
    
    var marker: Any?
}
