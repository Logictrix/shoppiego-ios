//
//  CGMessage.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 16..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import Foundation

class CGMessage{
    var userId = 0
    var profilePictureUrl = ""
    var isMale = true
    var nickname = ""
    var isUnread = false
    var message = ""
    var messageBy = ""
    var createdAt : Date? = nil
    var blockedByCurrentUser = false
    var blockedByPartner = false
}
