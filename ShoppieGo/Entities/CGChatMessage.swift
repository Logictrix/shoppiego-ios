//
//  CGChatMessage.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 31..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

class CGChatMessage {
    var userId = 0
    var message = ""
    var nickname = ""
    var profilePictureUrl = ""
    var isSystem = false
}
