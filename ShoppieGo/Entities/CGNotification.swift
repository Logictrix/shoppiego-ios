//
//  CGNotification.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 07..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

class CGNotification {
    
    var id = 0
    var userId = 0
    var language = ""
    var type = ""
    var title = ""
    var body = ""
    var icon = ""
    var date: Date?
    var url = ""
    var campaignId = 0
    var dropId = 0
    var lat = 0.0
    var lng = 0.0
    var isCathed = false
    
}
