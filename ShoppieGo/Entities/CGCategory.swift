//
//  CGCategory.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 02. 28..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import Foundation

class CGCategory{
    var id = 0
    var title = ""
    var iconUrl = ""
}
