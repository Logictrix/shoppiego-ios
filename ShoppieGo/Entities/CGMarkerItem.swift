//
//  CGMarkerItem.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 19..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import Foundation
import GoogleMaps

class CGMarkerItem: NSObject, GMUClusterItem {
    var position: CLLocationCoordinate2D
    var item: Any?
    
    init(position: CLLocationCoordinate2D, item: Any? = nil) {
        self.position = position
        self.item = item
    }
}
