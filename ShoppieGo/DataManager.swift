//
//  DataManager.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 02. 14..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import Foundation

class DataManager: NSObject {
    
    static let sharedInstance = DataManager()
    private var db: OpaquePointer? = nil
    private let currentVersion : Int32 = 18
    private let semaphore: DispatchSemaphore = DispatchSemaphore(value: 1)
    static var connectionCount = 0
    
    override init(){
        super.init()
        
        checkDB()
    }
    
    private func getString(_ upchar: UnsafePointer<UInt8>?) -> String? {
        if upchar == nil{
            return nil
        }
        
        return String(cString: upchar!)
    }
    
    private func dbFilePath() -> String {
        let documents = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        let fileURL = documents.appendingPathComponent("ShoppieGo.sql")
        return fileURL.path
    }
    
    private func getDBVersion() -> Int32{
        openDB()
        
        var stmnt : OpaquePointer? = nil
        var retVal : Int32 = -1
        
        if(sqlite3_prepare_v2(db, "PRAGMA user_version", -1, &stmnt, nil) != SQLITE_OK) {
            let errmsg = String(cString: sqlite3_errmsg(db));
            print("error preparing select: \(errmsg)")
        }
        
        if(sqlite3_step(stmnt) == SQLITE_ROW){
            retVal = sqlite3_column_int(stmnt, 0)
        }
        
        if sqlite3_finalize(stmnt) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error finalizing prepared statement: \(errmsg)")
        }
        
        closeDB()
        
        return retVal
    }
    
    private func setDBVersion(){
        openDB()
        execSQL(String(format:"PRAGMA user_version = %d", currentVersion))
        closeDB()
    }
    
    private func checkDB(){
        let fileManager : FileManager = FileManager.default
        if(fileManager.fileExists(atPath: dbFilePath())){
            if(getDBVersion() != currentVersion){
                dropDB()
                createDB()
                setDBVersion()
            }
        }
        else{
            createDB()
            openDB()
            setDBVersion()
        }
    }
    
    func createDB(){
        if(sqlite3_open(dbFilePath(), &db) != SQLITE_OK) {
            print("error creating database")
            closeDB()
        }
        else {
            execSQL("CREATE TABLE IF NOT EXISTS APP(KEY text primary key, VALUE text);");
            execSQL("CREATE TABLE IF NOT EXISTS SESSION(KEY text primary key, VALUE text);")
            execSQL("CREATE TABLE IF NOT EXISTS NOTIFICATION(ID integer primary key autoincrement, USER_ID integer, LANGUAGE text, TYPE text, TITLE text, BODY text, ICON text, CAMPAIGN_ID integer, DROP_ID integer, LAT real, LNG real, CATCHED integer default 0, URL text, DATE integer);")
            execSQL("CREATE TABLE IF NOT EXISTS CATEGORY(ID integer primary key, TITLE text, ICON_URL text);")
            // OWN_COUPONS: DISCOUNT_TYPE -> IS_FIXED
            execSQL("CREATE TABLE IF NOT EXISTS OWN_COUPONS(ID integer primary key, TITLE text, BRAND_NAME text, BRAND_IMAGE_URL text, COVER_IMAGE_URL text, COUNT integer, REDEEMED_COUNT integer, IS_UNLIMITED integer, VALID_DATE integer, IS_FIXED integer);")
            
            setApp(Constants.APP_SOUNDEFFECT, value: true)
            setApp(Constants.APP_VIBRATION, value: true)
            setApp(Constants.APP_PUSHNOTIFICATION, value: true)
        }
    }
    
    private func openDB(){
        semaphore.wait()
        
        DataManager.connectionCount += 1

        if DataManager.connectionCount == 1 {
            if(sqlite3_open_v2(dbFilePath(), &db, SQLITE_OPEN_READWRITE|SQLITE_OPEN_FULLMUTEX, nil) != SQLITE_OK){
                print("error opening database")
                closeDB()
            }
        }
        
        semaphore.signal()
    }
    
    private func closeDB(){
        semaphore.wait()
        
        DataManager.connectionCount -= 1
        if DataManager.connectionCount <= 0 {
            sqlite3_close(self.db)
            DataManager.connectionCount = 0
        }
        
        semaphore.signal()
    }
    
    private func execSQL(_ sql : String){
        semaphore.wait()
        
        var errMsg:UnsafeMutablePointer<Int8>? = nil
        
        if(sqlite3_exec(db, sql, nil, nil, &errMsg) != SQLITE_OK){
            if errMsg == nil {
                print("Unknown error")
            }
            else {
                print(String(cString: errMsg!))
            }
            sqlite3_close(db)
        }
        
        semaphore.signal()
    }
    
    func dropDB(){
        closeDB()
        let fileManager : FileManager = FileManager.default
        let success = fileManager.fileExists(atPath: dbFilePath())
        if(success) {
            try! fileManager.removeItem(atPath: dbFilePath())
        }
    }
    
    func cleanDB(){
        execSQL("DELETE FROM APP")
        execSQL("DELETE FROM SESSION")
        execSQL("DELETE FROM NOTIFICATION")
        execSQL("DELETE FROM CATEGORY")
        execSQL("DELETE FROM OWN_COUPONS")
    }
    
    func setApp(_ key: String, value: String){
        openDB()
        execSQL(String(format: "INSERT OR REPLACE INTO APP ('KEY', 'VALUE') VALUES ('%@', '%@')", key, value))
        closeDB()
    }
    
    func setApp(_ key: String, value: Int){
        let val = String(value)
        setApp(key, value: val)
    }
    
    func setApp(_ key: String, value: Float){
        let val = String(value)
        setApp(key, value: val)
    }
    
    func setApp(_ key: String, value: Bool){
        setApp(key, value: (value ? "1" : "0"))
    }
    
    func getAppString(_ key: String) -> String?{
        openDB()
        
        var stmnt : OpaquePointer? = nil
        var retVal : String?
        
        if(sqlite3_prepare_v2(db, String(format: "SELECT VALUE FROM APP WHERE KEY = '%@'", key), -1, &stmnt, nil) != SQLITE_OK) {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing select: \(errmsg)")
        }
        
        if(sqlite3_step(stmnt) == SQLITE_ROW){
            retVal = String(cString: sqlite3_column_text(stmnt, 0))
        }
        
        if sqlite3_finalize(stmnt) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error finalizing prepared statement: \(errmsg)")
        }
        
        closeDB()
        
        return retVal
    }
    
    func getAppInt(_ key: String) -> Int?{
        let val = getAppString(key)
        return (val == nil ? nil : Int(val!))
    }
    
    func getAppFloat(_ key: String) -> Float?{
        let val = getAppString(key)
        return (val == nil ? nil : Float(val!))
    }
    
    func getAppBool(_ key: String, defaultValue: Bool = false) -> Bool{
        let val = getAppString(key)
        return (val == nil ? defaultValue : val == "1")
    }
    
    func clearAppTable(){
        openDB()
        execSQL("DELETE FROM APP")
        closeDB()
    }
    
    func deleteApp(_ key: String){
        openDB()
        execSQL(String(format: "DELETE FROM APP WHERE KEY = '%@'", key))
        closeDB()
    }
    
    func setSession(_ key: String, value: String){
        openDB()
        execSQL(String(format: "INSERT OR REPLACE INTO SESSION ('KEY', 'VALUE') VALUES ('%@', '%@')", key, value))
        closeDB()
    }
    
    func setSession(_ key: String, value: Int){
        let val = String(value)
        setSession(key, value: val)
    }
    
    func setSession(_ key: String, value: Bool){
        setSession(key, value: (value ? "1" : "0"))
    }
    
    func setSession(_ key: String, value: Float){
        let val = String(value)
        setSession(key, value: val)
    }
    
    func setSession(_ key: String, value: Double){
        let val = String(value)
        setSession(key, value: val)
    }
    
    func getSessionString(_ key: String) -> String?{
        openDB()
        
        var stmnt : OpaquePointer? = nil
        var retVal : String? = nil
        
        if(sqlite3_prepare_v2(db, String(format: "SELECT VALUE FROM SESSION WHERE KEY = '%@'", key), -1, &stmnt, nil) != SQLITE_OK) {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing select: \(errmsg)")
        }
        
        if(sqlite3_step(stmnt) == SQLITE_ROW){
            retVal = String(cString: sqlite3_column_text(stmnt, 0))
        }
        
        if sqlite3_finalize(stmnt) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error finalizing prepared statement: \(errmsg)")
        }
        
        closeDB()
        
        return retVal
    }
    
    func getSessionInt(_ key: String) -> Int?{
        let val = getSessionString(key)
        return (val == nil ? nil : Int(val!))
    }
    
    func getSessionBool(_ key: String, defaultValue: Bool = false) -> Bool{
        let val = getSessionString(key)
        return (val == nil ? defaultValue : val == "1")
    }
    
    func getSessionFloat(_ key: String) -> Float?{
        let val = getSessionString(key)
        return (val == nil ? nil : Float(val!))
    }
    
    func getSessionDouble(_ key: String) -> Double?{
        let val = getSessionString(key)
        return (val == nil ? nil : Double(val!))
    }
    
    func clearSessionTable(){
        openDB()
        execSQL("DELETE FROM SESSION")
        closeDB()
    }
    
    func deleteSession(_ key: String){
        openDB()
        execSQL(String(format: "DELETE FROM SESSION WHERE KEY = '%@'", key))
        closeDB()
    }
    
    func addCategory(_ id: Int, title: String, iconUrl: String){
        openDB()
        execSQL(String(format: "INSERT OR REPLACE INTO CATEGORY ('ID', 'TITLE', 'ICON_URL') VALUES (%d, '%@', '%@')", id, title, iconUrl))
        closeDB()
    }
    
    func getCategories() -> Array<CGCategory>{
        openDB()
        
        var stmnt : OpaquePointer? = nil
        var retVal = [CGCategory]()
        
        if(sqlite3_prepare_v2(db, String(format: "SELECT ID, TITLE, ICON_URL FROM CATEGORY ORDER BY TITLE"), -1, &stmnt, nil) == SQLITE_OK) {
            while(sqlite3_step(stmnt) == SQLITE_ROW){
                let cat = CGCategory()
                cat.id = Int(sqlite3_column_int(stmnt, 0))
                cat.title = String(cString: sqlite3_column_text(stmnt, 1))
                cat.iconUrl = String(cString: sqlite3_column_text(stmnt, 2))
                
                retVal.append(cat)
            }
        }
        else{
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing select: \(errmsg)")
        }
        
        if sqlite3_finalize(stmnt) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error finalizing prepared statement: \(errmsg)")
        }
        
        closeDB()
        
        return retVal
    }
    
    func clearCategoryTable(){
        openDB()
        execSQL("DELETE FROM CATEGORY")
        closeDB()
    }
    
    func addCoupon(_ coupon: CGCoupon){
        let vDate = (coupon.validDate == nil ? -1 : Int32(coupon.validDate!.timeIntervalSince1970))
        
        openDB()
        execSQL(String(format: "INSERT OR REPLACE INTO OWN_COUPONS ('ID', 'TITLE', 'BRAND_NAME', 'BRAND_IMAGE_URL', 'COVER_IMAGE_URL', 'VALID_DATE', 'COUNT', 'REDEEMED_COUNT', 'IS_UNLIMITED', 'IS_FIXED') VALUES (%d, '%@', '%@', '%@', '%@', '%d', %d, %d, %d, %d)", coupon.id, coupon.title, coupon.brandName, coupon.brandImageUrl, coupon.coverImageurl ,vDate, coupon.count, coupon.redeemedCount, coupon.isUnlimited ? 1 : 0, coupon.isFixed ? 1 : 0))
        closeDB()
    }
    
    func getCoupon(_ id: Int) -> CGCoupon?{
        openDB()
        
        var stmnt : OpaquePointer? = nil
        var retVal : CGCoupon? = nil
        
        if(sqlite3_prepare_v2(db, String(format: "SELECT ID, TITLE, BRAND_NAME, BRAND_IMAGE_URL, COVER_IMAGE_URL, VALID_DATE, COUNT, REDEEMED_COUNT, IS_UNLIMITED, IS_FIXED FROM OWN_COUPONS WHERE ID = %d", id), -1, &stmnt, nil) == SQLITE_OK) {
            if(sqlite3_step(stmnt) == SQLITE_ROW){
                retVal = CGCoupon()
                retVal!.id = Int(sqlite3_column_int(stmnt, 0))
                retVal!.title = String(cString: sqlite3_column_text(stmnt, 1))
                retVal!.brandName = String(cString: sqlite3_column_text(stmnt, 2))
                retVal!.brandImageUrl = String(cString: sqlite3_column_text(stmnt, 3))
                 retVal!.coverImageurl = String(cString: sqlite3_column_text(stmnt, 4))
                let vDate = sqlite3_column_int(stmnt, 5)
                retVal!.validDate = (vDate == -1 ? nil : Date(timeIntervalSince1970: TimeInterval(vDate)))
                retVal!.count = Int(sqlite3_column_int(stmnt, 6))
                retVal!.redeemedCount = Int(sqlite3_column_int(stmnt, 7))
                retVal!.isUnlimited = Int(sqlite3_column_int(stmnt, 8)) == 1
                retVal!.isFixed = Int(sqlite3_column_int(stmnt, 9)) == 1
            }
        }
        else{
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing select: \(errmsg)")
        }
        
        if sqlite3_finalize(stmnt) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error finalizing prepared statement: \(errmsg)")
        }
        
        closeDB()
        
        return retVal
    }
    
    func adddbfield(sFieldName:String, sTable:String) -> Bool
    {
        var bReturn:Bool = false;
        let sSQL="ALTER TABLE " + sTable + " ADD COLUMN " + sFieldName + " INTEGER DEFAULT -1";
        var statement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, sSQL, -1, &statement, nil) != SQLITE_OK
        {
            print("Failed to prepare statement")
        }
        else
        {
            if sqlite3_step(statement) == SQLITE_DONE
            {
                print("field " + sFieldName + " added  to  " + sTable);
            }
            sqlite3_finalize(statement);
            bReturn=true;
        }
        return bReturn;
    }
    
    
    func getCoupons() -> Array<CGCoupon>{
        openDB()
        
        var stmnt : OpaquePointer? = nil
        var retVal = [CGCoupon]()
        
        if(sqlite3_prepare_v2(db, "SELECT ID, TITLE, BRAND_NAME, BRAND_IMAGE_URL, COVER_IMAGE_URL, VALID_DATE, COUNT, REDEEMED_COUNT, IS_UNLIMITED, IS_FIXED FROM OWN_COUPONS ORDER BY VALID_DATE ASC", -1, &stmnt, nil) == SQLITE_OK) {
            while(sqlite3_step(stmnt) == SQLITE_ROW){
                let coupon = CGCoupon()
                coupon.id = Int(sqlite3_column_int(stmnt, 0))
                coupon.title = String(cString: sqlite3_column_text(stmnt, 1))
                coupon.brandName = String(cString: sqlite3_column_text(stmnt, 2))
                coupon.brandImageUrl = String(cString: sqlite3_column_text(stmnt, 3))
                coupon.coverImageurl = String(cString: sqlite3_column_text(stmnt, 4))
                let vDate = sqlite3_column_int(stmnt, 5)
                coupon.validDate = (vDate == -1 ? nil : Date(timeIntervalSince1970: TimeInterval(vDate)))
                coupon.count = Int(sqlite3_column_int(stmnt, 6))
                coupon.redeemedCount = Int(sqlite3_column_int(stmnt, 7))
                coupon.isUnlimited = Int(sqlite3_column_int(stmnt, 8)) == 1
                coupon.isFixed = Int(sqlite3_column_int(stmnt, 9)) == 1
                
                retVal.append(coupon)
            }
        }
        else{
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing select: \(errmsg)")
        }
        
        if sqlite3_finalize(stmnt) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error finalizing prepared statement: \(errmsg)")
        }
        
        closeDB()
        
        return retVal
    }
    
    func filtCoupons(_ keyword: String) -> Array<CGCoupon>{
        openDB()
        
        var stmnt : OpaquePointer? = nil
        var retVal = [CGCoupon]()
        
        let now = Int32(Date().timeIntervalSince1970)
        
        if(sqlite3_prepare_v2(db, String(format: "SELECT ID, TITLE, BRAND_NAME, BRAND_IMAGE_URL, COVER_IMAGE_URL, VALID_DATE, COUNT, REDEEMED_COUNT, IS_UNLIMITED, IS_FIXED FROM OWN_COUPONS WHERE TITLE LIKE '%%%@%%' OR BRAND_NAME LIKE '%%%@%%' ORDER BY (VALID_DATE < %d), (CASE WHEN VALID_DATE >= %d THEN VALID_DATE END) ASC, (CASE WHEN VALID_DATE < %d THEN VALID_DATE END) DESC", keyword, keyword, now, now, now), -1, &stmnt, nil) == SQLITE_OK) {
            while(sqlite3_step(stmnt) == SQLITE_ROW){
                let coupon = CGCoupon()
                coupon.id = Int(sqlite3_column_int(stmnt, 0))
                coupon.title = String(cString: sqlite3_column_text(stmnt, 1))
                coupon.brandName = String(cString: sqlite3_column_text(stmnt, 2))
                coupon.brandImageUrl = String(cString: sqlite3_column_text(stmnt, 3))
                coupon.coverImageurl = String(cString: sqlite3_column_text(stmnt, 4))
                let vDate = sqlite3_column_int(stmnt, 5)
                coupon.validDate = (vDate == -1 ? nil : Date(timeIntervalSince1970: TimeInterval(vDate)))
                coupon.count = Int(sqlite3_column_int(stmnt, 6))
                coupon.redeemedCount = Int(sqlite3_column_int(stmnt, 7))
                coupon.isUnlimited = Int(sqlite3_column_int(stmnt, 8)) == 1
                coupon.isFixed = Int(sqlite3_column_int(stmnt, 9)) == 1
                
                retVal.append(coupon)
            }
        }
        else{
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing select: \(errmsg)")
        }
        
        if sqlite3_finalize(stmnt) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error finalizing prepared statement: \(errmsg)")
        }
        
        closeDB()
        
        return retVal
    }
    
    func getExchangeableCoupons() -> Array<CGCoupon>{
        openDB()
        
        var stmnt : OpaquePointer? = nil
        var retVal = [CGCoupon]()
        
        let now = Int32(Date().timeIntervalSince1970)
        
        if(sqlite3_prepare_v2(db, String(format: "SELECT ID, TITLE, BRAND_NAME, BRAND_IMAGE_URL, COVER_IMAGE_URL, VALID_DATE, COUNT, REDEEMED_COUNT, IS_UNLIMITED, IS_FIXED FROM OWN_COUPONS WHERE IS_UNLIMITED = 0 AND COUNT > REDEEMED_COUNT AND VALID_DATE >= %d ORDER BY VALID_DATE ASC", now), -1, &stmnt, nil) == SQLITE_OK) {
            while(sqlite3_step(stmnt) == SQLITE_ROW){
                let coupon = CGCoupon()
                coupon.id = Int(sqlite3_column_int(stmnt, 0))
                coupon.title = String(cString: sqlite3_column_text(stmnt, 1))
                coupon.brandName = String(cString: sqlite3_column_text(stmnt, 2))
                coupon.brandImageUrl = String(cString: sqlite3_column_text(stmnt, 3))
                coupon.coverImageurl = String(cString: sqlite3_column_text(stmnt, 4))
                let vDate = sqlite3_column_int(stmnt, 5)
                coupon.validDate = (vDate == -1 ? nil : Date(timeIntervalSince1970: TimeInterval(vDate)))
                coupon.count = Int(sqlite3_column_int(stmnt, 6))
                coupon.redeemedCount = Int(sqlite3_column_int(stmnt, 7))
                coupon.isUnlimited = Int(sqlite3_column_int(stmnt, 8)) == 1
                coupon.isFixed = Int(sqlite3_column_int(stmnt, 9)) == 1
                
                retVal.append(coupon)
            }
        }
        else{
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing select: \(errmsg)")
        }
        
        if sqlite3_finalize(stmnt) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error finalizing prepared statement: \(errmsg)")
        }
        
        closeDB()
        
        return retVal
    }
    
    func clearCouponTable(){
        openDB()
        execSQL("DELETE FROM OWN_COUPONS")
        closeDB()
    }
    
    func getCouponCount() -> Int {
        openDB()
        
        var stmnt : OpaquePointer? = nil
        var retVal = 0
        
        if(sqlite3_prepare_v2(db, "SELECT COUNT(ID) FROM OWN_COUPONS", -1, &stmnt, nil) == SQLITE_OK) {
            while(sqlite3_step(stmnt) == SQLITE_ROW){
                retVal = Int(sqlite3_column_int(stmnt, 0))
            }
        }
        else{
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing select: \(errmsg)")
        }
        
        if sqlite3_finalize(stmnt) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error finalizing prepared statement: \(errmsg)")
        }
        
        closeDB()
        
        return retVal
    }
    
    func clearNotificationTable(){
        openDB()
        execSQL("DELETE FROM NOTIFICATION")
        closeDB()
    }
    
    func addNotificationDM(_ notification: CGNotification){
        let vDate = (notification.date == nil ? -1 : Int32(notification.date!.timeIntervalSince1970))
        
        openDB()
        execSQL(String(format: "INSERT OR REPLACE INTO NOTIFICATION ('USER_ID', 'LANGUAGE', 'TYPE', 'TITLE', 'BODY', 'ICON', 'DATE', 'URL') VALUES (%d, '%@', '%@', '%@', '%@', '%@', %d, '%@')", notification.userId, notification.language, notification.type, notification.title, notification.body, notification.icon, vDate, notification.url))
        closeDB()
    }
    
    func addNotificationDrop(_ notification: CGNotification){
        let vDate = (notification.date == nil ? -1 : Int32(notification.date!.timeIntervalSince1970))
        
        openDB()
        execSQL(String(format: "INSERT OR REPLACE INTO NOTIFICATION ('USER_ID', 'LANGUAGE', 'TYPE', 'TITLE', 'BODY', 'ICON', 'DATE', 'CAMPAIGN_ID', 'DROP_ID', 'LAT', 'LNG') VALUES (%d, '%@', '%@', '%@', '%@', '%@', %d, %d, %d, %f, %f)", notification.userId, notification.language, notification.type, notification.title, notification.body, notification.icon, vDate, notification.campaignId, notification.dropId, notification.lat, notification.lng))
        closeDB()
    }
    
    func setNotificationCatched(userId: Int, dropId: Int){
        openDB()
        execSQL(String(format: "UPDATE NOTIFICATION SET CATCHED = 1 WHERE USER_ID = %d AND DROP_ID = %d", userId, dropId))
        closeDB()
    }
    
    func isNotificationCatched(userId: Int, dropId: Int) -> Bool {
        openDB()
        
        var stmnt : OpaquePointer? = nil
        var retVal = false
        
        if(sqlite3_prepare_v2(db, String(format: "SELECT CATCHED FROM NOTIFICATION WHERE USER_ID = %d AND DROP_ID = %d", userId, dropId), -1, &stmnt, nil) == SQLITE_OK) {
            while(sqlite3_step(stmnt) == SQLITE_ROW){
                retVal = Int(sqlite3_column_int(stmnt, 0)) == 1
            }
        }
        else{
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing select: \(errmsg)")
        }
        
        if sqlite3_finalize(stmnt) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error finalizing prepared statement: \(errmsg)")
        }
        
        closeDB()
        
        return retVal
    }
    
    func getNotificationCount() -> Int {
        openDB()
        
        var stmnt : OpaquePointer? = nil
        var retVal = 0
        
        if(sqlite3_prepare_v2(db, "SELECT COUNT(ID) FROM NOTIFICATION", -1, &stmnt, nil) == SQLITE_OK) {
            while(sqlite3_step(stmnt) == SQLITE_ROW){
                retVal = Int(sqlite3_column_int(stmnt, 0))
            }
        }
        else{
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing select: \(errmsg)")
        }
        
        if sqlite3_finalize(stmnt) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error finalizing prepared statement: \(errmsg)")
        }
        
        closeDB()
        
        return retVal
    }
    
    func getNotifications(userId: Int, language: String, limit: Int) -> Array<CGNotification>{
        openDB()
        
        var stmnt : OpaquePointer? = nil
        var retVal = [CGNotification]()
        
        if(sqlite3_prepare_v2(db, String(format: "SELECT ID, USER_ID, LANGUAGE, TYPE, TITLE, BODY, ICON, CAMPAIGN_ID, DROP_ID, LAT, LNG, CATCHED, URL, DATE FROM NOTIFICATION WHERE USER_ID = %d AND LANGUAGE = '%@' ORDER BY DATE DESC, ID DESC LIMIT %d", userId, language, limit), -1, &stmnt, nil) == SQLITE_OK) {
            while(sqlite3_step(stmnt) == SQLITE_ROW){
                let notification = CGNotification()
                notification.id = Int(sqlite3_column_int(stmnt, 0))
                notification.userId = Int(sqlite3_column_int(stmnt, 1))
                notification.language = String(cString: sqlite3_column_text(stmnt, 2))
                notification.type = String(cString: sqlite3_column_text(stmnt, 3))
                notification.title = String(cString: sqlite3_column_text(stmnt, 4))
                notification.body = String(cString: sqlite3_column_text(stmnt, 5))
                notification.icon = String(cString: sqlite3_column_text(stmnt, 6))
                notification.campaignId = Int(sqlite3_column_int(stmnt, 7))
                notification.dropId =  Int(sqlite3_column_int(stmnt, 8))
                notification.lat = Double(sqlite3_column_double(stmnt, 9))
                notification.lng = Double(sqlite3_column_double(stmnt, 10))
                notification.isCathed = Int(sqlite3_column_int(stmnt, 11)) == 1
                notification.url = getString(sqlite3_column_text(stmnt, 12)) ?? ""
                let vDate = sqlite3_column_int(stmnt, 13)
                notification.date = (vDate == -1 ? nil : Date(timeIntervalSince1970: TimeInterval(vDate)))
                
                retVal.append(notification)
            }
        }
        else{
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error preparing select: \(errmsg)")
        }
        
        if sqlite3_finalize(stmnt) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db))
            print("error finalizing prepared statement: \(errmsg)")
        }
        
        closeDB()
        
        return retVal
    }
}
