//
//  MapIconGenerator.swift
//  ShoppieGo
//
//  Created by Kaizer Jeno Kornel on 2019. 05. 24..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import Foundation

class MapClusterIconGenerator: GMUDefaultClusterIconGenerator {
    
    private let buckets = [10, 20, 50, 100, 200, 500, 1000]
    
    override func icon(forSize size: UInt) -> UIImage {
        let bucketIdx = getBucketIndex(forSize: size)
        let text = (size < buckets[0] ? String(format: "%d", size) : String(format: "%d+", buckets[bucketIdx]))
        let image = textToImage(drawText: text,
                                inImage: UIImage(named: "mrkr-cluster")!,
                                font: UIFont.systemFont(ofSize: 24, weight: .bold))
        return image
    }
    
    private func getBucketIndex(forSize size: UInt) -> Int {
        var i = 0
        while (i + 1 < buckets.count && buckets[i + 1] <= size) {
            i += 1
        }
        return i
    }
    
    private func textToImage(drawText text: String, inImage image: UIImage, font: UIFont) -> UIImage {
        UIGraphicsBeginImageContext(image.size)
        image.draw(in: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height))
        
        let textStyle = NSMutableParagraphStyle()
        textStyle.alignment = NSTextAlignment.center
        let textColor = UIColor.black
        let attributes=[
            NSAttributedString.Key.font: font,
            NSAttributedString.Key.paragraphStyle: textStyle,
            NSAttributedString.Key.foregroundColor: textColor]
        
        // vertically center (depending on font)
        let textH = font.lineHeight
        let textY = (image.size.height-textH)/2
        let textRect = CGRect(x: 0, y: textY, width: image.size.width, height: textH)
        text.draw(in: textRect.integral, withAttributes: attributes)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result!
    }
    
}
