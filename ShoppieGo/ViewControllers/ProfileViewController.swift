//
//  ProfileViewController.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 06..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit
import Photos
import Foundation

class ProfileViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var tvNotifications: UITableView!
    @IBOutlet weak var nicknameLabel: UILabel!
    @IBOutlet weak var profilePicture: UIRoundedImageView!
    @IBOutlet weak var plusProfilePicture: UIImageView!
    
    private let imagePicker = UIImagePickerController()
    private var notifications: Array<CGNotification> = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tvNotifications.delegate = self
        tvNotifications.dataSource = self
        imagePicker.delegate = self
        
        profilePicture.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(imageTapped)))
        plusProfilePicture.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(imageTapped)))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        nicknameLabel.text = DataManager.sharedInstance.getSessionString(Constants.SESSION_USER_NICKNAME) ?? ""
        let ppu = DataManager.sharedInstance.getSessionString(Constants.SESSION_USER_PROFILE_PICTURE_URL)
        if (ppu ?? "").isEmpty {
            profilePicture.image = UIImage(named: "avatar-lrg-female")
        }
        else {
            profilePicture.download(from: DataManager.sharedInstance.getSessionString(Constants.SESSION_USER_PROFILE_PICTURE_URL) ?? "")
        }
        
        refresh()
    }
    
    private func refresh(){
        let userId = DataManager.sharedInstance.getSessionInt(Constants.SESSION_USER_ID) ?? 0
        let language = ""
        
        notifications = DataManager.sharedInstance.getNotifications(userId: userId, language: language, limit: 10)
        tvNotifications.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvNotifications.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
        
        let noti = notifications[indexPath.item]
        
        cell.notificationImage.download(from: noti.icon)
        cell.notificationMessage.text = noti.body
        cell.notificationTime.text = ""
        
        if noti.date != nil {
            let days = CouponGoHelper.sharedInstance.getDaysFromNow(date: noti.date!)
            switch (days){
            case 0 :
                cell.notificationTime.text = "profile_today".localized(args: CouponGoHelper.sharedInstance.getTime(date: noti.date!))
                break
            case -1:
                cell.notificationTime.text = "profile_yesterday".localized(args: CouponGoHelper.sharedInstance.getTime(date: noti.date!))
                break
            default:
                cell.notificationTime.text = CouponGoHelper.sharedInstance.getMonthsAndDays(date: noti.date!)
                break
            }
            cell.notificationTime.setLineSpacing(lineSpacing: 2.0)
            cell.notificationTime.textAlignment = .right
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let noti = notifications[indexPath.item]
        
        CouponGoHelper.sharedInstance.launchParameters = ["type" : "cg_drop",
                                                          "lat" : String(format: "%f", noti.lat),
                                                          "lng" : String(format: "%f", noti.lng)]
        CouponGoHelper.sharedInstance.showMainTabBar(selectedIdx: 0)
    }
    
    @IBAction func settingsTouch(_ sender: Any) {
        CouponGoHelper.sharedInstance.showViewController(viewName: Constants.VC_SETTINGS)
    }
    
    @IBAction func signoutTouch(_ sender: Any) {
        DataManager.sharedInstance.clearSessionTable()
        CouponGoHelper.sharedInstance.showViewController(viewName: Constants.VC_START)
        CouponGoHelper.sharedInstance.accessToken = ""
    }
    
    @objc private func imageTapped(_ sender: Any) {
        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        switch photoAuthorizationStatus {
        case .authorized:
            print("Access is granted by user")
            showImagePicker()
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({
                (newStatus) in
                if newStatus ==  PHAuthorizationStatus.authorized {
                    print("success")
                    DispatchQueue.main.async {
                        self.showImagePicker()
                    }
                }
            })
            print("It is not determined until now")
        case .restricted:
            print("User do not have access to photo album.")
        case .denied:
            print("User has denied the permission.")
        default :
            print("Unknown state")
        }
    }
    
    func showImagePicker(){
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            guard let imageData = pickedImage.pngData() else { return }
            
            let boundary =  "*****";
            let crlf = "\r\n";
            let twoHyphens = "--";
            
            let url = URL(string: Constants.URL_APP + "/user")
            let request = NSMutableURLRequest(url: url!)
            request.httpMethod = "POST"
            request.addValue("Bearer " + CouponGoHelper.sharedInstance.accessToken, forHTTPHeaderField: "Authorization")
            request.addValue("multipart/form-data; boundary=" + boundary, forHTTPHeaderField: "Content-Type")

            let body = NSMutableData();
            body.appendString(twoHyphens + boundary + crlf)
            body.appendString("Content-Disposition: form-data; name=\"profile_picture\";filename=\"profile-image.png\"" + crlf)
            body.appendString("Content-Type: image/png" + crlf)
            body.appendString(crlf)
            body.append(imageData)
            body.appendString(crlf)
            body.appendString(twoHyphens + boundary + twoHyphens + crlf)
            
            request.httpBody = body as Data
            
            let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                do {
                    //let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    
                    guard let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary else {
                        return
                    }
                    
                    if json.value(forKey: "success") as! Bool {
                        DispatchQueue.main.async {
                            self.profilePicture.image = pickedImage
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                            let error = CouponGoHelper.sharedInstance.jsonToString(json.value(forKey: "error"))
                            CouponGoHelper.sharedInstance.showAlert(viewController: self, title: "", message: error)
                        }
                    }
                } catch _ {
                    DispatchQueue.main.async {
                        CouponGoHelper.sharedInstance.showAlert(viewController: self, title: "Hiba", message: "profile_image_too_large".localized())
                    }
                }
            })
            task.resume()
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion:nil)
    }
}
