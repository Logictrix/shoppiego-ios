//
//  CouponsViewController.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 02. 28..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

class CouponsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var tfSearch: UIRoundedTextField!
    @IBOutlet weak var couponsCollectionView: UICollectionView!
    
    private var couponList : Array<CGCoupon> = []
    private var searchTimer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tfSearch.delegate = self
        
        couponsCollectionView.delegate = self
        couponsCollectionView.dataSource = self
        
        DataManager.sharedInstance.clearCouponTable()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //view.layoutSubviews()
        refresh()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return false
    }
    
    override func viewDidLayoutSubviews() {
        let layout = couponsCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        layout.minimumInteritemSpacing = 5
        layout.itemSize = CGSize(width: (couponsCollectionView.frame.width - 20) / 2, height: layout.itemSize.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return couponList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = couponsCollectionView.dequeueReusableCell(withReuseIdentifier: "CouponCell", for: indexPath) as! CouponCell
        
        let listItem = couponList[indexPath.item]
        
       // cell.background.image = UIImage(named: String(format: "coupon-%d", (listItem.id % 6)))
        cell.count.text = String(format: "%d", listItem.count)
        cell.countView.isHidden = listItem.count <= 1
      //  cell.buttomImg.applyshadowWithCorner(containerView: cell.contentView, cornerRadious: 10.0)
        print("listItem_coverImageurl",listItem.coverImageurl)
        
        if !listItem.coverImageurl.isEmpty {
            cell.coverImage.isHidden = false
            cell.buttomImg.isHidden = true
            cell.coverImage.download(from: listItem.coverImageurl)
            cell.background.isHidden = false
        }else{
           cell.coverImage.isHidden = true
           cell.buttomImg.isHidden = true
           cell.background.isHidden = false
           cell.background.image = UIImage(named: String(format: "coupon-%d", (listItem.id % 6)))
        }
        
        if !listItem.brandImageUrl.isEmpty {
            cell.brandImage.download(from: listItem.brandImageUrl)
        }
        
        cell.discount.text = (listItem.isFixed ? "coupons_fix_discount".localized() : "coupons_percentage_discount".localized(args: listItem.title))
        cell.validDate.text = CouponGoHelper.sharedInstance.convertDateToString(listItem.validDate)
        cell.validDate.font = UIFont.systemFont(ofSize: 11)
        cell.validDate.textColor = .black
        cell.alertView.isHidden = true
        
        if CouponGoHelper.sharedInstance.getDaysFromNow(date: listItem.validDate) < Constants.VALID_DAYS_FROM_NOW {
            cell.alertView.isHidden = false
            cell.validDate.font = UIFont.systemFont(ofSize: 13, weight: .bold)
            cell.validDate.textColor = .red
        }
        
        return cell
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let coupon = couponList[indexPath.item]
        
        CouponGoHelper.sharedInstance.commManager.request(method: "GET", url: String(format: "/coupon/%d/own", coupon.id), queryData: nil, postData: nil) { (result, error) in
            if let result = result {
               
                let dict = result as NSDictionary
                 print("dict",dict)
                let obj = dict.value(forKey: "coupon") as! NSDictionary
                
                let coupon = CouponGoHelper.sharedInstance.jsonToCoupon(obj)
                
                DispatchQueue.main.async {
                    let vc = CouponGoHelper.sharedInstance.getViewController(viewName: Constants.VC_COUPON) as! CouponViewController
                    vc.delegate = self
                    vc.coupon = coupon
                    vc.isOwn = true
                    self.present(vc, animated: true, completion: nil)
                }
            }
            else if error != nil {
                CouponGoHelper.sharedInstance.showAlert(viewController: self, message: "error_server_error".localized(), animated: true, isError: true)
            }
        }
    }
    
  
   
    @IBAction func tfSearchChanged(_ sender: Any) {
        searchTimer?.invalidate()
        searchTimer = Timer.scheduledTimer(timeInterval: Constants.SEARCH_DELAY, target: self, selector: #selector(timedFilter), userInfo: nil, repeats: false)
    }
    
    @objc func timedFilter(){
        filterCoupons()
    }
    
    private func refresh(){
//        if DataManager.sharedInstance.getCouponCount() > 0 {
//            return
//        }
//        
        CouponGoHelper.sharedInstance.addLoadAnimation(parent: self.view)
        
        CouponGoHelper.sharedInstance.commManager.request(method: "GET", url: "/coupon/list/own", queryData: nil, postData: nil, delayInMilliseconds: 500) { (result, error) in
            if let result = result {
                DispatchQueue.main.async {
                    let dict = result as NSDictionary
                    let couponList = dict.value(forKey: "coupon_list") as! NSArray
                     print("couponList",couponList)
                    for item in couponList{
                        let obj = item as! NSDictionary
                        
                        let coupon = CouponGoHelper.sharedInstance.jsonToCoupon(obj)
                        
                        DataManager.sharedInstance.addCoupon(coupon)
                    }
                    
                    //self.couponList = DataManager.sharedInstance.getCoupons()
                    self.couponList = DataManager.sharedInstance.filtCoupons("")
                    self.couponsCollectionView.reloadData()
                    
                    CouponGoHelper.sharedInstance.removeLoadAnimation(parent: self.view)
                }
            } else if error != nil {
                CouponGoHelper.sharedInstance.showAlert(viewController: self, message: "error_server_error".localized(), animated: true, isError: true)
            }
        }
    }
    
    private func filterCoupons(){
        CouponGoHelper.sharedInstance.addLoadAnimation(parent: self.view)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500), execute: {
            
            //CouponGoHelper.sharedInstance.couponFilterStr = self.tfSearch.text!
            let dm = DataManager.sharedInstance
            
            self.couponList = dm.filtCoupons(self.tfSearch.text!)
           
            self.couponsCollectionView.reloadData()
            
            CouponGoHelper.sharedInstance.removeLoadAnimation(parent: self.view)
            
            CouponGoHelper.sharedInstance.removeNoResults(parent: self.couponsCollectionView)
            if self.couponList.count == 0 {
                CouponGoHelper.sharedInstance.addNoResults(parent: self.couponsCollectionView, yOffset: 30, text: "coupons_no_result".localized())
            }
        })
    }
}

extension CouponsViewController: CouponCallbackDelegate{
    func couponBackTouched() {
        self.presentedViewController?.dismiss(animated: true, completion: nil)
    }
}
