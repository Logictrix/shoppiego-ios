//
//  MarketViewController.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 07..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

class MarketViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var searchTextField: UIRoundedTextField!
    @IBOutlet weak var searchLabel: UILabel!
    @IBOutlet weak var iconScrollView: UIScrollView!
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet weak var couponScrollView: UIScrollView!
    @IBOutlet weak var couponsTableView: UITableView!
    
    @IBOutlet var separatorTopConstraint: NSLayoutConstraint!
    @IBOutlet var couponsTableHeightConstraint: NSLayoutConstraint!
    
    var filterView : UIView? = nil
    var coupons: Array<CGCoupon> = []
    var filterIdx = 0
    var iconRowButtons : Array<UIRoundedButton> = []
    var categories : Array<CGCategory> = []
    var categoryTitles : Dictionary<Int, String> = [:]
    var wasBack = false
    private var currentPage = 0
    private var hasMoreData = true
    private var searchTimer: Timer?
    private var isSearching = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        categories = DataManager.sharedInstance.getCategories()
        
        couponsTableView.isHidden = true
        couponsTableView.delegate = self
        couponsTableView.dataSource = self
        couponScrollView.delegate = self
        searchTextField.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    
        if !wasBack {
            searchTextField.text = ""
            updateSearchTextField(label: "All")
            showFilterView()
        }else{
          filterCoupons()
        }
        wasBack = false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return false
    }
    
    private func showFilterView(){
        iconScrollView.isHidden = true
        iconScrollView.frame = CGRect(x: iconScrollView.frame.origin.x, y: iconScrollView.frame.origin.y, width: iconScrollView.frame.width, height: 0)
        couponsTableView.isHidden = true
        separatorTopConstraint.constant = 0
        buildFilterView()
        couponScrollView.addSubview(filterView!)
        couponScrollView.contentSize = (filterView?.frame.size)!
        CouponGoHelper.sharedInstance.removeNoResults(parent: mainContentView)
    }
    
    private func showIconRow(){
        buildIconRow()
        
        iconScrollView.isHidden = false
        iconScrollView.frame = CGRect(x: iconScrollView.frame.origin.x, y: iconScrollView.frame.origin.y, width: iconScrollView.frame.width, height: 60)
        separatorTopConstraint.constant = iconScrollView.frame.height
        
        filterView?.removeFromSuperview()
        couponsTableView.isHidden = false
        couponScrollView.contentSize = (couponsTableView.frame.size)
        
        iconTouch(iconRowButtons[filterIdx])
    }
    
    private func makeIconRowButton(frame: CGRect, tag: Int, title: String = "", iconUrl: String = "") -> UIRoundedButton {
        let btn = UIRoundedButton(frame: frame)
        btn.cornerRadius = 20.0
        btn.borderColor = .black
        btn.borderWidth = 1.0
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 9, weight: .bold)
        btn.setTitleColor(.black, for: .normal)
        btn.setTitleColor(.white, for: .selected)
        btn.setTitle(title, for: .normal)
        btn.setTitle(title, for: .selected)
        if iconUrl != "" {
            btn.download(from: iconUrl, stateFor: .normal)
            btn.download(from: iconUrl, stateFor: .selected, needInvert: true)
        }
        btn.tag = tag
        btn.backgroundColor = .white
        btn.contentMode = .scaleAspectFit
        btn.imageView?.contentMode = .scaleAspectFit
        btn.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        btn.addTarget(self, action:#selector(iconTouch(_:)), for: .touchUpInside)
        
        return btn
    }
    
    private func buildIconRow(){
        let btnAll = makeIconRowButton(frame: CGRect(x: 20, y: 10, width: 40, height: 40), tag: 0, title: "All")
        iconScrollView.addSubview(btnAll)
        iconRowButtons.append(btnAll)
        
        var xOffs = btnAll.frame.origin.x
        for category in categories {
            
            xOffs += 45
            
            let btn = makeIconRowButton(frame: CGRect(x: xOffs, y: 10, width: 40, height: 40), tag: category.id, title: "", iconUrl: category.iconUrl)
            iconScrollView.addSubview(btn)
            iconRowButtons.append(btn)
            
            categoryTitles.updateValue(category.title, forKey: category.id)
        }
        xOffs += 65
        iconScrollView.contentSize = CGSize(width: xOffs, height: iconScrollView.contentSize.height)
    }
    
    @objc private func iconTouch(_ sender: Any){
        let btnSel = sender as! UIRoundedButton
        
        for btn in iconRowButtons {
            btn.isSelected = btn.tag == btnSel.tag
            btn.backgroundColor = btn.isSelected ? .red : .white
            btn.borderWidth = btn.isSelected ? 0 : 1
        }
        
        filterIdx = btnSel.tag
        updateSearchTextField(label: (btnSel.tag == 0 ? "market_search_all".localized() : (categoryTitles[btnSel.tag])!))
        filterCoupons()
    }
    
    private func buildFilterView(){
        if filterView != nil {
            return
        }
        
        couponScrollView.layoutIfNeeded()
        
        var h : CGFloat = 0.0
        var idx = 0
        let half = couponScrollView.frame.width / 2
        
        filterView = UIView(frame: CGRect(x: 0, y: 0, width: couponScrollView.frame.width, height: 0))
        
        for category in categories{
            let btn = UIRoundedButton(frame: CGRect(x: (idx % 2 == 0 ? 0 : half), y: CGFloat((idx / 2) * 120), width: half, height: 120))
            btn.layer.addBorder(edge: UIRectEdge.bottom, color: .black, thickness: 1.0)
            if (idx % 2) == 0{
                btn.layer.addBorder(edge: UIRectEdge.right, color: .black, thickness: 1.0)
            }
            btn.tag = idx + 1
            btn.addTarget(self, action:#selector(filterItemTouch(_:)), for: .touchUpInside)
            
            let imgView = UIImageView(frame: CGRect(x: (half - btn.frame.width) / 2, y: 24.0, width: btn.frame.width, height: 50))
            imgView.contentMode = .scaleAspectFit
            imgView.download(from: category.iconUrl)
            btn.addSubview(imgView)
            
            let label = UILabel(frame: CGRect(x: 0, y: 80, width: half, height: 20))
            label.text = category.title
            label.font = UIFont.systemFont(ofSize: 13, weight: .bold)
            label.textAlignment = NSTextAlignment.center
            //label.sizeToFit()
            btn.addSubview(label)
            
            filterView?.addSubview(btn)
            
            h += (idx % 2) == 0 ? 120 : 0
            idx += 1
        }
        
        let btn = UIRoundedButton()
        btn.frame = CGRect(x: half - 100, y: h + 20, width: 200, height: 40)
        btn.cornerRadius = 20.0
        btn.borderColor = .black
        btn.borderWidth = 1.0
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 9, weight: .bold)
        btn.setTitleColor(.black, for: .normal)
        btn.setTitle("market_all_coupons".localized(), for: .normal)
        btn.backgroundColor = .white
        btn.addTarget(self, action:#selector(filterItemTouch(_:)), for: .touchUpInside)
        btn.tag = 0
        filterView?.addSubview(btn)
        
        h += 80
        
        filterView?.frame = CGRect(x: 0, y: 0, width: couponScrollView.frame.width, height: h)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return coupons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = couponsTableView.dequeueReusableCell(withIdentifier: "MarketCouponCell", for: indexPath) as! MarketCouponCell
        
        cell.brandImage.download(from: coupons[indexPath.item].brandImageUrl)
        cell.brandName.text = coupons[indexPath.item].brandName
        cell.discountLabel.text = coupons[indexPath.item].title
        cell.validLabel.text = "market_valid".localized(args: CouponGoHelper.sharedInstance.convertDateToString(coupons[indexPath.item].validDate))
        cell.detailButton.tag = coupons[indexPath.item].id
        cell.detailButton.addTarget(self, action:#selector(couponDetailTouch(_:)), for: .touchUpInside)
        print("campaignId",coupons[indexPath.item].campaignId)
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.isEqual(couponScrollView) && !iconScrollView.isHidden {
            if scrollView.panGestureRecognizer.translation(in: scrollView.superview).y < 0 {
                if (scrollView.contentOffset.y + scrollView.bounds.size.height + couponsTableView.rowHeight > scrollView.contentSize.height)
                {
                    if hasMoreData && !isSearching {
                        filterCoupons(nextPage: true)
                    }
                }
            }
        }
    }
    
    func updateSearchTextField(label: String){
        searchLabel.text = label
        searchLabel.sizeToFit()
        searchLabel.frame = CGRect(x: searchLabel.frame.origin.x, y: searchLabel.frame.origin.y, width: searchLabel.frame.width, height: searchTextField.frame.height)
        
        searchTextField.paddingLeft = searchLabel.frame.width + 20
        searchTextField.layoutSubviews()
    }
    
    @objc private func filterItemTouch(_ sender: Any){
        let btn = sender as! UIRoundedButton
        
        filterIdx = btn.tag
        
        showIconRow()
        //filterCoupons()
    }
    
    @IBAction func searchTextFieldChanged(_ sender: Any) {
        searchTimer?.invalidate()
        searchTimer = Timer.scheduledTimer(timeInterval: Constants.SEARCH_DELAY, target: self, selector: #selector(timedFilter), userInfo: nil, repeats: false)
    }
    
    @objc func timedFilter(){
        if iconScrollView.isHidden {
            showIconRow()
        }
        
        filterCoupons()
    }
    
    private func refreshCouponsTable() {
        if currentPage == 0 {
            couponScrollView.scrollToTop()
        }
        
        couponsTableView.reloadData()
        
        couponsTableHeightConstraint.constant = (CGFloat(coupons.count) * couponsTableView.rowHeight) + (hasMoreData ? couponsTableView.rowHeight / 2 : 0)
        /*if couponsTableHeightConstraint.constant < couponScrollView.frame.height {
           couponsTableHeightConstraint.constant = couponScrollView.frame.height
        }*/
        couponScrollView.contentSize = CGSize(width: couponScrollView.contentSize.width, height: couponsTableHeightConstraint.constant)
        self.updateViewConstraints()
    }
    
    private func filterCoupons(nextPage: Bool = false){
        isSearching = true
        
        var tmpCoupons: Array<CGCoupon> = []
        
        CouponGoHelper.sharedInstance.removeNoResults(parent: self.mainContentView)
        CouponGoHelper.sharedInstance.addLoadAnimation(parent: self.view)
        
        var page = 0
        if nextPage {
            page = currentPage + 1
            tmpCoupons = coupons
        }
        else {
            page = 0
            hasMoreData = true
            //coupons.removeAll()
            refreshCouponsTable()
        }
        
        var q : [String : String] = ["page": String(format: "%d", page)]
        if !(searchTextField.text ?? "").isEmpty {
            q.updateValue(searchTextField.text!, forKey: "keyword")
        }
        if filterIdx > 0 {
            q.updateValue(String(format: "%d", filterIdx), forKey: "category_id")
        }
        
        CouponGoHelper.sharedInstance.commManager.request(method: "GET", url: "/coupon/list/market", queryData: q, postData: nil, delayInMilliseconds: 500) { (result, error) in
            if let result = result {
                DispatchQueue.main.async {
                    
                    let dict = result as NSDictionary
                    let couponList = dict.value(forKey: "coupon_list") as! NSArray
                    for item in couponList{
                        let obj = item as! NSDictionary
                        
                        let coupon = CouponGoHelper.sharedInstance.jsonToCoupon(obj)
                        
                        tmpCoupons.append(coupon)
                    }
                    
                    if tmpCoupons.count > 0 {
                        self.currentPage = page
                    }
                    self.hasMoreData = tmpCoupons.count == 5
                    
                    self.coupons = tmpCoupons
                    self.refreshCouponsTable()
                    
                    CouponGoHelper.sharedInstance.removeLoadAnimation(parent: self.view)
                    self.isSearching = false
                    
                    if self.coupons.count == 0 {
                        CouponGoHelper.sharedInstance.addNoResults(parent: self.mainContentView, yOffset: 90, text: "market_no_result".localized())
                    }
                }
            }
            else if error != nil {
                CouponGoHelper.sharedInstance.removeLoadAnimation(parent: self.view)
                self.isSearching = false
                
                CouponGoHelper.sharedInstance.showAlert(viewController: self, message: "error_server_error".localized(), animated: true, isError: true)
            }
        }
    }
    
    @objc private func couponDetailTouch(_ sender: Any){
        let btn = sender as! UIRoundedButton
        
        CouponGoHelper.sharedInstance.commManager.request(method: "GET", url: String(format: "/coupon/%d/market", btn.tag), queryData: nil, postData: nil) { (result, error) in
            if let result = result {
                let dict = result as NSDictionary
                let obj = dict.value(forKey: "coupon") as! NSDictionary
                print("obj_obj",obj)
                let coupon = CouponGoHelper.sharedInstance.jsonToCoupon(obj)
                
                DispatchQueue.main.async {
                    let vc = CouponGoHelper.sharedInstance.getViewController(viewName: Constants.VC_COUPON) as! CouponViewController
                    vc.delegate = self
                    vc.coupon = coupon
                    self.present(vc, animated: true, completion: nil)
                }
            }
            else if error != nil {
                CouponGoHelper.sharedInstance.showAlert(viewController: self, message: "error_server_error".localized(), animated: true, isError: true)
            }
        }
    }
}

extension MarketViewController: CouponCallbackDelegate{
    func couponBackTouched() {
        self.presentedViewController?.dismiss(animated: true, completion: nil)
        wasBack = true
    }
}
