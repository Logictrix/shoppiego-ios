//
//  TestViewController.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 04. 15..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit
import WebKit


protocol CouponCallbackDelegate {
    func couponBackTouched()
}

class CouponViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,WKNavigationDelegate {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var redeemView: UIView!
    @IBOutlet weak var detailsView: UIView!
    @IBOutlet weak var ownedView: UIView!
    @IBOutlet weak var redeemedView: UIView!
    @IBOutlet weak var marketView: UIView!
    @IBOutlet weak var availableView: UIView!
    
    @IBOutlet var headerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var contentViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var redeemViewTopConstraint: NSLayoutConstraint!
    @IBOutlet var redeemViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var detailsViewTopConstraint: NSLayoutConstraint!
    @IBOutlet var detailsViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var validViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var validLabelLeadingConstraint: NSLayoutConstraint!
    @IBOutlet var validLabelTopConstraint: NSLayoutConstraint!
    @IBOutlet var webshopViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var ownedViewTopConstraint: NSLayoutConstraint!
    @IBOutlet var ownedViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var redeemedViewTopConstraint: NSLayoutConstraint!
    @IBOutlet var redeemedViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var redeemedTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var marketViewTopConstraint: NSLayoutConstraint!
    @IBOutlet var marketViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var availableViewTopConstraint: NSLayoutConstraint!
    @IBOutlet var availableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var availableTableViewHeightConstraint: NSLayoutConstraint!
  //  @IBOutlet var webViewHight: NSLayoutConstraint!
    
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var brandImage: UIRoundedImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var validView: UIView!
    @IBOutlet weak var alertView: UIRoundedView!
    @IBOutlet weak var expireLabel: UILabel!
    @IBOutlet weak var validDateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var webshopView: UIView!
    @IBOutlet weak var webshopTextView: UITextView!
    @IBOutlet weak var couponCountLabel: UILabel!
    @IBOutlet weak var marketCountLabel: UILabel!
    @IBOutlet weak var redeemedTableView: UITableView!
    @IBOutlet weak var availableTableView: UITableView!
    @IBOutlet weak var webView: WKWebView!
 //   var webView: WKWebView!
    
    var delegate: CouponCallbackDelegate?
    
    private let redeemedSimpleRowHeight = 50.0
    private let redeemedBarcodeRowHeight = 214.0
    private let availableRowHeight = 54.0
    
    var coupon: CGCoupon?
    var isOwn = false
    var webViewHight: CGFloat = 100.0

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        redeemedTableView.delegate = self
        redeemedTableView.dataSource = self
        availableTableView.delegate = self
        availableTableView.dataSource = self
        guard let coup = coupon else { return }
        
        // Header view
        if !coup.coverImageurl.isEmpty{
            backgroundImage.download(from: coup.coverImageurl)
            backgroundImage.contentMode = .scaleToFill
            backgroundImage.clipsToBounds = true
        }
        else{
            backgroundImage.image = UIImage(named: String(format: "coupon-bg-%d", (coup.id % 6)))
        }
        
        brandImage.download(from: coup.brandImageUrl)
        titleLabel.text = (coup.isFixed ? "coupon_fix_discount".localized() : "coupon_percentage_discount".localized(args: coup.title))
        
        
        let url = URL(string: coup.descriptionUrl)
        let requestObj = URLRequest(url: url! as URL)
        webView.load(requestObj)
        webView.navigationDelegate = self
        webView.clipsToBounds = true
        webView.scrollView.isScrollEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.layoutIfNeeded()
      // new change
      //  refresh()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //refresh()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //refresh()
    }
    
    private func refresh(){
        guard let coup = coupon else { return }
        let remainingDays = CouponGoHelper.sharedInstance.getDaysFromNow(date: coup.validDate)
        
        // Header view
        if !coup.coverImageurl.isEmpty{
            backgroundImage.download(from: coup.coverImageurl)
            backgroundImage.contentMode = .scaleToFill
            backgroundImage.clipsToBounds = true
        }
        else{
            backgroundImage.image = UIImage(named: String(format: "coupon-bg-%d", (coup.id % 6)))
        }
        
        brandImage.download(from: coup.brandImageUrl)
        titleLabel.text = (coup.isFixed ? "coupon_fix_discount".localized() : "coupon_percentage_discount".localized(args: coup.title))
        
        // Redeem view
        redeemView.isHidden = !isOwn || coup.freeCount == 0 || remainingDays < 0
        
        // Details view
        validView.isHidden = coup.validDate == nil
        if !validView.isHidden {
            validDateLabel.text = "coupon_valid".localized(args: CouponGoHelper.sharedInstance.convertDateToString(coup.validDate))
            
            
            if remainingDays > Constants.VALID_DAYS_FROM_NOW {
                alertView.isHidden = true
                expireLabel.isHidden = true
                validLabelLeadingConstraint.constant = 0.0
                validLabelTopConstraint.constant = 0.0
                validViewHeightConstraint.constant = validDateLabel.frame.height
            }
            else{
                alertView.isHidden = false
                expireLabel.isHidden = false
                expireLabel.text = (remainingDays < 0 ? "coupon_expired" : "coupon_expire_soon").localized()
                validLabelLeadingConstraint.constant = 38.0
                validLabelTopConstraint.constant = 17.0
                validViewHeightConstraint.constant = alertView.frame.height
            }
        }
        detailsViewHeightConstraint.constant -= descriptionLabel.frame.height
        descriptionLabel.text = coup.description
        descriptionLabel.setLineSpacing(lineSpacing: 10.0)
        descriptionLabel.setCorrectLabelFrameHeightWithWebView(hight: webViewHight)
    //
        // new change
        detailsViewHeightConstraint.constant += descriptionLabel.frame.height
       
//          webViewHight.constant = 500
//          webView.layoutIfNeeded()
//          detailsViewHeightConstraint.constant += webView.frame.height
     
        if coup.webshop.isEmpty {
            webshopView.isHidden = true
        }
        else {
            webshopView.isHidden = false
            webshopTextView.text = coup.webshop
        }
        
        // Owned view
        ownedView.isHidden = !isOwn
        if !ownedView.isHidden {
            couponCountLabel.text = String(format: "%d", coup.freeCount)
            let marketCnt = coup.count - coup.freeCount - coup.redeemedItems.count
            marketCountLabel.text = marketCnt > 0 ? "coupon_in_market".localized(args: marketCnt) : ""
        }
        
        // Redeemed view
        redeemedView.isHidden = !isOwn || coup.redeemedItems.count == 0
        let redeemedHeightDiff = redeemedViewHeightConstraint.constant - redeemedTableViewHeightConstraint.constant
        redeemedTableViewHeightConstraint.constant = 0
        for itm in coup.redeemedItems {
            redeemedTableViewHeightConstraint.constant += CGFloat(itm.codeImageUrl.isEmpty ? redeemedSimpleRowHeight : redeemedBarcodeRowHeight)
        }
        redeemedViewHeightConstraint.constant = redeemedHeightDiff + redeemedTableViewHeightConstraint.constant
        
        // Market view
        marketView.isHidden = !isOwn || coup.isUnlimited || coup.freeCount == 0
        
        // MarketList view
        availableView.isHidden = coup.isUnlimited || coup.availableItems.count == 0
        availableTableViewHeightConstraint.constant = CGFloat(coup.availableItems.count) * availableTableView.rowHeight
        
        redeemViewHeightConstraint.isActive = !redeemView.isHidden
        webshopViewHeightConstraint.isActive = !webshopView.isHidden
        detailsViewHeightConstraint.isActive = !detailsView.isHidden
        ownedViewHeightConstraint.isActive = !ownedView.isHidden
        redeemedViewHeightConstraint.isActive = !redeemedView.isHidden
        marketViewHeightConstraint.isActive = !marketView.isHidden
        availableViewHeightConstraint.isActive = !availableView.isHidden
        
        redeemViewTopConstraint.constant = headerViewHeightConstraint.getActiveConstant()
        detailsViewTopConstraint.constant = redeemViewTopConstraint.constant + redeemViewHeightConstraint.getActiveConstant()
        ownedViewTopConstraint.constant = detailsViewTopConstraint.constant +
            detailsViewHeightConstraint.getActiveConstant()
        redeemedViewTopConstraint.constant = ownedViewTopConstraint.constant +
            ownedViewHeightConstraint.getActiveConstant()
        marketViewTopConstraint.constant = redeemedViewTopConstraint.constant +
            redeemedViewHeightConstraint.getActiveConstant()
        availableViewTopConstraint.constant = marketViewTopConstraint.constant +
            marketViewHeightConstraint.getActiveConstant()
        
        
        contentViewHeightConstraint.constant = headerViewHeightConstraint.getActiveConstant()
        contentViewHeightConstraint.constant += redeemViewHeightConstraint.getActiveConstant()
        contentViewHeightConstraint.constant += detailsViewHeightConstraint.getActiveConstant()
        contentViewHeightConstraint.constant += ownedViewHeightConstraint.getActiveConstant()
        contentViewHeightConstraint.constant += redeemedViewHeightConstraint.getActiveConstant()
        contentViewHeightConstraint.constant += marketViewHeightConstraint.getActiveConstant()
        contentViewHeightConstraint.constant += availableViewHeightConstraint.getActiveConstant()
        contentViewHeightConstraint.constant += 24.0
        
        
        // VISHNU
      //  webView = WKWebView(frame: descriptionLabel.frame)
        //self.detailsView.addSubview(webView)
        self.updateViewConstraints()
    
    }
    
    // WEB
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation: WKNavigation!){
         CouponGoHelper.sharedInstance.addLoadAnimation(parent: self.view)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
//        webView.frame.size.height = 1
//        webView.frame.size = webView.scrollView.contentSize
      //  webViewHight.constant = webView.scrollView.contentSize.height
      //  detailsViewHeightConstraint.constant += webViewHight.constant
       //  self.updateViewConstraints()
        webViewHight = webView.scrollView.contentSize.height
        self.refresh()
        CouponGoHelper.sharedInstance.removeLoadAnimation(parent: self.view)
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //if tableView.restorationIdentifier == "AvailableCoupons RedeemedCoupons" {
        return tableView.restorationIdentifier == "RedeemedCoupons" ? coupon!.redeemedItems.count : coupon!.availableItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let coup = coupon!
        
        if tableView.restorationIdentifier == "RedeemedCoupons" {
            let itm = coup.redeemedItems[indexPath.item]
            if itm.codeImageUrl.isEmpty {
                let cell = redeemedTableView.dequeueReusableCell(withIdentifier: "RedeemedCouponSimpleCell") as! RedeemedCouponSimpleCell
                
                cell.couponCode.text = itm.code
                cell.redeemDate.text = CouponGoHelper.sharedInstance.convertDateToString(itm.date)
                
                return cell
            }
            else {
                let cell = redeemedTableView.dequeueReusableCell(withIdentifier: "RedeemedCouponBarcodeCell") as! RedeemedCouponBarcodeCell
                
                cell.couponCode.text = itm.code
                cell.redeemDate.text = CouponGoHelper.sharedInstance.convertDateToString(itm.date)
                cell.barcodeImage.download(from: itm.codeImageUrl)
                
                return cell
            }
        }
        else {
            let itm = coup.availableItems[indexPath.item]
            let cell = availableTableView.dequeueReusableCell(withIdentifier: "AvailableCell", for: indexPath) as! AvailableCell
            
            if itm.profilePictureUrl.isEmpty {
                cell.avatar.image = UIImage(named: "avatar-sml-male")
            }
            else {
                cell.avatar.download(from: itm.profilePictureUrl)
            }
            cell.name.text = itm.nickname
            cell.button.tag = indexPath.item
            if itm.isFree {
                cell.free.isHidden = false
                cell.button.setImage(UIImage(named: "sign-pipe-sml-black"), for: .normal)
                cell.button.setTitle("coupon_available_collect".localized(), for: .normal)
                cell.button.addTarget(self, action: #selector(acquireTouch), for: .touchUpInside)
                
            }
            else {
                cell.free.isHidden = true
                cell.button.setImage(UIImage(named: "flash-black"), for: .normal)
                cell.button.setTitle("coupon_available_chat".localized(), for: .normal)
                cell.button.addTarget(self, action: #selector(chatTouch), for: .touchUpInside)
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView.restorationIdentifier == "RedeemedCoupons" {
            if let coup = coupon {
                let itm = coup.redeemedItems[indexPath.item]
                return CGFloat(itm.codeImageUrl.isEmpty ? redeemedSimpleRowHeight : redeemedBarcodeRowHeight)
            }
        }
        else {
            return CGFloat(availableRowHeight)
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.restorationIdentifier == "RedeemedCoupons" {
            if let coup = coupon?.redeemedItems[indexPath.item]{
                UIPasteboard.general.string = coup.code
                
                CouponGoHelper.sharedInstance.showAlert(viewController: self, message: "coupon_code_copied".localized())
            }
        }
    }
    
    @objc func acquireTouch(_ sender: Any) {
        let btn = sender as! UIRoundedButton
        
        let p: [String: String] = ["user_id": String(format: "%d", coupon!.availableItems[btn.tag].userId)]
        CouponGoHelper.sharedInstance.commManager.requestWithPUT(method: "PUT", url: String(format: "/coupon/%d/market/get-it-free", coupon!.id), queryData: nil, postData: p) { (result, error) in
            if result != nil {
                DispatchQueue.main.async {
                    self.coupon!.availableItems.remove(at: btn.tag)
                    self.availableTableView.reloadData()
                }
            }
            else if error != nil {
                CouponGoHelper.sharedInstance.showAlert(viewController: self, message: "error_server_error".localized(), animated: true, isError: true)
            }
        }
    }
    
    @objc func chatTouch(_ sender: Any) {
        let btn = sender as! UIRoundedButton
        
        let vc = CouponGoHelper.sharedInstance.getViewController(viewName: Constants.VC_CHAT) as! ChatViewController
        vc.partnerUserId = coupon!.availableItems[btn.tag].userId
        vc.partnerProfilePictureUrl = coupon!.availableItems[btn.tag].profilePictureUrl
        vc.partnerNickname = coupon!.availableItems[btn.tag].nickname
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func backTouch(_ sender: Any) {
        delegate?.couponBackTouched()
    }
    
    @IBAction func redeemTouch(_ sender: Any) {
        CouponGoHelper.sharedInstance.commManager.request(method: "POST", url: String(format: "/coupon/%d/redeem", coupon!.id), queryData: nil, postData: nil) { (result, error) in
            if let result = result {
                DispatchQueue.main.async {
                    self.coupon?.freeCount -= 1
                    
                    let dict = result as NSDictionary
                    let obj = dict.value(forKey: "coupon") as! NSDictionary
                    
                    let redeemedCoupon = CGCouponRedeemedItem()
                    redeemedCoupon.code = CouponGoHelper.sharedInstance.jsonToString(obj.value(forKey: "code"))
                    redeemedCoupon.codeImageUrl = CouponGoHelper.sharedInstance.jsonToString(obj.value(forKey: "code_image_url"))
                    redeemedCoupon.date = CouponGoHelper.sharedInstance.jsonToDateTime(obj.value(forKey: "redeemed_at"))
                    
                    self.coupon!.redeemedItems.append(redeemedCoupon)
                    self.redeemedTableView.reloadData()
                    self.refresh()
                    
                    let vc = CouponGoHelper.sharedInstance.getViewController(viewName: Constants.VC_COUPON_REDEEM)! as! CouponRedeemViewController
                    vc.code = redeemedCoupon.code
                    vc.codeImageUrl = redeemedCoupon.codeImageUrl
                    vc.webshop = self.coupon?.webshop ?? ""
                    self.present(vc, animated: true, completion: nil)
                }
            }
            else if error != nil {
                CouponGoHelper.sharedInstance.showAlert(viewController: self, message: "error_server_error".localized(), animated: true, isError: true)
            }
        }
    }
    
    @IBAction func goToWebshopTouch(_ sender: Any) {
        if let ws = coupon?.webshop {
            if let url = URL(string: ws) {
                UIApplication.shared.open(url, options: [:])
            }
        }
    }
    
    @IBAction func offerTouch(_ sender: Any) {
        let vc = CouponGoHelper.sharedInstance.getViewController(viewName: Constants.VC_COUPON_OFFER)! as! CouponOfferViewController
        vc.isFree = true
        vc.coupon = self.coupon
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func swapTouch(_ sender: Any) {
        let vc = CouponGoHelper.sharedInstance.getViewController(viewName: Constants.VC_COUPON_OFFER)! as! CouponOfferViewController
        vc.isFree = false
        vc.coupon = self.coupon
        self.present(vc, animated: true, completion: nil)
    }
    
    func successOffer(count: Int, isFree: Bool){
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(Constants.POPUP_ASYNC_AFTER_TIME)) {
            let vc = CouponGoHelper.sharedInstance.getViewController(viewName: Constants.VC_COUPON_SUCCESS)! as! CouponSuccessViewController
            vc.isFree = isFree
            vc.count = count
            self.present(vc, animated: true, completion: nil)
            
            self.coupon?.freeCount -= count
            self.refresh()
        }
    }
}
