//
//  ErrorViewController.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 15..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

class AlertViewController: UIViewController {

    @IBOutlet var alertView: UIRoundedView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var messageLabel: UILabel!
    
    var isError: Bool = false
    var alertTitle: String = ""
    var alertMessage: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        titleLabel.text = alertTitle
        messageLabel.text = alertMessage
        if isError {
            alertView.backgroundColor = UIColor(red: 241, green: 79, blue: 80, alpha: 255)
            titleLabel.textColor = .white
            messageLabel.textColor = .white
        }
        else {
            alertView.backgroundColor = .white
            titleLabel.textColor = .black
            messageLabel.textColor = .black
        }
    }
    
    @IBAction func closeTouch(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
}
