//
//  MapViewController.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 02. 22..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit
import GoogleMaps
import HDAugmentedReality

class MapViewController: UIViewController, CLLocationManagerDelegate, GMUClusterManagerDelegate, GMSMapViewDelegate, GMUClusterRendererDelegate {
    @IBOutlet weak var mapView: UIView!
    @IBOutlet weak var startPointButton: UIButton!
    @IBOutlet weak var keyCountLabel: UILabel!
    
    private var isMapReady : Bool = false
    private var coupons : Dictionary<Int, CGCoupon> = [:]
    private var keys : Dictionary<Int, CGKey> = [:]
    private var autoOpenCoord: CLLocationCoordinate2D? = nil
    
    var gmaps: GMSMapView!
    var clusterManager: GMUClusterManager!
    var iconGenerator: MapClusterIconGenerator!
    var algorithm: GMUNonHierarchicalDistanceBasedAlgorithm!
    var renderer: GMUDefaultClusterRenderer!
    
    fileprivate var arViewController: ARViewController!
    var selectedItem: CGItem?
    private var wasPopup = false
    private var updateGroup = DispatchGroup()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isMapReady = false
        
        var lat = Constants.DEFAULT_LAT
        var long = Constants.DEFAULT_LONG
        if CouponGoHelper.sharedInstance.mapUserLocation != nil {
            lat = CouponGoHelper.sharedInstance.mapUserLocation!.coordinate.latitude
            long = CouponGoHelper.sharedInstance.mapUserLocation!.coordinate.longitude
        }
        
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: (DataManager.sharedInstance.getAppFloat(Constants.APP_DEFAULT_ZOOM) ?? Float(Constants.DEFAULT_ZOOM)))
        gmaps = GMSMapView.map(withFrame: view.frame, camera: camera)
        
        do {
            if let styleURL = Bundle.main.url(forResource: "map_style", withExtension: "json"){
                gmaps.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            }
            else{
                NSLog("Unable to find map_style.json")
            }
        }
        catch{
            NSLog("One or more of the map styles failed to load")
        }
        gmaps.isMyLocationEnabled = true
        gmaps.settings.compassButton = false
        gmaps.settings.myLocationButton = false
        gmaps.setMinZoom(DataManager.sharedInstance.getAppFloat(Constants.APP_MIN_ZOOM) ?? Constants.MIN_ZOOM, maxZoom: DataManager.sharedInstance.getAppFloat(Constants.APP_MAX_ZOOM) ?? Constants.MAX_ZOOM)
        gmaps.settings.tiltGestures = false
        gmaps.delegate = self
        
        mapView.addSubview(gmaps)
        
        //let iconGenerator = GMUDefaultClusterIconGenerator(buckets: [ 10, 20, 50, 100, 200, 500, 1000])
        iconGenerator = MapClusterIconGenerator()
        algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        renderer = GMUDefaultClusterRenderer(mapView: gmaps, clusterIconGenerator: iconGenerator)
        //renderer.animatesClusters = false
        renderer.animationDuration = 0.3
        renderer.minimumClusterSize = 2
        renderer.delegate = self
        
        clusterManager = GMUClusterManager(map: gmaps, algorithm: algorithm, renderer: renderer)
        clusterManager.cluster()
        
        //changeCameraButton.isHidden = true
        
        // TODO teszt begin
        /*let tstCoupon = CGCoupon()
        tstCoupon.id = 1
        tstCoupon.campaignId = 6
        tstCoupon.brandImageUrl = "https://testcdn.shoppiego.hu/img/brand/63.png?965643b6524b36991505e677f466ea7a"
        tstCoupon.lat = 47.333900
        tstCoupon.lng = 19.044100
        tstCoupon.title = "30%"
        tstCoupon.count = 10
        tstCoupon.requiredKeyCount = 2
        tstCoupon.isUnlimited = true
        //tstCoupon.openAt = CouponGoHelper.sharedInstance.convertStringToDateTime(string: "2019-04-23 12:00:00")
        
        let position = CLLocationCoordinate2DMake(tstCoupon.lat, tstCoupon.lng)
        let markerItem = CGMarkerItem(position: position, item: tstCoupon)
        tstCoupon.marker = markerItem
        
        self.coupons[tstCoupon.id] = tstCoupon
        self.clusterManager.add(markerItem)*/

        /*let tstCoupon = CGKey()
         tstCoupon.id = 1
         tstCoupon.count = 20
         tstCoupon.lat = 47.333900
         tstCoupon.lng = 19.044100
        
        let position = CLLocationCoordinate2DMake(tstCoupon.lat, tstCoupon.lng)
        let markerItem = CGMarkerItem(position: position, item: tstCoupon)
        tstCoupon.marker = markerItem
        
        self.keys[tstCoupon.id] = tstCoupon
        self.clusterManager.add(markerItem)*/
        // TODO teszt end
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateKeyCountLabel()
        handleLocationManagerStatus()
        
        if CouponGoHelper.sharedInstance.mapUserLocation != nil {
            handleAutoOpen()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        CouponGoHelper.sharedInstance.locationManager.delegate = nil
    }
    
    private func handleAutoOpen(){
        if CouponGoHelper.sharedInstance.launchParameters != nil {
            let lp = CouponGoHelper.sharedInstance.launchParameters as! NSDictionary
            if "cg_drop" == CouponGoHelper.sharedInstance.jsonToString(lp.value(forKey: "type")) {
                let lat = CouponGoHelper.sharedInstance.jsonToDouble(lp.value(forKey: "lat"))
                let lng = CouponGoHelper.sharedInstance.jsonToDouble(lp.value(forKey: "lng"))
                autoOpenCoord = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                gmaps.animate(toLocation: autoOpenCoord!)
            }
            CouponGoHelper.sharedInstance.launchParameters = nil
        }
    }
    
    private func handleLocationManagerStatus(){
        if CLLocationManager.locationServicesEnabled() {
            CouponGoHelper.sharedInstance.locationManager.delegate = self
            if CLLocationManager.authorizationStatus() == .notDetermined {
                startPointButton.isHidden = true
            }
            else {
                CouponGoHelper.sharedInstance.locationManager.startUpdatingLocation()
                if CLLocationManager.authorizationStatus() == .authorizedWhenInUse{
                    CouponGoHelper.sharedInstance.locationManager.allowsBackgroundLocationUpdates = false
                    CouponGoHelper.sharedInstance.locationManager.pausesLocationUpdatesAutomatically = false
                    startPointButton.isHidden = false
                }
                else if CLLocationManager.authorizationStatus() == .authorizedAlways{
                    CouponGoHelper.sharedInstance.locationManager.allowsBackgroundLocationUpdates = true
                    CouponGoHelper.sharedInstance.locationManager.pausesLocationUpdatesAutomatically = true
                    if #available(iOS 11.0, *) {
                        CouponGoHelper.sharedInstance.locationManager.showsBackgroundLocationIndicator = false
                    }
                    startPointButton.isHidden = false
                }
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let lastLoc = locations.last {
            if CouponGoHelper.sharedInstance.mapUserLocation == nil && CouponGoHelper.sharedInstance.launchParameters == nil {
                let coord = lastLoc.coordinate
                gmaps.animate(toLocation: lastLoc.coordinate)
                
                //napi max egyszer a user koordinátáinak elküldése a szerverre
                let storedLastDate = DataManager.sharedInstance.getSessionString(Constants.SESSION_LAST_LOCATION_DATE)
                let lastDate = CouponGoHelper.sharedInstance.convertDateToString(Date())
                if lastDate != storedLastDate {
                    let p : [String : String] = ["lat": String(format: "%f", coord.latitude),
                                                 "long": String(format: "%f", coord.longitude)]
                    CouponGoHelper.sharedInstance.commManager.request(method: "POST", url: "/user/location", queryData: nil, postData: p) { (result, error) in
                        if result != nil {
                            DispatchQueue.main.async {
                                DataManager.sharedInstance.setSession(Constants.SESSION_LAST_LOCATION_DATE, value: lastDate)
                            }
                        }
                        else if error != nil {
                            
                        }
                    }
                }
            }
        }
        CouponGoHelper.sharedInstance.mapUserLocation = locations.last
        if let coord = locations.last?.coordinate {
            DataManager.sharedInstance.setSession(Constants.SESSION_LAST_LOCATION_LAT, value: coord.latitude)
            DataManager.sharedInstance.setSession(Constants.SESSION_LAST_LOCATION_LNG, value: coord.longitude)
        }
    
        handleAutoOpen()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        handleLocationManagerStatus()
        
        if gmaps != nil && !startPointButton.isHidden {
            gmaps.isMyLocationEnabled = true
        }
    }
    
    func renderer(_ renderer: GMUClusterRenderer, markerFor object: Any) -> GMSMarker? {
        let marker = GMSMarker()
        if let markerItem = object as? CGMarkerItem {
            if let couponItem = markerItem.item as? CGCoupon {
                let isGold = couponItem.requiredKeyCount == 5
                let isLocked = couponItem.requiredKeyCount > 0
                let imgName = (isGold ? "mrkr-gold" : "mrkr-simple") + (isLocked ? "-locked" : "") + (couponItem.isUnlimited ? "-unlimited" : "")
                
                marker.icon = UIImage(named: imgName)
                CouponGoHelper.sharedInstance.downloadAndCacheImage(link: couponItem.brandImageUrl) { (brandImage) in
                    DispatchQueue.main.async {
                        if couponItem.isUnlimited {
                            marker.icon = MapMarkerIconGenerator.getCouponIcon(backgroundImage: UIImage(named: imgName)!, brandImage: brandImage, brandRect: CGRect(x: 46, y: 35, width: 21, height: 21))
                        }
                        else {
                            let lblRect = (isGold ? CGRect(x: 81, y: 59, width: 33, height: 10) : CGRect(x: 78, y: (isLocked ? 57 : 50), width: 33, height: 10))
                            let lblText = (couponItem.count > 20 ? "20+" : String(format: "%d", couponItem.count))
                            let lblColor = (isGold ? UIColor.black : UIColor.white)
                            let brandRect = (isGold ? CGRect(x: 50, y: 36, width: 21, height: 21) : CGRect(x: 46, y: 35, width: 21, height: 21))
                            marker.icon = MapMarkerIconGenerator.getCouponIcon(backgroundImage: UIImage(named: imgName)!, brandImage: brandImage, brandRect: brandRect, labelRect: lblRect, labelText: lblText, labelColor: lblColor)
                        }
                    }
                }
            }
            else if let keyItem = markerItem.item as? CGKey {
                let txt = keyItem.count > 20 ? "20+" : String(format: "%d", keyItem.count)
                marker.icon = MapMarkerIconGenerator.getKeyIcon(image: UIImage(named: "mrkr-key")!, text: txt)
            }
        }
        
        return marker
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let markerItem = marker.userData as? CGMarkerItem {
            guard let item = markerItem.item as? CGItem else { return false }
            
            if (item as? CGCoupon)?.isUnlimited ?? false {
                itemTapped(markerItem)
            } else {
                CouponGoHelper.sharedInstance.commManager.request(method: "GET", url: String(format: "/%@/%d/map", item.type, item.id), queryData: nil, postData: nil) { (result, error) in
                    if let result = result {
                        let obj = result as NSDictionary
                        let cnt = CouponGoHelper.sharedInstance.jsonToInt(obj.value(forKey: "count"))
                        
                        if let rMarker = item.marker as? GMUClusterItem {
                            if cnt <= 0 {
                                if let _ = item as? CGCoupon {
                                    self.coupons.removeValue(forKey: item.id)
                                    self.clusterManager.remove(rMarker)
                                }
                                    
                                else if let _ = item as? CGKey {
                                    self.keys.removeValue(forKey: item.id)
                                    self.clusterManager.remove(rMarker)
                                }
                            }
                            else{
                                if let _ = item as? CGCoupon {
                                    self.coupons[item.id]?.count = cnt
                                }
                                else if let _ = item as? CGKey {
                                    self.keys[item.id]?.count = cnt
                                }
                            }
                            
                            DispatchQueue.main.async {
                                self.clusterManager.cluster()
                                
                                if cnt > 0 {
                                    self.itemTapped(markerItem)
                                }
                            }
                        }
                        
                    }
                    else if let error = error {
                        var errShowed = false
                        let err = error as NSError
                        if err.code == 0 {
                            let dict = err.userInfo as NSDictionary
                            let errCode = CouponGoHelper.sharedInstance.jsonToInt(dict.value(forKey: "code"))
                            if errCode == 2000 {
                                if item is CGCoupon {
                                    CouponGoHelper.sharedInstance.showAlert(viewController: self, message: "map_empty_drop_chest".localized(), animated: true, isError: true)
                                    errShowed = true
                                }
                                else if item is CGKey {
                                    CouponGoHelper.sharedInstance.showAlert(viewController: self, message: "map_empty_drop_keys".localized(), animated: true, isError: true)
                                    errShowed = true
                                }
                            }
                            else {
                                let errMsg = CouponGoHelper.sharedInstance.jsonToString(dict.value(forKey: "error"))
                                CouponGoHelper.sharedInstance.showAlert(viewController: self, message: errMsg, animated: true, isError: true)
                                errShowed = true
                            }
                        }
                        
                        if !errShowed {
                            CouponGoHelper.sharedInstance.showAlert(viewController: self, message: "error_server_error".localized(), animated: true, isError: true)
                        }
                    }
                }
            }
        }
        
        return true
    }
    
    private func showARView() {
        arViewController = ARViewController()
        arViewController.dataSource = self
        arViewController.presenter.maxVisibleAnnotations = 30
        arViewController.definesPresentationContext = true
        arViewController.uiOptions.closeButtonEnabled = false
        
        let trackingManager = arViewController.trackingManager
        trackingManager.userDistanceFilter = 15
        trackingManager.reloadDistanceFilter = 50
        
        let closeBtn = UIRoundedButton()
        closeBtn.frame = CGRect(x: 23, y: 14, width: 32, height: 32)
        closeBtn.cornerRadius = 16
        closeBtn.setImage(UIImage(named: "sign-close-mid-white"), for: .normal)
        closeBtn.backgroundColor = .black
        closeBtn.addTarget(self, action: #selector(arviewCloseTouch(_:)), for: .touchUpInside)
        arViewController.view.addSubview(closeBtn)
        
        let cameraBtn = UIRoundedButton()
        cameraBtn.frame = CGRect(x: self.view.frame.width - 103, y: 14, width: 80, height: 32)
        cameraBtn.cornerRadius = 16
        cameraBtn.setImage(UIImage(named: "change-camera"), for: .normal)
        cameraBtn.backgroundColor = .white
        cameraBtn.addTarget(self, action: #selector(arviewCameraTouch(_:)), for: .touchUpInside)
        arViewController.view.addSubview(cameraBtn)
        
        let location = CLLocation(coordinate: CLLocationCoordinate2D(latitude: self.selectedItem!.lat, longitude: self.selectedItem!.lng), altitude: 0, horizontalAccuracy: 1, verticalAccuracy: 1, course: 0, speed: 0, timestamp: Date())
        let annotation = ARAnnotation(identifier: "\(self.selectedItem!.id)", title: "title", location: location)
        arViewController.setAnnotations([annotation!])
        self.present(arViewController, animated: false, completion: nil)
    }
    
    private func closeARView(){
        self.arViewController.dismiss(animated: false, completion: nil)
        self.arViewController = nil
    }
    
    @objc private func arviewCloseTouch(_ sender:UIButton!){
        DispatchQueue.main.async {
            self.closeARView()
        }
    }
    
    @objc private func arviewCameraTouch(_ sender:UIButton!){
        DispatchQueue.main.async {
            self.closeARView()
            self.changeCamera()
        }
    }
    
    private func showItem(){
        let isOn = DataManager.sharedInstance.getSessionBool(Constants.SESSION_AR_MODE, defaultValue: true)
        if isOn{
            self.showARView()
        } else {
            let vc = CouponGoHelper.sharedInstance.getViewController(viewName: Constants.VC_MAP_WANT_IT) as! MapWantItViewController
            vc.delegate = self
            vc.item = self.selectedItem
            self.present(vc, animated: false, completion: nil)
        }
    }
    
    private func itemTapped(_ markerItem: CGMarkerItem){
        let maxDist = DataManager.sharedInstance.getAppInt(Constants.APP_MAX_DISTANCE) ?? Constants.MAX_DISTANCE
        guard let item = markerItem.item as? CGItem else { return }
        
        var cdSecs = 0
        if let couponItem = item as? CGCoupon {
            cdSecs = CouponGoHelper.sharedInstance.getSecondsFromNow(date: couponItem.openAt)
        }
        
        if cdSecs <= 0 {
            let distance = CouponGoHelper.sharedInstance.getDistance(from: CLLocationCoordinate2D(latitude: item.lat, longitude: item .lng), to: (gmaps.myLocation?.coordinate)!)
            
            if distance < Double(maxDist) {
                selectedItem = item
                showItem()
            }
            else{
                let vc = CouponGoHelper.sharedInstance.getViewController(viewName: Constants.VC_MAP_TOOFAR) as! MapTooFarViewController
                vc.distance = distance
                vc.item = item
                self.present(vc, animated: true, completion: nil)
            }
        }
        else{
            let vc = CouponGoHelper.sharedInstance.getViewController(viewName: Constants.VC_MAP_SURE) as! MapSureViewController
            vc.delegate = self
            vc.item = item
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func mapViewDidFinishTileRendering(_ mapView: GMSMapView) {
        updateItems()
    }
    
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
        let newCamera = GMSCameraPosition.camera(withTarget: cluster.position, zoom: gmaps.camera.zoom + 1)
        let update = GMSCameraUpdate.setCamera(newCamera)
        gmaps.moveCamera(update)
        
        return false
    }

    @IBAction func keymarketTouch(_ sender: Any) {
        //CouponGoHelper.sharedInstance.presentViewController(parent: self, viewName: "KeyStoreViewController")
    }
    
    @IBAction func startpointTouch(_ sender: Any) {
        //let update = GMSCameraUpdate.setTarget(userLocation)
        if CouponGoHelper.sharedInstance.mapUserLocation != nil {
            gmaps.animate(toLocation: CouponGoHelper.sharedInstance.mapUserLocation!.coordinate)
            gmaps.animate(toZoom: DataManager.sharedInstance.getAppFloat(Constants.APP_DEFAULT_ZOOM) ?? Float(Constants.DEFAULT_ZOOM))
        }
    }
    
    func updateItems(){
        if gmaps.myLocation != nil {
            let coord = gmaps.camera.target
            let zoom = gmaps.camera.zoom
            
            let ignoredCouponIds = coupons.map({ "\($0.key)" } ).joined(separator: ",")
            let ignoredKeyIds = keys.map({ "\($0.key)" } ).joined(separator: ",")
            let q : [String : String] = ["lat": String(format: "%f", coord.latitude),
                                         "long": String(format: "%f", coord.longitude),
                                         "zoom": String(format: "%f", zoom),
                                         "ic": ignoredCouponIds,
                                         "ik": ignoredKeyIds]
            CouponGoHelper.sharedInstance.commManager.request(method: "GET", url: "/coupon/list/map", queryData: q, postData: nil) { (result, error) in
                if let result = result {
                    var needToCluster = false
                    
                    let dict = result as NSDictionary
                    if let couponList = dict.value(forKey: "coupon_list") as? NSArray {
                        for item in couponList{
                            let obj = item as! NSDictionary
                            
                            let cItem = CouponGoHelper.sharedInstance.jsonToCoupon(obj)
                            
                            if self.coupons[cItem.id] == nil {
                                needToCluster = true
                                
                                let position = CLLocationCoordinate2DMake(cItem.lat, cItem.lng)
                                let markerItem = CGMarkerItem(position: position, item: cItem)
                                cItem.marker = markerItem
                                
                                self.coupons[cItem.id] = cItem
                                self.clusterManager.add(markerItem)
                            }
                        }
                    }
                    
                    if let keyList = dict.value(forKey: "key_list") as? NSArray {
                        for item in keyList{
                            let obj = item as! NSDictionary
                            
                            let kItem = CouponGoHelper.sharedInstance.jsonToKey(obj)
                            
                            if self.keys[kItem.id] == nil {
                                needToCluster = true
                                
                                let position = CLLocationCoordinate2DMake(kItem.lat, kItem.lng)
                                let markerItem = CGMarkerItem(position: position, item: kItem)
                                kItem.marker = markerItem
                                
                                self.keys[kItem.id] = kItem
                                self.clusterManager.add(markerItem)
                            }
                        }
                    }
                    
                    if let removedCouponList = dict.value(forKey: "removed_coupons") as? NSArray {
                        for item in removedCouponList{
                            let rId = CouponGoHelper.sharedInstance.jsonToInt(item)
                            if let rCoupon = self.coupons.removeValue(forKey: rId) {
                                if let rMarker = rCoupon.marker as? GMUClusterItem {
                                    self.clusterManager.remove(rMarker)
                                    needToCluster = true
                                }
                                
                            }
                        }
                    }
                    
                    if let removedKeyList = dict.value(forKey: "removed_keys") as? NSArray {
                        for item in removedKeyList{
                            let rId = CouponGoHelper.sharedInstance.jsonToInt(item)
                            if let rKey = self.keys.removeValue(forKey: rId) {
                                if let rMarker = rKey.marker as? GMUClusterItem {
                                    self.clusterManager.remove(rMarker)
                                    needToCluster = true
                                }
                            }
                        }
                    }
                    
                    if needToCluster {
                        DispatchQueue.main.async {
                            self.clusterManager.cluster()
                            self.clusterManager.setDelegate(self, mapDelegate: self)
                        }
                    }
                    
                    if let aoc = self.autoOpenCoord {
                        for (_, coupon) in self.coupons{
                            if coupon.lat == aoc.latitude && coupon.lng == aoc.longitude {
                                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100), execute: {
                                    if let item = coupon.marker as? CGMarkerItem{
                                        self.itemTapped(item)
                                    }
                                    self.autoOpenCoord = nil
                                })
                                break
                            }
                        }
                    }
                    
                } else if error != nil {
                    CouponGoHelper.sharedInstance.showAlert(viewController: self, message: "error_server_error".localized(), animated: true, isError: true)
                }
            }
        }
    }
    
    @objc func marketTouch(_ sender: Any) {
        
    }
    
    func updateKeyCountLabel(){
        let keyCnt = DataManager.sharedInstance.getSessionInt("user.key_count") ?? 0
        keyCountLabel.text = String(format: "%d", keyCnt)
    }
    
    func changeCamera(){
        let isOn = DataManager.sharedInstance.getSessionBool(Constants.SESSION_AR_MODE)
        DataManager.sharedInstance.setSession(Constants.SESSION_AR_MODE, value: !isOn)
        
        showItem()
    }
    
    func popupSure(){
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(Constants.POPUP_ASYNC_AFTER_TIME)) {
            let vc = CouponGoHelper.sharedInstance.getViewController(viewName: Constants.VC_MAP_SURE) as! MapSureViewController
            vc.delegate = self
            vc.item = self.selectedItem
            self.present(vc, animated: true)
        }
    }
    
    func popupAcquire(){
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(Constants.POPUP_ASYNC_AFTER_TIME)) {
            if let couponItem = self.selectedItem as? CGCoupon {
                let parameters: [String: String] = ["id": String(format: "%d", couponItem.id),
                                                    "lat": String(format: "%f", couponItem.lat),
                                                    "long": String(format: "%f", couponItem.lng)]
                CouponGoHelper.sharedInstance.commManager.request(method: "PUT", url: "/coupon/catch", queryData: nil, postData: parameters) { (result, error) in
                    if let result = result {
                        // a saját kuponok törlése, hogy a kuponok képernyőn kikényszerítsem a frissítést
                        DataManager.sharedInstance.clearCouponTable()
                        
                        let dict = result as NSDictionary
                        let obj = dict.value(forKey: "coupon") as! NSDictionary
                        
                        let cItem = CGCoupon()
                        cItem.id = CouponGoHelper.sharedInstance.jsonToInt(obj.value(forKey: "id"))
                        cItem.brandImageUrl = CouponGoHelper.sharedInstance.jsonToString(obj.value(forKey: "brand_image_url"))
                        cItem.coverImageurl = CouponGoHelper.sharedInstance.jsonToString(obj.value(forKey: "cover_image_url"))
                        cItem.isFixed = CouponGoHelper.sharedInstance.jsonToBool(obj.value(forKey: "discount_type"), value: "fix")
                        cItem.title = CouponGoHelper.sharedInstance.jsonToString(obj.value(forKey: "title"))
                        cItem.description = CouponGoHelper.sharedInstance.jsonToString(obj.value(forKey: "description"))
                        cItem.validDate = CouponGoHelper.sharedInstance.jsonToDate(obj.value(forKey: "valid_date"))
                        cItem.webshop = CouponGoHelper.sharedInstance.jsonToString(obj.value(forKey: "webshop"))
                        cItem.count = CouponGoHelper.sharedInstance.jsonToInt(obj.value(forKey: "count"))
                        
                        DispatchQueue.main.async {
                            let vc = CouponGoHelper.sharedInstance.getViewController(viewName: Constants.VC_MAP_ACQUIRE) as! MapAcquireViewController
                            vc.delegate = self
                            vc.item = couponItem
                            self.present(vc, animated: true)
                        }
                    } else if error != nil {
                        CouponGoHelper.sharedInstance.showAlert(viewController: self, message: "error_server_error".localized(), animated: true, isError: true)
                    }
                }
            }
            else if let keyItem = self.selectedItem as? CGKey {
                let parameters: [String: String] = ["id": String(format: "%d", keyItem.id),
                                                    "lat": String(format: "%f", keyItem.lat),
                                                    "long": String(format: "%f", keyItem.lng)]
                CouponGoHelper.sharedInstance.commManager.request(method: "PUT", url: "/key/catch", queryData: nil, postData: parameters) { (result, error) in
                    if let result = result {
                        
                        let dict = result as NSDictionary
                        let keyCnt = (dict.value(forKey: "key_count") as! NSNumber).intValue
                        DataManager.sharedInstance.setSession("user.key_count", value: keyCnt)
                        
                        DispatchQueue.main.async {
                            let vc = CouponGoHelper.sharedInstance.getViewController(viewName: Constants.VC_MAP_ACQUIRE) as! MapAcquireViewController
                            vc.delegate = self
                            vc.item = keyItem
                            self.present(vc, animated: true)
                        }
                    } else if error != nil {
                        CouponGoHelper.sharedInstance.showAlert(viewController: self, message: "error_server_error".localized(), animated: true, isError: true)
                    }
                }
            }
        }
    }
    
    func acquireDone(){
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(Constants.POPUP_ASYNC_AFTER_TIME)) {
            if let couponItem = self.selectedItem as? CGCoupon {
                self.coupons.removeValue(forKey: couponItem.id)
            }
            else if let keyItem = self.selectedItem as? CGKey {
                self.keys.removeValue(forKey: keyItem.id)
                self.updateKeyCountLabel()
            }
            
            if let itm = self.selectedItem as? CGItem {
                if let m = itm.marker as? GMUClusterItem {
                    self.clusterManager.remove(m)
                    self.clusterManager.cluster()
                }
            }
        }
    }
}

extension MapViewController: ARDataSource {
    func ar(_ arViewController: ARViewController, viewForAnnotation: ARAnnotation) -> ARAnnotationView {
        let annotationView = AnnotationView()
        annotationView.annotation = viewForAnnotation
        // annotationView.delegate = self
        annotationView.frame = CGRect(x: 0, y: 0, width: 250, height: 490)
        annotationView.item = self.selectedItem
        annotationView.delegate = self
        return annotationView
    }
}

extension MapViewController: ARAnnotationCallbackDelegate {
    func showNotEnoughKeyPopup() {
        CouponGoHelper.sharedInstance.showAlert(viewController: arViewController, title: "", message: "map_wantit_not_enough_key".localized())
    }
    
    func sureQuestionHappened() {
        self.popupSure()
        self.arViewController.dismiss(animated: true, completion: nil)
        self.arViewController = nil
    }
    
    func detailsPopup() {
        guard let coupon = self.selectedItem as? CGCoupon else { return }
        
        self.wasPopup = true
        self.arViewController.dismiss(animated: true, completion: nil)
        self.arViewController = nil
        
        CouponGoHelper.sharedInstance.showCouponDetails(viewController: self, coupon: coupon, url: String(format: "/coupon/%d/info", coupon.campaignId))
    }
    
    func acquireHappened() {
        self.popupAcquire()
        self.arViewController.dismiss(animated: true, completion: nil)
        self.arViewController = nil
    }
}

extension MapViewController: MapWantItCallbackDelegate{
    func mapWantItSure() {
        self.presentedViewController?.dismiss(animated: true, completion: nil)
        self.popupSure()
    }
    
    func mapWantItAcquire() {
        self.presentedViewController?.dismiss(animated: true, completion: nil)
        self.popupAcquire()
    }
    
    func mapWantItDetailsTouched() {
        guard let coupon = self.selectedItem as? CGCoupon else { return }
        
        self.wasPopup = true
        self.presentedViewController?.dismiss(animated: true, completion: nil)
        CouponGoHelper.sharedInstance.showCouponDetails(viewController: self, coupon: coupon, url: String(format: "/coupon/%d/info", coupon.campaignId))
    }
    
    func mapWantItChangeCamera() {
        self.presentedViewController?.dismiss(animated: false, completion: nil)
        self.changeCamera()
    }
    
    func mapWantItClosed() {
        self.presentedViewController?.dismiss(animated: true, completion: nil)
    }
}

extension MapViewController: CouponCallbackDelegate{
    func couponBackTouched() {
        self.presentedViewController?.dismiss(animated: true, completion: nil)
        
        if wasPopup {
            wasPopup = false
            showItem()
        }
    }
}

extension MapViewController: MapSureCallbackDelegate{
    func mapSureAcquire() {
        self.presentedViewController?.dismiss(animated: true, completion: nil)
        self.popupAcquire()
    }
    
    func mapSureClose() {
        self.presentedViewController?.dismiss(animated: true, completion: nil)
        self.popupSure()
    }
}

extension MapViewController: MapAcquireCallbackDelegate{
    func mapAcquireDone() {
        self.presentedViewController?.dismiss(animated: true, completion: nil)
        self.acquireDone()
    }
}
