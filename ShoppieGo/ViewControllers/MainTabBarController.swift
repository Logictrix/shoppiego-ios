//
//  MainTabBarController.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 02. 22..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging

class MainTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)

        switch UIDevice().type {
        case .iPhoneX, .iPhoneXR, .iPhoneXS, .iPhoneXSmax : break
        default:
            UITabBarItem.appearance().titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -4)
            break
        }
    }
    
    @objc func didBecomeActive() {
        let storedToken = DataManager.sharedInstance.getSessionString(Constants.SESSION_FCM_TOKEN)
        let fcmToken = Messaging.messaging().fcmToken
        if fcmToken != nil && storedToken != fcmToken {
            DataManager.sharedInstance.setSession(Constants.SESSION_FCM_TOKEN, value: fcmToken!)
            CouponGoHelper.sharedInstance.sendFCMToken(fcmToken!)
        }
        
        if let op = CouponGoHelper.sharedInstance.launchParameters {
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500), execute: {
                
                
                let dict = op as! NSDictionary
                let type = CouponGoHelper.sharedInstance.jsonToString(dict.value(forKey: "type"))
                if "cg_drop" == type {
                    CouponGoHelper.sharedInstance.showMainTabBar(selectedIdx: 0)
                }
                else if "chat_message" == type {
                    CouponGoHelper.sharedInstance.showMainTabBar(selectedIdx: 3)
                }
            })
        }
    }
}
