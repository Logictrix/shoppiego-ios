//
//  ChangeNameViewController.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 04. 13..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

class ChangeNameViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var nicknameTextField: UITextField!
    
    private var nickname = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        nickname = DataManager.sharedInstance.getSessionString(Constants.SESSION_USER_NICKNAME) ?? ""
        nicknameTextField.text = nickname
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return false
    }

    @IBAction func closeTouch(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func okTouch(_ sender: Any) {
        if nicknameTextField.text != nickname && !(nicknameTextField.text ?? "").isEmpty {
            let parameters: [String: String] = ["nickname": nicknameTextField.text!]
            
            CouponGoHelper.sharedInstance.commManager.request(method: "POST", url: "/user", queryData: nil, postData: parameters) { (result, error) in
                if let result = result {
                    DispatchQueue.main.async {
                        CouponGoHelper.sharedInstance.updateUser(result)
                        self.dismiss(animated: true, completion: nil)
                    }
                } else if error != nil {
                    CouponGoHelper.sharedInstance.showAlert(viewController: self, message: "error_server_error".localized(), animated: true, isError: true)
                }
            }
        }
    }
}
