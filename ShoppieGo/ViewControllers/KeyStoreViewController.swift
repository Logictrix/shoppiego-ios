//
//  KeyStoreViewController.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 02. 22..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

class KeyStoreViewController: UIViewController {
    @IBOutlet weak var vKeyInfo: UIView!
    @IBOutlet weak var lblKeyInfo: UILabel!
    var vKey: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let space = CGFloat(5)
        let padding = CGFloat(26)
        
        let img = UIImageView(image: UIImage(named: "key-white"))
        img.frame = CGRect(x: padding, y: 10, width: img.frame.width, height: img.frame.height)
        
        let lbl = UILabel(frame: CGRect(x: img.frame.origin.x + img.frame.width + space, y: 10, width: 100, height: 14))
        lbl.text = "1"
        lbl.font = UIFont.systemFont(ofSize: 12, weight: .bold)
        lbl.textColor = .white
        lbl.sizeToFit()
        
        vKey = UIView(frame: CGRect(x: 0, y: lblKeyInfo.frame.origin.y + lblKeyInfo.frame.height + 30, width: lbl.frame.origin.x + lbl.frame.width + padding, height: 32))
        vKey.backgroundColor = .black
        vKey.layer.cornerRadius = 16
        vKey.layer.masksToBounds = true
        
        vKey.addSubview(img)
        vKey.addSubview(lbl)
        
        vKeyInfo.addSubview(vKey)
    }
    
    override func viewDidLayoutSubviews() {
        vKey.frame = CGRect(x: (vKeyInfo.frame.width - vKey.frame.width) / 2, y: vKey.frame.origin.y, width: vKey.frame.width, height: vKey.frame.height)
    }
    
    @IBAction func backTouch(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    

}
