import UIKit
import HDAugmentedReality

protocol ARAnnotationCallbackDelegate {
    func acquireHappened()
    func sureQuestionHappened()
    func detailsPopup()
    func showNotEnoughKeyPopup()
}

class AnnotationView: ARAnnotationView {
    var delegate: ARAnnotationCallbackDelegate?
    var item: CGItem? {
        didSet {
            self.prepareView()
        }
    }
    
    override func bindUi() {
        self.isUserInteractionEnabled = true
        self.backgroundColor = UIColor.clear
    }
    
    func prepareView () {
        
        
        if let couponItem = item as? CGCoupon {
            let detailButton = UIRoundedButton(frame: CGRect(x: 55, y: 0, width: 160, height: 32))
            detailButton.cornerRadius = 16
            detailButton.setTitle("annotation_discount_details".localized(), for: .normal)
            detailButton.backgroundColor = .black
            detailButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 9)
            detailButton.addTarget(self, action: #selector(detailButtonTouch), for: .touchUpInside)
            self.addSubview(detailButton)
            
            let chest = UIImageView(frame: CGRect(x: 15, y: 120, width: 240, height: 300))
            
            let ticketView = UIView(frame: CGRect(x: 0, y: 40, width: 270, height: 132))
            
            let ticket = UIImageView(frame: CGRect(x: 0, y: 0, width: 270, height: 132))
            ticket.image = UIImage(named: couponItem.requiredKeyCount == 5 ? "chest-gold-ticket" : "chest-normal-ticket")
            
            let discountLabel = UILabel(frame: CGRect(x: 11, y: 5, width: 80, height: 110))
            discountLabel.text = couponItem.isFixed ? "fix" : couponItem.title
            discountLabel.textAlignment = .center
            discountLabel.font = UIFont.boldSystemFont(ofSize: 35)
            discountLabel.transform = CGAffineTransform(rotationAngle: CGFloat.pi * 270 / 180.0)
            
            let brandImg = UIImageView(frame: CGRect(x: 115, y: 25, width: 120, height: 70))
            brandImg.download(from: couponItem.brandImageUrl)
            brandImg.contentMode = .scaleAspectFit
            
            if couponItem.requiredKeyCount == 0 {
                chest.image = UIImage(named: "chest-opened")
            }
            else {
                chest.image = UIImage.init(named: "chest-closed")
                
                let textView = UIView(frame: CGRect(x: 60, y: 60, width: 120, height: 70))
                let closedLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 120, height: 18))
                closedLabel.font = UIFont.boldSystemFont(ofSize: 18)
                closedLabel.text = "annotation_closed_chest".localized()
                closedLabel.textAlignment = .center
                textView.addSubview(closedLabel)
                let keycountLabel = UILabel(frame: CGRect(x: 20, y: 42, width: 35, height: 18))
                keycountLabel.font = UIFont.boldSystemFont(ofSize: 18)
                keycountLabel.text = String(format: "%d", couponItem.requiredKeyCount)
                keycountLabel.textAlignment = .right
                textView.addSubview(keycountLabel)
                let keyView = UIImageView(frame: CGRect(x: 63, y: 34, width: 28, height: 33))
                keyView.image = UIImage(named: "key")
                textView.addSubview(keyView)
                chest.addSubview(textView)
            }
            self.addSubview(chest)
            ticketView.addSubview(ticket)
            ticketView.addSubview(discountLabel)
            ticketView.addSubview(brandImg)
            self.addSubview(ticketView)
        }
        else if (item as? CGKey) != nil{
            let keyImg = UIImageView(frame: CGRect(x: 15, y: 120, width: 240, height: 300))
            keyImg.image = UIImage(named: "key-lrg")
            self.addSubview(keyImg)
        }
        
        let wantitButton = UIRoundedButton(frame: CGRect(x: 100, y: 435, width: 70, height: 70))
        wantitButton.cornerRadius = 35
        wantitButton.setTitle("annotation_wantit_button".localized(), for: .normal)
        wantitButton.backgroundColor = UIColor.init(displayP3Red: 189.0/255.0, green: 16.0/255.0, blue: 224.0/255.0, alpha: 1.0)
        wantitButton.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        wantitButton.addTarget(self, action: #selector(wantitButtonTouch), for: .touchUpInside)
        self.addSubview(wantitButton)
    }
    
    @objc func detailButtonTouch(sender: UIButton!) {
        self.delegate?.detailsPopup()
    }
    
    @objc func wantitButtonTouch(sender: UIButton!) {
        if let couponItem = self.item as? CGCoupon {
            if couponItem.requiredKeyCount == 0 {
                self.delegate?.acquireHappened()
            }
            else{
                let myKeyCnt = DataManager.sharedInstance.getSessionInt("user.key_count") ?? 0
                if myKeyCnt < couponItem.requiredKeyCount{
                    self.delegate?.showNotEnoughKeyPopup()
                }
                else {
                    self.delegate?.sureQuestionHappened()
                }
            }
        }
        else if (self.item as? CGKey) != nil {
            self.delegate?.acquireHappened()
        }
    }
}
