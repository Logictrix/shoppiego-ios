//
//  RegistrationVC.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 02. 19..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit
import SafariServices

class RegistrationViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate {
    @IBOutlet weak var tvTerms: UITextView!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UIPaddingTextField!
    @IBOutlet weak var eyeButton: UICheckbox!
    @IBOutlet weak var inputBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var RegisterButton: UIBorderedButton!
    @IBOutlet weak var lblTxt: UILabel!
    
    private var defaultBottomConstraint: CGFloat = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tvTerms.hyperlink(str: "registration_terms".localized(), args: Constants.URL_TERMS, Constants.URL_PRIVACY, underline: true)
        tvTerms.delegate = self
        
        eyeButton.isHidden = true
        
        emailField.delegate = self
        passwordField.delegate = self
        RegisterButton.AddshadowOnButton()
        
        defaultBottomConstraint = inputBottomConstraint.constant
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        emailField.changePlaceHolder(placeHolderTxt: "E-mail address", color: UIColor.darkGray)
        passwordField.changePlaceHolder(placeHolderTxt: "Password", color: UIColor.darkGray)
        RegisterButton.setTitle("Lets_go".localized(), for: .normal)
        emailField.AddRoundLine()
        passwordField.AddRoundLine()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField as? UIPaddingTextField != nil {
            textField.resignFirstResponder()
        }
        else {
            passwordField.becomeFirstResponder()
        }
        
        return false
    }
    
    @objc func keyboardWillShow(notification: NSNotification){
        if let userInfo = notification.userInfo as? Dictionary<String, AnyObject>{
            let frame = userInfo[UIResponder.keyboardFrameEndUserInfoKey]
            let keyboardRect = frame?.cgRectValue
            if let keyboardHeight = keyboardRect?.height {
                self.inputBottomConstraint.constant = keyboardHeight
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification){
        self.inputBottomConstraint.constant = defaultBottomConstraint
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        let svc = SFSafariViewController(url: URL)
        present(svc, animated: true, completion: nil)
        
        return false
    }
    
    @IBAction func backTouch(_ sender: Any) {
        CouponGoHelper.sharedInstance.showViewController(viewName: "StartViewController")
    }
    
    @IBAction func registrationTouch(_ sender: Any) {
        let email = emailField.text ?? ""
        let pwd = passwordField.text ?? ""
        if email.isEmpty || pwd.isEmpty {
            CouponGoHelper.sharedInstance.showAlert(viewController: self, message: "registration_missing_details".localized(), animated: true, isError: true)
            return
        }
        
        let p: [String: String] = ["email": email,
                                   "password": pwd]
        CouponGoHelper.sharedInstance.commManager.request(method: "PUT", url: "/auth/signup", queryData: nil, postData: p) { (result, error) in
            if let result = result {
                let token = result.value(forKey: "access_token") as! String
                
                DispatchQueue.main.async {
                    CouponGoHelper.sharedInstance.accessToken = token
                    DataManager.sharedInstance.setSession(Constants.SESSION_USER_ACCESS_TOKEN, value: token)
                    CouponGoHelper.sharedInstance.updateData()
                    CouponGoHelper.sharedInstance.showMainTabBar()
                }
            }
            else if let error = error {
                print(error)
                print(error.localizedDescription)
                var errmsg = "error_server_error"
                if let errorString = (error as NSError).userInfo["error"] {
                    errmsg = errorString as! String
                }
                CouponGoHelper.sharedInstance.showAlert(viewController: self, message: errmsg.localized(), animated: true, isError: true)
            }
        }
    }
    
    @IBAction func eyeTouch(_ sender: Any) {
        let btn = sender as! UICheckbox
        passwordField.isSecureTextEntry = btn.checked
    }
    
    @IBAction func passwordChanged(_ sender: Any) {
        if passwordField.text == "" {
            eyeButton.isHidden = true
            passwordField.paddingRight = 0
        }
        else{
            eyeButton.isHidden = false
            passwordField.paddingRight = eyeButton.frame.width + 8
        }
    }
}
