//
//  InitViewController.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 20..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

class InitViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        CouponGoHelper.sharedInstance.accessToken = DataManager.sharedInstance.getSessionString(Constants.SESSION_USER_ACCESS_TOKEN) ?? ""
        if !(CouponGoHelper.sharedInstance.accessToken).isEmpty{
            CouponGoHelper.sharedInstance.updateData()
            CouponGoHelper.sharedInstance.showMainTabBar()
        }
        else{
            //OLD CODE
            //   CouponGoHelper.sharedInstance.showViewController(viewName: Constants.VC_START)
            CouponGoHelper.sharedInstance.showViewController(viewName: Constants.VC_SLIDER)
        }
    }

}
