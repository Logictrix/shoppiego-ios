//
//  MapChestViewController.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 13..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

protocol MapWantItCallbackDelegate{
    func mapWantItSure()
    func mapWantItAcquire()
    func mapWantItDetailsTouched()
    func mapWantItChangeCamera()
    func mapWantItClosed()
}

class MapWantItViewController: UIViewController {

    @IBOutlet weak var ticketView: UIView!
    @IBOutlet weak var discountLabel: UIRotatedLabel!
    @IBOutlet weak var brandImage: UIImageView!
    @IBOutlet weak var ticketImage: UIImageView!
    @IBOutlet weak var keyCountLabel: UILabel!
    @IBOutlet weak var chestView: UIView!
    @IBOutlet weak var chestImage: UIImageView!
    @IBOutlet weak var chestTextView: UIView!
    
    var delegate: MapWantItCallbackDelegate?
    var item : CGItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        refresh()
    }
    
    private func refresh(){
        if let couponItem = item as? CGCoupon {
            if couponItem.requiredKeyCount == 0 {
                chestTextView.isHidden = true
                chestImage.image = UIImage(named: "chest-opened")
            }
            else {
                chestImage.image = UIImage(named: "chest-closed")
            }
            
            if couponItem.requiredKeyCount == 5 {
                ticketImage.image = UIImage(named: "chest-gold-ticket")
            }
            else{
                ticketImage.image = UIImage(named: "chest-normal-ticket")
            }
            
            discountLabel.text = couponItem.isFixed ? "fix" : couponItem.title
            brandImage.download(from: couponItem.brandImageUrl)
            keyCountLabel.text = String(format: "%d", couponItem.requiredKeyCount)
        }
        else if (item as? CGKey) != nil{
            ticketView.isHidden = true
            chestTextView.isHidden = true
            chestImage.image = UIImage(named: "key-lrg")
        }
    }
    
    @IBAction func okTouch(_ sender: Any) {
        if let couponItem = self.item as? CGCoupon {
            if couponItem.requiredKeyCount == 0 {
                /*if let mtb = self.presentingViewController as? MainTabBarController {
                    if let svc = mtb.selectedViewController as? MapViewController {
                        svc.popupAcquire(couponItem)
                        self.dismiss(animated: true, completion: nil)
                    }
                }*/
                delegate?.mapWantItAcquire()
            }
            else{
                let myKeyCnt = DataManager.sharedInstance.getSessionInt("user.key_count") ?? 0
                if myKeyCnt < couponItem.requiredKeyCount{
                    CouponGoHelper.sharedInstance.showAlert(viewController: self, title: "", message: "map_wantit_not_enough_key".localized())
                }
                else {
                    /*if let mtb = self.presentingViewController as? MainTabBarController {
                        if let svc = mtb.selectedViewController as? MapViewController {
                            svc.popupSure(couponItem)
                            self.dismiss(animated: true, completion: nil)
                        }
                    }*/
                    delegate?.mapWantItSure()
                }
            }
        }
        else if let keyItem = self.item as? CGKey {
            /*if let mtb = self.presentingViewController as? MainTabBarController {
                if let svc = mtb.selectedViewController as? MapViewController {
                    svc.popupAcquire(keyItem)
                    self.dismiss(animated: true, completion: nil)
                }
            }*/
            delegate?.mapWantItAcquire()
        }
    }
    
    @IBAction func closeTouch(_ sender: Any) {
        //dismiss(animated: true, completion: nil)
        delegate?.mapWantItClosed()
    }
    
    @IBAction func changeTouch(_ sender: Any) {
        /*if let mtb = self.presentingViewController as? MainTabBarController {
            if let svc = mtb.selectedViewController as? MapViewController {
                svc.changeCamera(self.item)
                self.dismiss(animated: false, completion: nil)
            }
        }*/
        delegate?.mapWantItChangeCamera()
    }
    
    @IBAction func detailsTouch(_ sender: Any) {
        delegate?.mapWantItDetailsTouched()
    }
}

extension MapWantItViewController: CouponCallbackDelegate{
    func couponBackTouched() {
        self.presentedViewController?.dismiss(animated: true, completion: nil)
    }
}
