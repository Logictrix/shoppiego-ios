//
//  CouponChangeViewController.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 05..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

class CouponSuccessViewController: UIViewController {

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblCount: UILabel!
    @IBOutlet weak var lblCoupon: UILabel!
    
    public var isFree = false
    public var count = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isFree {
            lblTitle.text = "couponsuccess_title_offer".localized()
        }
        else{
            lblTitle.text = "couponsuccess_title_swap".localized()
        }
        lblCoupon.text = count > 1 ? "couponsuccess_coupons".localized() : "couponsuccess_coupon".localized()
        lblCount.text = String(format: "%d", count)
    }
    
    @IBAction func okTouch(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
