

import UIKit

class SliderViewController: UIViewController {
    
    @IBOutlet weak var btnStart: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var carouselScrollView: UIScrollView!
    @IBOutlet weak var dotView: UIView!
    
    private var carouselTexts: [String] = []
    private var carouselTextsDetail: [String] = []
    private var carouselDots: Dictionary<Int, UIImageView> = [:]
    private var timer: Timer?
    private var carouselCnt = 0
    private var carouselIdx = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        carouselTexts.append("we_are_glad".localized())
        carouselTexts.append("what_can".localized())
        carouselTexts.append("where_should".localized())
        carouselTexts.append("hunt".localized())
        
        carouselTextsDetail.append("forget".localized())
        carouselTextsDetail.append("look_around".localized())
        carouselTextsDetail.append("go_close".localized())
        carouselTextsDetail.append("catch_the_coupon".localized())
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(swipeHandler))
        swipeLeft.direction = .left
        carouselScrollView.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(swipeHandler))
        swipeRight.direction = .right
        carouselScrollView.addGestureRecognizer(swipeRight)
        
        btnSkip.setTitle("skip".localized(), for: .normal)
        btnNext.setTitle("next".localized(), for: .normal)
        btnStart.setTitle("let_start".localized(), for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btnStart.isHidden = true
        btnNext.isHidden = false
        btnSkip.isHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        carouselCnt = carouselTexts.count
        var dotX: CGFloat = 0.0
        let dotLen = (dotView.frame.width - 10.0) / CGFloat(carouselCnt - 1)
        var carouselX: CGFloat = 0.0
        
        for i in 0 ... (carouselCnt - 1) {
            let v = UIView(frame: CGRect(x: carouselX, y: 0, width: carouselScrollView.frame.width, height: carouselScrollView.frame.height))
            
            let lblTitle = UILabel(frame: CGRect(x: 0, y: 0, width: v.frame.width, height: 63))
            lblTitle.font = UIFont.systemFont(ofSize: 26, weight: .bold)
            lblTitle.textAlignment = .center
            lblTitle.text = carouselTexts[i]
            lblTitle.numberOfLines = 0
            lblTitle.lineBreakMode = .byWordWrapping
            
            let lblDetail = UILabel(frame: CGRect(x: 0, y: 74, width: v.frame.width, height: 40))
            lblDetail.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
            lblDetail.textAlignment = .center
            lblDetail.text = carouselTextsDetail[i]
            lblDetail.numberOfLines = 0
            lblDetail.lineBreakMode = .byWordWrapping
            
            let img = UIImageView(frame: CGRect(x: 0, y:  63 + 40 + 15, width: v.frame.width, height: carouselScrollView.frame.height - 100))
            //img.image = UIImage(named: String(format: "carousel-%d", (i + 1)))
            //img.image = UIImage(named: String(format: "login-%d", (i + 1)))
            img.image = UIImage(named: String(format: "Group_%d", (i + 1)))
            img.contentMode = .scaleAspectFit
            v.addSubview(lblTitle)
            v.addSubview(lblDetail)
            v.addSubview(img)
            
            carouselScrollView.addSubview(v)
            carouselX += carouselScrollView.frame.width
            
            let dotImg = UIImageView(frame: CGRect(x: dotX, y: 0, width: 10, height: 10))
            dotImg.image = UIImage(named: "carousel-dot")
            carouselDots[i] = dotImg
            dotX += dotLen
            
            dotView.addSubview(dotImg)
        }
        carouselScrollView.contentSize = CGSize(width: carouselX, height: carouselScrollView.frame.height)
        carouselRefresh()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        let csubViews = carouselScrollView.subviews
        for subview in csubViews{
            subview.removeFromSuperview()
        }
        
        let dsubViews = dotView.subviews
        for subview in dsubViews{
            subview.removeFromSuperview()
        }
        
        carouselDots.removeAll()
        
        timer?.invalidate()
    }
    
    private func carouselRefresh(){
        if carouselIdx >= carouselCnt {
            return
        }
        
        if carouselIdx == 3 {
          
            btnStart.isHidden = false
            btnNext.isHidden = true
            btnSkip.isHidden = true
        }else{
           
            btnStart.isHidden = true
            btnNext.isHidden = false
            btnSkip.isHidden = false
        }
        
        for i in 0 ... (carouselCnt - 1) {
            carouselDots[i]!.image = UIImage(named: (i == carouselIdx ? "carousel-dot-black" : "carousel-dot"))
        }
        
        carouselScrollView.setContentOffset(CGPoint(x: CGFloat(carouselIdx) * carouselScrollView.frame.width, y: 0), animated: true)
      //  timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(carouselGoToNext), userInfo: nil, repeats: false)
    }
    
    @objc private func carouselGoToNext(){
        carouselIdx += 1
        if carouselIdx == carouselCnt {
            carouselIdx = 0
        }
        carouselRefresh()
    }
    
    @objc private func swipeHandler(gesture: UISwipeGestureRecognizer){
        timer?.invalidate()
        
        if gesture.direction == .left {
            carouselIdx += 1
            if carouselIdx == carouselCnt {
                carouselIdx = carouselCnt - 1
            }
        }
        else if gesture.direction == .right {
            carouselIdx -= 1
            if carouselIdx < 0 {
                carouselIdx = 0
            }
        }
        
        carouselRefresh()
    }
    
   
    
    @IBAction func SkipTouch(_ sender: Any) {
        CouponGoHelper.sharedInstance.showViewController(viewName: Constants.VC_START)
    }
    
    @IBAction func NextTouch(_ sender: Any) {
        carouselIdx += 1
        if carouselIdx == carouselCnt {
            carouselIdx = carouselCnt - 1
        }
        carouselRefresh()
    }
    
    @IBAction func startTouch(_ sender: Any) {
     CouponGoHelper.sharedInstance.showViewController(viewName: Constants.VC_START)
    }
    
    
    
}

