//
//  ChatItemsViewController.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 14..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

class ChatItemsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var couponCollectionView: UICollectionView!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var allLabel: UILabel!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    
    private var couponList : Array<CGCoupon> = []
    var selectedCoupons : Array<Int> = []
    var keyCount = 0
    private var allKeyCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        allKeyCount = DataManager.sharedInstance.getSessionInt(Constants.SESSION_USER_KEYCOUNT) ?? 0
        //allKeyCount = 50
        
        couponCollectionView.delegate = self
        couponCollectionView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadCoupons()
        refreshKeys()
    }
    
    override func viewDidLayoutSubviews() {
        let layout = couponCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        layout.minimumInteritemSpacing = 5
        layout.itemSize = CGSize(width: (couponCollectionView.frame.width - 20) / 2, height: layout.itemSize.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return couponList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let itm = couponList[indexPath.item]
        
        let cell = couponCollectionView.dequeueReusableCell(withReuseIdentifier: "SelectableCouponCell", for: indexPath) as! SelectableCouponCell
        
        cell.background.image = UIImage(named: String(format: "coupon-%d", (itm.id % 6)))
        
        cell.brandImage.download(from: itm.brandImageUrl)
        
        cell.discount.text = itm.title
        cell.validDate.text = CouponGoHelper.sharedInstance.convertDateToString(itm.validDate)
        cell.validDate.font = UIFont.systemFont(ofSize: 11)
        cell.validDate.textColor = .black
        
        if CouponGoHelper.sharedInstance.getDaysFromNow(date: itm.validDate) > 58 {
            cell.validDate.font = UIFont.systemFont(ofSize: 13, weight: .bold)
            cell.validDate.textColor = .red
        }
        
        if selectedCoupons.contains(itm.id) {
            //cell.selectedImage.borderWidth = 0
            cell.selectedImage.backgroundColor = UIColor(red: 189, green: 16, blue: 224, alpha: 100)
            cell.selectedImage.image = UIImage(named: "sign-pipe-sml-white")
        }
        else{
            //cell.selectedImage.borderWidth = 1
            cell.selectedImage.backgroundColor = .white
            cell.selectedImage.image = nil
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let couponId = couponList[indexPath.item].id
        
        if !selectedCoupons.contains(couponId) {
            selectedCoupons.append(couponId)
        }
        else{
            let idx = selectedCoupons.firstIndex(of: couponId)!
            selectedCoupons.remove(at: idx)
        }
        
        couponCollectionView!.reloadItems(at: [indexPath])
    }

    private func loadCoupons(){
        if DataManager.sharedInstance.getCouponCount() == 0 {
            CouponGoHelper.sharedInstance.addLoadAnimation(parent: self.view)
            couponList.removeAll()

            CouponGoHelper.sharedInstance.commManager.request(method: "GET", url: "/coupon/list/own", queryData: nil, postData: nil, delayInMilliseconds: 500) { (result, error) in
                if let result = result {
                    DispatchQueue.main.async {
                        let dict = result as NSDictionary
                        let couponList = dict.value(forKey: "coupon_list") as! NSArray
                        for item in couponList{
                            let obj = item as! NSDictionary
                            
                            let coupon = CouponGoHelper.sharedInstance.jsonToCoupon(obj)
                            
                            DataManager.sharedInstance.addCoupon(coupon)
                        }
                        self.couponList = DataManager.sharedInstance.getExchangeableCoupons()
                        self.couponCollectionView.reloadData()
                        
                        CouponGoHelper.sharedInstance.removeLoadAnimation(parent: self.view)
                    }
                } else if error != nil {
                    CouponGoHelper.sharedInstance.showAlert(viewController: self, message: "error_server_error".localized(), animated: true, isError: true)
                }
            }
        }
        else {
            couponList = DataManager.sharedInstance.getExchangeableCoupons()
            couponCollectionView.reloadData()
        }
    }
    
    @IBAction func minusTouch(_ sender: Any) {
        if self.keyCount > 0 {
            self.keyCount -= 1
        }
        refreshKeys()
    }
    
    @IBAction func plusTouch(_ sender: Any) {
        if self.keyCount < allKeyCount {
            self.keyCount += 1
        }
        refreshKeys()
    }
    
    @IBAction func backTouch(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func okTouch(_ sender: Any) {
        var selCoupons: Array<CGCoupon> = []
        for coupon in couponList {
            if selectedCoupons.contains(coupon.id) {
                selCoupons.append(coupon)
            }
        }
        
        let itms = CGTradeItems()
        itms.keyCount = keyCount
        itms.coupons = selCoupons
        
        if let pvc = self.presentingViewController as? ChatViewController {
            pvc.setTradeItems(items: itms)
        }
        
        dismiss(animated: false, completion: nil)
    }
    
    private func refreshKeys(){
        countLabel.text = String(format: "%d", self.keyCount)
        minusButton.isHidden = self.keyCount == 0
        plusButton.isHidden = self.keyCount == allKeyCount
        allLabel.text = "chatitems_all_keys".localized(args: allKeyCount)
    }
}
