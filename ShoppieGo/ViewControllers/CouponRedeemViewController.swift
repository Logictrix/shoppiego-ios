//
//  CouponRedeemPopupViewController.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 05..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit
import SafariServices

class CouponRedeemViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var barcodeImage: UIImageView!
    @IBOutlet weak var descriptionTextView: UITextView!
        
    @IBOutlet var barcodeImageHeightConstraint: NSLayoutConstraint!
    @IBOutlet var descriptionTextTopConstraint: NSLayoutConstraint!
    
    var code = ""
    var webshop = ""
    var codeImageUrl = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let url = URL(string: webshop)
        let host = url?.host ?? ""
        
        codeLabel.text = code
        descriptionTextView.hyperlink(str: "couponredeem_description".localized(args: host), args: host, font: UIFont.systemFont(ofSize: 14, weight: .bold))
        descriptionTextView.delegate = self
        
        if codeImageUrl.isEmpty {
            barcodeImage.isHidden = true
            barcodeImageHeightConstraint.isActive = false
            descriptionTextTopConstraint.constant -= barcodeImageHeightConstraint.constant
        }
        else {
            barcodeImage.download(from: codeImageUrl)
        }
        
        self.updateViewConstraints()
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        let svc = SFSafariViewController(url: URL)
        present(svc, animated: true, completion: nil)
        
        return false
    }
    
    @IBAction func closeTouch(_ sender: Any) {
        CouponGoHelper.sharedInstance.showViewController(viewName: Constants.VC_COUPON)
    }
    
    @IBAction func laterTouch(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func webshopTouch(_ sender: Any) {
        if let url = URL(string: webshop) {
            UIApplication.shared.open(url, options: [:])
        }
    }
}
