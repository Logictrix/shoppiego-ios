//
//  SettingsViewController.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 02. 22..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit
import SafariServices

class SettingsViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var soundeffectCheckbox: UICheckbox!
    @IBOutlet weak var vibrationCheckbox: UICheckbox!
    @IBOutlet weak var pushnotificationCheckbox: UICheckbox!
    @IBOutlet weak var helpcenterTextView: UITextView!
    @IBOutlet weak var reportanerrorTextView: UITextView!
    @IBOutlet weak var aboutusTextView: UITextView!
    
    @IBOutlet var notificationViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var contentViewHeightConstraint: NSLayoutConstraint!

    private var notificationCheckboxes: Dictionary<Int, UICheckbox> = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        soundeffectCheckbox.checked = DataManager.sharedInstance.getAppBool(Constants.APP_SOUNDEFFECT)
        vibrationCheckbox.checked = DataManager.sharedInstance.getAppBool(Constants.APP_VIBRATION)
        pushnotificationCheckbox.checked = DataManager.sharedInstance.getAppBool(Constants.APP_PUSHNOTIFICATION)
        
        helpcenterTextView.hyperlink(str: "settings_help_center".localized(), args: Constants.URL_HELPCENTER, font: UIFont.systemFont(ofSize: 13, weight: .bold))
        helpcenterTextView.delegate = self
        reportanerrorTextView.hyperlink(str: "settings_report_an_error".localized(), args: Constants.URL_REPORT_AN_ERROR, font: UIFont.systemFont(ofSize: 13, weight: .bold))
        reportanerrorTextView.delegate = self
        aboutusTextView.hyperlink(str: "settings_about_us".localized(), args: Constants.URL_ABOUT_US, font: UIFont.systemFont(ofSize: 13, weight: .bold))
        aboutusTextView.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        refresh()
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        let svc = SFSafariViewController(url: URL)
        present(svc, animated: true, completion: nil)
        
        return false
    }
    
    private func refresh(){
        contentViewHeightConstraint.constant -= notificationViewHeightConstraint.constant
        notificationViewHeightConstraint.constant = 0
        let checkedCategories = DataManager.sharedInstance.getSessionString(Constants.SESSION_SETTINGS_NOTIFICATIONS)?.components(separatedBy: ",")
        
        var yOffs: CGFloat = 48.0
        let w = notificationView.frame.width
        let cats = DataManager.sharedInstance.getCategories()
        for cat in cats {
            
            let lbl = UILabel(frame: CGRect(x: 0, y: yOffs, width: w - 43.0, height: 42))
            lbl.font = UIFont.systemFont(ofSize: 13, weight: .bold)
            lbl.text = cat.title
            notificationView.addSubview(lbl)
            
            let cb = UICheckbox(frame: CGRect(x: w - 23.0, y: yOffs + 9.5, width: 23, height: 23))
            cb.onImage = UIImage(named: "toggle-on")
            cb.offImage = UIImage(named: "toggle-off")
            cb.checked = checkedCategories?.contains(String(format: "%d", cat.id)) ?? false
            cb.tag = cat.id
            cb.addTarget(self, action: #selector(categoryCheckboxTouch), for: .touchUpInside)
            notificationCheckboxes[cat.id] = cb
            notificationView.addSubview(cb)
            
            yOffs += 50.0
        }
        
        yOffs -= 8.0
        
        notificationViewHeightConstraint.constant += yOffs
        contentViewHeightConstraint.constant += yOffs
        
        self.updateViewConstraints()
    }
    
    @objc private func categoryCheckboxTouch(sender: UICheckbox!){
        var idx = 0
        var parameters: [String: String] = [:]
        for (id, cb) in notificationCheckboxes {
            if cb.checked {
                parameters[String(format: "settings[category_notifications][%d]", idx)] = String(format: "%d", id)
                idx += 1
            }
        }

        /*if parameters.count == 0 {
            parameters["settings[category_notifications]"] = ""
        }*/
        
        CouponGoHelper.sharedInstance.commManager.request(method: "POST", url: "/user", queryData: nil, postData: parameters) { (result, error) in
            if let result = result {
                DispatchQueue.main.async {
                    CouponGoHelper.sharedInstance.updateUser(result)
                }
            } else if error != nil {
                CouponGoHelper.sharedInstance.showAlert(viewController: self, message: "error_server_error".localized(), animated: true, isError: true)
            }
        }
    }
    
    @IBAction func backTouch(_ sender: Any) {
        CouponGoHelper.sharedInstance.showMainTabBar(selectedIdx: Constants.TB_PROFILE)
    }
    
    @IBAction func signoutTouch(_ sender: Any) {
        DataManager.sharedInstance.clearSessionTable()
        CouponGoHelper.sharedInstance.showViewController(viewName: Constants.VC_START)
    }
    
    @IBAction func changeNameTouch(_ sender: Any) {
        CouponGoHelper.sharedInstance.presentViewController(parent: self, viewName: Constants.VC_CHANGE_NAME, animated: true)
    }
    
    @IBAction func soundeffectTouch(_ sender: Any) {
        DataManager.sharedInstance.setApp(Constants.APP_SOUNDEFFECT, value: soundeffectCheckbox.checked)
    }
    
    @IBAction func vibrationTouch(_ sender: Any) {
        DataManager.sharedInstance.setApp(Constants.APP_VIBRATION, value: vibrationCheckbox.checked)
    }
    
    @IBAction func pushnotificationTouch(_ sender: Any) {
        DataManager.sharedInstance.setApp(Constants.APP_PUSHNOTIFICATION, value: pushnotificationCheckbox.checked)
    }
}
