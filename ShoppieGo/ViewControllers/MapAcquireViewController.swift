//
//  MapAcquireViewController.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 13..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

protocol MapAcquireCallbackDelegate{
    func mapAcquireDone()
}

class MapAcquireViewController: UIViewController {

    @IBOutlet weak var brandImage: UIImageView!
    @IBOutlet weak var discountTopLabel: UILabel!
    @IBOutlet weak var discountBottomLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var validLabel: UILabel!
    @IBOutlet weak var borderView: UIRoundedView!
    @IBOutlet var discountViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var discountButton: UIRoundedButton!
    @IBOutlet var okButtonTopConstaint: NSLayoutConstraint!
    
    var item: Any?
    var delegate: MapAcquireCallbackDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        refresh()
    }
    
    func refresh(){
        if let couponItem = item as? CGCoupon {
            borderView.borderColor = UIColor(rgb: (couponItem.requiredKeyCount == 5 ? Constants.COLOR_GOLD : Constants.COLOR_NORMAL))
            if couponItem.isFixed {
                discountTopLabel.text = "map_acquire_fix".localized()
                discountBottomLabel.text = "map_acquire_discount".localized()
            }
            else{
                discountBottomLabel.removeFromSuperview()
                discountTopLabel.frame = CGRect(x: discountTopLabel.frame.origin.x, y: discountTopLabel.frame.origin.y, width: discountTopLabel.frame.width, height: discountViewHeightConstraint.constant)
                discountTopLabel.font = UIFont.systemFont(ofSize: 50, weight: .bold)
                discountTopLabel.text = couponItem.title
            }
            brandImage.download(from: couponItem.brandImageUrl)
            descriptionLabel.text = couponItem.description
            let sDate = CouponGoHelper.sharedInstance.convertDateToString(couponItem.validDate)
            validLabel.text = (sDate.isEmpty ? "" : "map_acquire_valid".localized(args: sDate))
        }
        else if (item as? CGKey) != nil{
            borderView.borderWidth = 0.0
            borderView.cornerRadius = 0.0
            brandImage.image = UIImage(named: "key-lrg")
            discountButton.isHidden = true
            discountTopLabel.isHidden = true
            discountBottomLabel.isHidden = true
            descriptionLabel.isHidden = true
            validLabel.isHidden = true
            okButtonTopConstaint.constant = 20.0
        }
        self.updateViewConstraints()
    }
    
    @IBAction func okTouch(_ sender: Any) {
        //CouponGoHelper.sharedInstance.showMainTabBar()
        /*if let mtb = self.presentingViewController as? MainTabBarController {
            if let svc = mtb.selectedViewController as? MapViewController {
                svc.mapAcquireDone()
                self.dismiss(animated: true, completion: nil)
            }
        }*/
        delegate?.mapAcquireDone()
    }
    
    @IBAction func detailsTouch(_ sender: Any) {
        guard let coupon = item as? CGCoupon else { return }
        
        CouponGoHelper.sharedInstance.showCouponDetails(viewController: self, coupon: coupon, url: String(format: "/coupon/%d/info", coupon.campaignId))
    }
}

extension MapAcquireViewController: CouponCallbackDelegate{
    func couponBackTouched() {
        self.presentedViewController?.dismiss(animated: true, completion: nil)
    }
}
