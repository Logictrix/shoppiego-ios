//
//  MapTooFarViewController.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 13..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit
import MapKit

class MapTooFarViewController: UIViewController {

    @IBOutlet weak private var keyImageTopConstraint: NSLayoutConstraint!
    @IBOutlet weak private var availableLabelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak private var buttonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak private var couponView: UIView!
    @IBOutlet weak private var brandImage: UIRoundedImageView!
    @IBOutlet weak private var discountTopLabel: UILabel!
    @IBOutlet weak private var discountBottomLabel: UILabel!
    @IBOutlet weak private var availableLabel: UILabel!
    @IBOutlet weak private var keyImage: UIImageView!
    @IBOutlet weak private var keyCountView: UIView!
    @IBOutlet weak private var keyCountLabel: UILabel!
    @IBOutlet weak private var distanceLabel: UILabel!
    @IBOutlet weak private var couponInnerView: UIView!
    @IBOutlet weak var couponInnerViewHeightConstraint: NSLayoutConstraint!
    
    var item : Any?
    var distance : CLLocationDistance = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        refresh()
    }
    
    @IBAction func okTouch(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    private func refresh(){
        let dist = round(distance)
        
        if dist < 2000 {
            distanceLabel.text = "map_toofar_dist_meter".localized(args: Int(dist))
        }
        else {
            distanceLabel.text = "map_toofar_dist_kilometer".localized(args: Int(round(dist / 1000)))
        }
        
        if let couponItem = item as? CGCoupon  {
            if couponItem.isFixed {
                discountTopLabel.text = "map_toofar_fix".localized()
                discountBottomLabel.text = "map_toofar_discount".localized()
                discountBottomLabel.isHidden = false
            }
            else{
                discountBottomLabel.isHidden = true
                discountTopLabel.frame = CGRect(x: discountTopLabel.frame.origin.x, y: discountTopLabel.frame.origin.y, width: discountTopLabel.frame.width, height: couponInnerViewHeightConstraint.constant)
                discountTopLabel.font = UIFont.systemFont(ofSize: 50, weight: .bold)
                discountTopLabel.text = couponItem.title
            }
            availableLabelTopConstraint.constant = keyImageTopConstraint.constant
            keyImage.isHidden = true
            availableLabel.text = couponItem.isUnlimited ? "map_toofar_unlimited_coupons".localized() : "map_toofar_limited_coupons".localized(args: couponItem.count)
            brandImage.download(from: couponItem.brandImageUrl)
            
            if couponItem.requiredKeyCount > 0 {
                keyCountLabel.text = String(format: "%d", couponItem.requiredKeyCount)
            }
            else{
                keyCountView.isHidden = true
                buttonTopConstraint.constant = 24
            }
        }
        else if let keyItem = item as? CGKey {
            keyImageTopConstraint.constant = 40
            availableLabelTopConstraint.constant = keyImageTopConstraint.constant +  keyImage.frame.height + 8
            buttonTopConstraint.constant = 24
            couponView.isHidden = true
            keyCountView.isHidden = true
            availableLabel.text = "map_toofar_limited_keys".localized(args: keyItem.count)
        }
        self.updateViewConstraints()
    }

    @IBAction func detailsTouch(_ sender: Any) {
        guard let coupon = item as? CGCoupon else { return }
        
        CouponGoHelper.sharedInstance.showCouponDetails(viewController: self, coupon: coupon, url: String(format: "/coupon/%d/info", coupon.campaignId))
    }
    
    @IBAction func navigateTouch(_ sender: Any) {
        if let couponItem = item as? CGCoupon  {
            if let coord = CouponGoHelper.sharedInstance.mapUserLocation?.coordinate {
                let source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: coord.latitude, longitude: coord.longitude)))
                //source.name = ""
                
                let destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: couponItem.lat, longitude: couponItem.lng)))
                //destination.name = ""
                
                MKMapItem.openMaps(with: [source, destination], launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
            }
        } else if let keyItem = item as? CGKey {
            if let coord = CouponGoHelper.sharedInstance.mapUserLocation?.coordinate {
                let source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: coord.latitude, longitude: coord.longitude)))
                //source.name = ""
                
                let destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: keyItem.lat, longitude: keyItem.lng)))
                //destination.name = ""
                
                MKMapItem.openMaps(with: [source, destination], launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
            }
        }
    }
}

extension MapTooFarViewController: CouponCallbackDelegate{
    func couponBackTouched() {
        self.presentedViewController?.dismiss(animated: true, completion: nil)
    }
}
