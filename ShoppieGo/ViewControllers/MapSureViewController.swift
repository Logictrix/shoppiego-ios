//
//  MapSureViewController.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 13..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

protocol MapSureCallbackDelegate{
    func mapSureAcquire()
    func mapSureClose()
}

class MapSureViewController: UIViewController {

    @IBOutlet weak var borderView: UIRoundedView!
    @IBOutlet weak var brandImage: UIImageView!
    @IBOutlet weak var discountTopLabel: UILabel!
    @IBOutlet weak var discountBottomLabel: UILabel!
    @IBOutlet weak var keyCountLabel: UILabel!
    @IBOutlet weak var detailsButton: UIRoundedButton!
    @IBOutlet weak var countdownView: UIView!
    @IBOutlet weak var daysLabel: UILabel!
    @IBOutlet weak var hoursLabel: UILabel!
    @IBOutlet weak var minutesLabel: UILabel!
    @IBOutlet weak var secondsLabel: UILabel!
    @IBOutlet weak var keyView: UIView!
    @IBOutlet weak var keyViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var keyViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var discountViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var countdownHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var okButton: UIRoundedButton!
    
    var delegate: MapSureCallbackDelegate?
    
    var item: Any?
    private var countdownTimer: Timer?
    private var keyViewTop: CGFloat?
    private var buttonViewTop: CGFloat?

    override func viewDidLoad() {
        super.viewDidLoad()

        setOkButton(enabled: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        refresh()
    }
    
    private func refresh(){
        if let couponItem = item as? CGCoupon {
            keyViewTop = keyViewTop ?? keyViewTopConstraint.constant
            buttonViewTop = buttonViewTop ?? buttonViewTopConstraint.constant
            
            borderView.borderColor = UIColor(rgb: (couponItem.requiredKeyCount == 5 ? Constants.COLOR_GOLD : Constants.COLOR_NORMAL))
            brandImage.download(from: couponItem.brandImageUrl)
            if couponItem.isFixed {
                discountBottomLabel.isHidden = false
                discountTopLabel.text = "map_sure_fix".localized()
                discountBottomLabel.text = "map_sure_discount".localized()
            }
            else {
                discountBottomLabel.removeFromSuperview()
                discountTopLabel.frame = CGRect(x: discountTopLabel.frame.origin.x, y: discountTopLabel.frame.origin.y, width: discountTopLabel.frame.width, height: discountViewHeightConstraint.constant)
                discountTopLabel.font = UIFont.systemFont(ofSize: 50, weight: .bold)
                discountTopLabel.text = couponItem.title
            }
            discountTopLabel.text = couponItem.title
            keyCountLabel.text = String(format: "%d", couponItem.requiredKeyCount)
            
            let cdSecs = CouponGoHelper.sharedInstance.getSecondsFromNow(date: couponItem.openAt)
            if cdSecs <= 0 {
                countdownView.isHidden = true
                if couponItem.requiredKeyCount == 0 {
                    keyView.isHidden = true
                    buttonViewTopConstraint.constant = buttonViewTop! - keyViewHeightConstraint.constant - countdownHeightConstraint.constant
                }
                else {
                    keyView.isHidden = false
                    keyViewTopConstraint.constant = keyViewTop! - countdownHeightConstraint.constant
                    buttonViewTopConstraint.constant = buttonViewTop! - countdownHeightConstraint.constant
                }
                setOkButton(enabled: true)
            }
            else{
                keyView.isHidden = true
                buttonViewTopConstraint.constant = buttonViewTop! - keyViewHeightConstraint.constant
                if countdownTimer?.isValid ?? true {
                    startCountdown()
                }
            }
            
            self.updateViewConstraints()
        }
    }
    
    func startCountdown(){
        refreshCountdown()
        countdownTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(refreshCountdown), userInfo: nil, repeats: true)
    }
    
    @objc func refreshCountdown(){
        let couponItem = item as! CGCoupon
        var secs = CouponGoHelper.sharedInstance.getSecondsFromNow(date: couponItem.openAt)
        var days = 0
        var hours = 0
        var mins = 0
        if secs > 0 {
            days = Int(secs / 86400)
            secs -= days * 86400
            hours = Int(secs / 3600)
            secs -= hours * 3600
            mins = Int(secs / 60)
            secs -= mins * 60
        }
        else{
            secs = 0
            countdownTimer?.invalidate()
        }
        
        DispatchQueue.main.async {
            if self.countdownTimer?.isValid ?? false {
                self.daysLabel.text = String(format: "%d", days)
                self.hoursLabel.text = String(format: "%02d", hours)
                self.minutesLabel.text = String(format: "%02d", mins)
                self.secondsLabel.text = String(format: "%02d", secs)
            }
            else {
                self.refresh()
            }
        }
    }
    
    private func setOkButton(enabled: Bool){
        okButton.isEnabled = enabled
        if enabled {
            okButton.backgroundColor = .black
        }
        else {
            okButton.backgroundColor = .lightGray
            okButton.alpha = 1.0
        }
    }
    
    @IBAction func okTouch(_ sender: Any) {
        /*if (item as? CGCoupon) != nil {
            if let mtb = self.presentingViewController as? MainTabBarController {
                if let svc = mtb.selectedViewController as? MapViewController {
                    svc.popupAcquire()
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
        else{
            //Ennek itt nem szabad soha meghívódnia
            self.dismiss(animated: true, completion: nil)
        }*/
        delegate?.mapSureAcquire()
    }
    
    @IBAction func closeTouch(_ sender: Any) {
        //dismiss(animated: true, completion: nil)
        delegate?.mapSureClose()
    }
    
    @IBAction func detailsTouch(_ sender: Any) {
        guard let coupon = item as? CGCoupon else { return }
        
        CouponGoHelper.sharedInstance.showCouponDetails(viewController: self, coupon: coupon, url: String(format: "/coupon/%d/info", coupon.campaignId))
    }
}

extension MapSureViewController: CouponCallbackDelegate{
    func couponBackTouched() {
        self.presentedViewController?.dismiss(animated: true, completion: nil)
    }
}
