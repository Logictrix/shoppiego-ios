//
//  CouponOfferViewController.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 05..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

class CouponOfferViewController: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var btnOk: UIButton!
    
    private var count = 0
    private var max = 0
    var isFree = false
    var coupon: CGCoupon?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if isFree {
            lblTitle.text = "couponoffer_title_offer".localized()
            lblText.text = "couponoffer_text_offer".localized()
        }
        else{
            lblTitle.text = "couponoffer_title_swap".localized()
            lblText.text = "couponoffer_text_swap".localized()
        }
        // TODO
        max = coupon?.freeCount ?? 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        refresh()
    }
    
    func refresh(){
        btnMinus.isHidden = count == 0
        btnPlus.isHidden = count == max
        btnOk.isHidden = count == 0
        
        lblCount.text = String(format: "%d", count)
        lblTitle.setCorrectLabelFrameHeight()
        lblText.setCorrectLabelFrameHeight()
    }
    
    @IBAction func closeTouch(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func minusTouch(_ sender: Any) {
        if count > 0{
            count -= 1
        }
        
        refresh()
    }
    
    @IBAction func plusTouch(_ sender: Any) {
        if count < max{
            count += 1
        }
        
        refresh()
    }
    
    @IBAction func okTouch(_ sender: Any) {
        let parameters: [String: String] = ["count": String(format: "%d", count),
                                            "is_free": (isFree ? "1" : "0")]
        CouponGoHelper.sharedInstance.commManager.request(method: "POST", url: String(format: "/coupon/%d/set/for-sale", coupon!.id), queryData: nil, postData: parameters) { (result, error) in
            if result != nil {
                DispatchQueue.main.async {
                    if let pvc = self.presentingViewController as? CouponViewController {
                        pvc.successOffer(count: self.count, isFree: self.isFree)
                    }
                    self.dismiss(animated: true, completion: nil)
                }
            }
            else if error != nil {
                CouponGoHelper.sharedInstance.showAlert(viewController: self, message: "error_server_error".localized(), animated: true, isError: true)
            }
        }
    }
}
