//
//  MessagesViewController.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 16..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

class MessagesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, ChatCallbackDelegate {
    
    @IBOutlet weak var messagesTableView: UITableView!
    
    private var messages : Array<CGMessage> = []
    private var page = 0
    private var pageLimit = 0
    private var hasMoreData = true
    private var isLoading = false
    private var loadTimer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        messagesTableView.delegate = self
        messagesTableView.dataSource = self
        messagesTableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        messages.removeAll()
        page = 0
        pageLimit = 0
        hasMoreData = true
        
        loadTimer?.invalidate()
        loadTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(timedRefresh), userInfo: nil, repeats: false)
    }
    
    @objc func timedRefresh(){
        refresh()
    }

    private func refresh(){
        if !hasMoreData {
            return
        }
        
        isLoading = true
        
        CouponGoHelper.sharedInstance.addLoadAnimation(parent: self.view)
        
        CouponGoHelper.sharedInstance.commManager.request(method: "GET", url: String(format: "/list?page=%d", page), queryData: nil, postData: nil, delayInMilliseconds: 500) { (result, error) in
            if let result = result {
                let dict = result as NSDictionary
                let data = dict.value(forKey: "data") as! NSDictionary
                self.pageLimit = CouponGoHelper.sharedInstance.jsonToInt(data.value(forKey: "limit"))
                let list = data.value(forKey: "list") as! NSArray
                for listItem in list {
                    let item = listItem as! NSDictionary
                    
                    let msg = CGMessage()
                    msg.userId = CouponGoHelper.sharedInstance.jsonToInt(item.object(forKey: "user_id"))
                    msg.profilePictureUrl = CouponGoHelper.sharedInstance.jsonToString(item.value(forKey: "profile_picture_url"))
                    msg.nickname = CouponGoHelper.sharedInstance.jsonToString(item.value(forKey: "user_nickname"))
                    msg.message = CouponGoHelper.sharedInstance.jsonToString(item.value(forKey: "message"))
                    msg.messageBy = CouponGoHelper.sharedInstance.jsonToString(item.value(forKey: "message_by"))
                    msg.createdAt = CouponGoHelper.sharedInstance.jsonToDateTime(item.value(forKey: "message_created_at"))
                    msg.isUnread = CouponGoHelper.sharedInstance.jsonToBool(item.value(forKey: "is_unread"))
                    msg.blockedByCurrentUser = CouponGoHelper.sharedInstance.jsonToBool(item.value(forKey: "blocked_by_current_user"))
                    msg.blockedByPartner = CouponGoHelper.sharedInstance.jsonToBool(item.value(forKey: "blocked_by_partner"))
                    
                    self.messages.append(msg)
                }
                
                self.hasMoreData = self.pageLimit == list.count
                self.page += 1
                
                DispatchQueue.main.async {
                    self.messagesTableView.reloadData()
                    
                    CouponGoHelper.sharedInstance.removeLoadAnimation(parent: self.view)
                    self.isLoading = false
                }
            }
            else if error != nil {
                CouponGoHelper.sharedInstance.removeLoadAnimation(parent: self.view)
                self.isLoading = false
                
                CouponGoHelper.sharedInstance.showAlert(viewController: self, message: "error_server_error".localized(), animated: true, isError: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = messagesTableView.dequeueReusableCell(withIdentifier: "MessageCell", for: indexPath) as! MessageCell
        if messages.count <= indexPath.item {
            return cell
        }
        
        let msg = messages[indexPath.item]
        
        if msg.profilePictureUrl.isEmpty {
            cell.profilePicture.image = UIImage(named: "avatar-mid-male")
        }
        else {
            cell.profilePicture.download(from: msg.profilePictureUrl)
        }
        cell.nicknameLabel.text = msg.nickname
        var font = UIFont.systemFont(ofSize: 13)
        if "exchange_successful" == msg.message {
            cell.messageLabel.text = "messages_exchange_successful".localized()
            font = UIFont.systemFont(ofSize: 13, weight: .semibold)
        }
        else {
            cell.messageLabel.text = msg.message
            if msg.isUnread {
                font = UIFont.systemFont(ofSize: 13, weight: .bold)
            }
        }
        cell.messageLabel.font = font
        
        if msg.createdAt != nil {
            let days = CouponGoHelper.sharedInstance.getDaysFromNow(date: msg.createdAt!)
            switch (days){
            case 0 :
                cell.datetimeLabel.text = "messages_today".localized(args: CouponGoHelper.sharedInstance.getTime(date: msg.createdAt!))
                break
            case -1:
                cell.datetimeLabel.text = "messages_yesterday".localized(args: CouponGoHelper.sharedInstance.getTime(date: msg.createdAt!))
                break
            default:
                cell.datetimeLabel.text = CouponGoHelper.sharedInstance.getMonthsAndDays(date: msg.createdAt!)
                break
            }
            cell.datetimeLabel.setLineSpacing(lineSpacing: 2.0)
            cell.datetimeLabel.textAlignment = .right
        }
        else {
            cell.datetimeLabel.text = ""
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if messages.count <= indexPath.item{
            return
        }
        
        let msg = messages[indexPath.item]
        
        let vc = CouponGoHelper.sharedInstance.getViewController(viewName: Constants.VC_CHAT) as! ChatViewController
        vc.partnerUserId = msg.userId
        vc.partnerNickname = msg.nickname
        vc.partnerProfilePictureUrl = msg.profilePictureUrl
        vc.blockedByCurrentUser = msg.blockedByCurrentUser
        vc.blockedByPartner = msg.blockedByPartner
        self.present(vc, animated: true, completion: nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.isEqual(messagesTableView) {
            if scrollView.panGestureRecognizer.translation(in: scrollView.superview).y < 0 {
                if (scrollView.contentOffset.y + scrollView.bounds.size.height + messagesTableView.rowHeight > scrollView.contentSize.height)
                {
                    if hasMoreData && !isLoading {
                        refresh()
                    }
                }
            }
        }
    }
    
    func deleteConversationHappened(_ userId: Int) {
        self.presentedViewController?.dismiss(animated: true, completion: nil)
        
        if let index = messages.firstIndex(where: { $0.userId == userId }) {
            messages.remove(at: index)
        }
        
        let q: [String: String] = ["partner_user_id": String(format: "%d", userId)]
        CouponGoHelper.sharedInstance.commManager.request(method: "DELETE", url: String(format: "/messages", page), queryData: q, postData: nil, delayInMilliseconds: 500) { (result, error) in
            if result != nil {
                DispatchQueue.main.async {
                    self.messagesTableView.reloadData()
                }
            }
            else if error != nil {
                
            }
        }
    }
}
