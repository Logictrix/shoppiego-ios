//
//  ViewController.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 02. 14..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import SafariServices

class StartViewController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var tvTerms: UITextView!
    @IBOutlet weak var carouselScrollView: UIScrollView!
    @IBOutlet weak var dotView: UIView!
    @IBOutlet weak var lblTxt: UILabel!
    
    private var carouselTexts: [String] = []
    private var carouselDots: Dictionary<Int, UIImageView> = [:]
    private var timer: Timer?
    private var carouselCnt = 0
    private var carouselIdx = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        carouselScrollView.isHidden = true
        dotView.isHidden = true
        lblTxt.text  = "Go_get_lost_in_the_coupan_jungle".localized()
        tvTerms.hyperlink(str: "start_terms".localized(), args: Constants.URL_TERMS, Constants.URL_PRIVACY, underline: true)
        tvTerms.delegate = self
        
        carouselTexts.append("start_carousel1".localized())
        carouselTexts.append("start_carousel2".localized())
        carouselTexts.append("start_carousel3".localized())
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(swipeHandler))
        swipeLeft.direction = .left
        carouselScrollView.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(swipeHandler))
        swipeRight.direction = .right
        carouselScrollView.addGestureRecognizer(swipeRight)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
        carouselCnt = carouselTexts.count
        var dotX: CGFloat = 0.0
        let dotLen = (dotView.frame.width - 10.0) / CGFloat(carouselCnt - 1)
        var carouselX: CGFloat = 0.0
        
        for i in 0 ... (carouselCnt - 1) {
            let v = UIView(frame: CGRect(x: carouselX, y: 0, width: carouselScrollView.frame.width, height: carouselScrollView.frame.height))
            
            let lbl = UILabel(frame: CGRect(x: 60.0, y: 0, width: v.frame.width - 120.0, height: 35))
            lbl.font = UIFont.systemFont(ofSize: 13, weight: .bold)
            lbl.textAlignment = .center
            lbl.text = carouselTexts[i]
            lbl.numberOfLines = 0
            lbl.lineBreakMode = .byWordWrapping
            
            let img = UIImageView(frame: CGRect(x: 0, y: 40.0, width: v.frame.width, height: 240.0))
            //img.image = UIImage(named: String(format: "carousel-%d", (i + 1)))
            //img.image = UIImage(named: String(format: "login-%d", (i + 1)))
            img.image = UIImage(named: String(format: "login_%d", (i + 1)))
            img.contentMode = .scaleAspectFit
            v.addSubview(lbl)
            v.addSubview(img)
            
            carouselScrollView.addSubview(v)
            carouselX += carouselScrollView.frame.width
            
            let dotImg = UIImageView(frame: CGRect(x: dotX, y: 0, width: 10, height: 10))
            dotImg.image = UIImage(named: "carousel-dot")
            carouselDots[i] = dotImg
            dotX += dotLen
            
            dotView.addSubview(dotImg)
        }
        carouselScrollView.contentSize = CGSize(width: carouselX, height: carouselScrollView.frame.height)
        carouselRefresh()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        let csubViews = carouselScrollView.subviews
        for subview in csubViews{
            subview.removeFromSuperview()
        }
        
        let dsubViews = dotView.subviews
        for subview in dsubViews{
            subview.removeFromSuperview()
        }
        
        carouselDots.removeAll()
        
        timer?.invalidate()
    }
    
    private func carouselRefresh(){
        if carouselIdx >= carouselCnt {
            return
        }
        
        for i in 0 ... (carouselCnt - 1) {
            carouselDots[i]!.image = UIImage(named: (i == carouselIdx ? "carousel-dot-black" : "carousel-dot"))
        }
        
        carouselScrollView.setContentOffset(CGPoint(x: CGFloat(carouselIdx) * carouselScrollView.frame.width, y: 0), animated: true)
        timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(carouselGoToNext), userInfo: nil, repeats: false)
    }
    
    @objc private func carouselGoToNext(){
        carouselIdx += 1
        if carouselIdx == carouselCnt {
            carouselIdx = 0
        }
        carouselRefresh()
    }
    
    @objc private func swipeHandler(gesture: UISwipeGestureRecognizer){
        timer?.invalidate()
        
        if gesture.direction == .left {
            carouselIdx += 1
            if carouselIdx == carouselCnt {
                carouselIdx = carouselCnt - 1
            }            
        }
        else if gesture.direction == .right {
            carouselIdx -= 1
            if carouselIdx < 0 {
                carouselIdx = 0
            }
        }
        
        carouselRefresh()
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        print("URL",URL)
        let svc = SFSafariViewController(url: URL)
        present(svc, animated: true, completion: nil)
         
        return false
    }
    
    @IBAction func backTouch(_ sender: Any) {
          CouponGoHelper.sharedInstance.showViewController(viewName: Constants.VC_SLIDER)
    }
    
    @IBAction func loginTouch(_ sender: Any) {
        CouponGoHelper.sharedInstance.showViewController(viewName: Constants.VC_LOGIN)
    }
    
    @IBAction func registrationTouch(_ sender: Any) {
        CouponGoHelper.sharedInstance.showViewController(viewName: Constants.VC_REGISTRATION)
    }
    
    @IBAction func facebookTouch(_ sender: Any) {
        let fbLoginManager : LoginManager = LoginManager()
        fbLoginManager.logIn(permissions: ["email"], from: self) { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : LoginManagerLoginResult = result!
                if (result?.isCancelled)!{
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email"))
                {
                    if let accessToken = fbloginresult.token?.tokenString {
                        let p: [String: String] = ["access_token" : accessToken]
                        
                        CouponGoHelper.sharedInstance.commManager.request(method: "POST", url: "/auth/login-with-facebook", queryData: nil, postData: p) { (result, error) in
                            if let result = result {
                                let token = result.value(forKey: "access_token") as! String
                                
                                DispatchQueue.main.async {
                                    CouponGoHelper.sharedInstance.accessToken = token
                                    DataManager.sharedInstance.setSession(Constants.SESSION_USER_ACCESS_TOKEN, value: token)
                                    CouponGoHelper.sharedInstance.updateData()
                                    CouponGoHelper.sharedInstance.showMainTabBar()
                                }
                            }
                            else if error != nil {
                                CouponGoHelper.sharedInstance.showAlert(viewController: self, message: "error_server_error".localized(), animated: true, isError: true)
                            }
                        }
                    }
                }
            }
            else {
                CouponGoHelper.sharedInstance.showAlert(viewController: self, message: "start_fb_error".localized(), animated: true, isError: true)
            }
        }
    }
    
    
}

