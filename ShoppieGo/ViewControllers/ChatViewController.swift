//
//  ChatViewController.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 14..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

protocol ChatCallbackDelegate {
    func deleteConversationHappened(_ userId: Int)
}

class ChatViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, CGChatDelegate {
    
    @IBOutlet weak var chatScrollView: UIScrollView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var chatTableView: UITableView!
    @IBOutlet weak var messageField: UITextField!
    @IBOutlet weak var leftScrollView: UIScrollView!
    @IBOutlet weak var leftContentView: UIView!
    @IBOutlet weak var rightScrollView: UIScrollView!
    @IBOutlet weak var rightContentView: UIView!
    @IBOutlet weak var tradeView: UIRoundedView!
    @IBOutlet weak var partnerProfileImage: UIRoundedImageView!
    @IBOutlet weak var partnerNicknameLabel: UILabel!
    @IBOutlet weak var partnerTradeNicknameLabel: UILabel!
    @IBOutlet weak var partnerTradeProfileImage: UIRoundedImageView!
    @IBOutlet weak var myTradeProfileImage: UIRoundedImageView!
    @IBOutlet weak var acceptButton: UIRoundedButton!
    
    @IBOutlet var chatTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var chatTableViewTopConstraint: NSLayoutConstraint!
    @IBOutlet var messageViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet var chatScrollViewBottomConstraint: NSLayoutConstraint!
    
    var partnerUserId = 0
    var partnerProfilePictureUrl = ""
    var partnerNickname = ""
    var blockedByCurrentUser = false
    var blockedByPartner = false
    var delegate: ChatCallbackDelegate?
    private var userId = 0
    private var profilePictureUrl = ""
    private var ownTradeItems: CGTradeItems?
    private var partnerTradeItems: CGTradeItems?
    private var pageLimit = 0
    private var page = 0
    private var hasMoreData = true
    private var isLoading = false
    private var needRefresh = true

    private var chat: Array<CGChatMessage> = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        userId = DataManager.sharedInstance.getSessionInt(Constants.SESSION_USER_ID) ?? 0
        profilePictureUrl = DataManager.sharedInstance.getSessionString(Constants.SESSION_USER_PROFILE_PICTURE_URL) ?? ""

        chatTableView.delegate = self
        chatTableView.dataSource = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        messageField.delegate = self
        
        NotificationHandler.chatDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !partnerProfilePictureUrl.isEmpty {
            partnerProfileImage.download(from: partnerProfilePictureUrl)
            partnerTradeProfileImage.download(from: partnerProfilePictureUrl)
        }
        else{
            partnerProfileImage.image = UIImage(named: "avatar-sml-male")
            partnerTradeProfileImage.image = UIImage(named: "avatar-sml-male")
        }
        partnerNicknameLabel.text = partnerNickname
        partnerTradeNicknameLabel.text = partnerNickname
        
        if !profilePictureUrl.isEmpty {
            myTradeProfileImage.download(from: profilePictureUrl)
        }
        else{
            myTradeProfileImage.image = UIImage(named: "avatar-sml-male")
        }
        
        refresh()
        needRefresh = true
    }
    
    private func refresh(updateBoxes: Bool = true, updateMessages: Bool = true){
        if !needRefresh {
            return
        }
        
        isLoading = true
        CouponGoHelper.sharedInstance.addLoadAnimation(parent: self.view)
        
        let q: [String: String] = ["partner_user_id" : String(format: "%d", partnerUserId),
                                   "page": String(format: "%d", page)]
        
        CouponGoHelper.sharedInstance.commManager.request(method: "GET", url: "/messages", queryData: q, postData: nil) { (result, error) in
            if let result = result {
                 DispatchQueue.main.async {
                    let dict = result as NSDictionary
                    let data = dict.value(forKey: "data") as! NSDictionary
                    self.pageLimit = CouponGoHelper.sharedInstance.jsonToInt(data.value(forKey: "limit"))
                    
                    if updateMessages {
                        self.chat.removeAll()
                        
                        let messages = data.value(forKey: "messages") as! NSArray
                        self.hasMoreData = self.pageLimit == messages.count
                        if messages.count > 0 {
                            self.page += self.hasMoreData ? 1 : 0
                            for message in messages {
                                let item = message as! NSDictionary
                                
                                let msg = CGChatMessage()
                                msg.userId = CouponGoHelper.sharedInstance.jsonToInt(item.value(forKey: "user_id"))
                                msg.message = CouponGoHelper.sharedInstance.jsonToString(item.value(forKey: "message"))
                                msg.isSystem = msg.userId == 0
                                
                                //self.chat.append(msg)
                                self.addNewMessage(msg)
                            }
                        }
                        self.chatTableView.reloadData()
                    }
                    
                    if updateBoxes {
                        let boxes = data.value(forKey: "boxes") as! NSDictionary
                        self.ownTradeItems = CouponGoHelper.sharedInstance.getTradeItems(boxes.value(forKey: "own"))
                        self.fillTradeView(true)
                        self.partnerTradeItems = CouponGoHelper.sharedInstance.getTradeItems(boxes.value(forKey: "partner"))
                        self.fillTradeView(false)
                    }
                    
                    CouponGoHelper.sharedInstance.removeLoadAnimation(parent: self.view)
                    self.isLoading = false
                }
            }
            else if error != nil {
                CouponGoHelper.sharedInstance.removeLoadAnimation(parent: self.view)
                self.isLoading = false
                
                CouponGoHelper.sharedInstance.showAlert(viewController: self, message: "error_server_error".localized(), animated: true, isError: true)
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return false
    }
    
    @objc func keyboardWillShow(notification: NSNotification){
        if let userInfo = notification.userInfo as? Dictionary<String, AnyObject>{
            let frame = userInfo[UIResponder.keyboardFrameEndUserInfoKey]
            let keyboardRect = frame?.cgRectValue
            if let keyboardHeight = keyboardRect?.height {
                chatScrollViewBottomConstraint.constant = 0
                messageViewBottomConstraint.constant = keyboardHeight
                
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    self.chatScrollView.scrollToBottom()
                }
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification){
        messageViewBottomConstraint.constant = 0
        chatScrollViewBottomConstraint.constant = tradeView.frame.height + 8
    }
    
    @IBAction func backTouch(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func changeTouch(_ sender: Any) {
        let vc = CouponGoHelper.sharedInstance.getViewController(viewName: Constants.VC_CHAT_ITEMS) as! ChatItemsViewController
        
        var selCoupons: Array<Int> = []
        
        if ownTradeItems != nil {
            for coupon in ownTradeItems!.coupons {
                selCoupons.append(coupon.id)
            }
            
            vc.selectedCoupons = selCoupons
            vc.keyCount = ownTradeItems!.keyCount
        }
        
        needRefresh = false
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func acceptTouch(_ sender: Any) {
        guard let pti = partnerTradeItems else { return }
        
        if pti.isAccepted {
            let p: [String: String] = ["partner_user_id" : String(format: "%d", partnerUserId)]
            
            CouponGoHelper.sharedInstance.commManager.request(method: "POST", url: "/message/box/cancel", queryData: nil, postData: p) { (result, error) in
                if result != nil {
                    DispatchQueue.main.async {
                        self.refresh(updateBoxes: true, updateMessages: false)
                    }
                }
                else if error != nil {
                    
                }
            }
        }
        else {
            let p: [String: String] = ["partner_user_id" : String(format: "%d", partnerUserId)]
            
            CouponGoHelper.sharedInstance.commManager.request(method: "POST", url: "/message/box/accept", queryData: nil, postData: p) { (result, error) in
                if let result = result {
                    let dict = result as NSDictionary
                    let data = dict.value(forKey: "data") as! NSDictionary
                    let boxChanged = CouponGoHelper.sharedInstance.jsonToBool(data.value(forKey: "box_changed"))
                    let exchangeSuccess = CouponGoHelper.sharedInstance.jsonToBool(data.value(forKey: "exchange_success"))
                    
                    DispatchQueue.main.async {
                        if boxChanged {
                            if let pti = self.partnerTradeItems {
                                pti.isAccepted = false
                            }
                        }
                        else if exchangeSuccess {
                            self.exchangeSuccess()
                            
                            let msg = CGChatMessage()
                            msg.userId = 0 // rendszerüzenet
                            msg.message = "chat_exchange_successful".localized()
                            msg.isSystem = true // rendszerüzenet
                            
                            self.addNewMessage(msg)
                        }
                        self.refresh(updateBoxes: true, updateMessages: false)
                    }
                }
                else if error != nil {
                    CouponGoHelper.sharedInstance.showAlert(viewController: self, message: "error_server_error".localized(), animated: true, isError: true)
                }
            }
        }
        
        pti.isAccepted = !pti.isAccepted
        fillTradeView(false)
        
    }
    
    @IBAction func sendTouch(_ sender: Any) {
        let p: [String: String] = ["partner_user_id" : String(format: "%d", partnerUserId),
                                   "message" : messageField.text!]
        
        CouponGoHelper.sharedInstance.commManager.request(method: "POST", url: "/message", queryData: nil, postData: p) { (result, error) in
            if result != nil {
                DispatchQueue.main.async {
                    let msg = CGChatMessage()
                    msg.userId = self.userId
                    msg.message = self.messageField.text!
                    self.chat.append(msg)
                    self.messageField.text = ""
                    
                    let contentOffset = self.chatTableView.contentOffset
                    self.chatTableView.reloadData()
                    self.chatTableView.layoutIfNeeded()
                    self.chatTableView.setContentOffset(contentOffset, animated: false)
                    self.chatTableView.scrollToBottom()
                }
            }
            else if error != nil {
                CouponGoHelper.sharedInstance.showAlert(viewController: self, message: "error_server_error".localized(), animated: true, isError: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chat.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let chatItem = chat[indexPath.item]
        
        if chatItem.isSystem {
            let cell = chatTableView.dequeueReusableCell(withIdentifier: "ChatSystemCell", for: indexPath) as! ChatSystemCell
            if "exchange_successful" == chatItem.message{
                cell.messageLabel.text = "chat_exchange_successful".localized()
            }
            else {
                cell.messageLabel.text = chatItem.message
            }
            
            return cell
        }
        else if chatItem.userId == userId {
            let cell = chatTableView.dequeueReusableCell(withIdentifier: "ChatOwnCell", for: indexPath) as! ChatOwnCell
            if !profilePictureUrl.isEmpty {
                cell.avatarImage.download(from: profilePictureUrl)
            }
            else {
                cell.avatarImage.image = UIImage(named: "avatar-sml-male")
            }
            cell.messageLabel.text = chatItem.message
            
            return cell
        }
        else{
            let cell = chatTableView.dequeueReusableCell(withIdentifier: "ChatPartnerCell", for: indexPath) as! ChatPartnerCell
            if !chatItem.profilePictureUrl.isEmpty {
                cell.avatarImage.download(from: chatItem.profilePictureUrl)
            }
            else {
                cell.avatarImage.image = UIImage(named: "avatar-sml-male")
            }
            cell.messageLabel.text = chatItem.message
            
            return cell
        }
    }
    
    private func fillTradeView(_ isOwn: Bool){
        let contentView = (isOwn ? rightContentView : leftContentView)!
        let scrollView = (isOwn ? rightScrollView : leftScrollView)!
        let tradeItems = isOwn ? ownTradeItems : partnerTradeItems
        
        removeAllSubView(parent: contentView)
        
        var hasItems = false
        if let ti = tradeItems {
            let w = tradeView.frame.width / 4
            contentView.frame.size.height = 0
            
            var x : CGFloat = 0
            var y : CGFloat = 0
            
            var idx = 0
            if ti.keyCount > 0 {
                let v = addKeyView(x: x, y: y, w: w, keyCount: ti.keyCount)
                idx += 1
                x += w
                contentView.addSubview(v)
                hasItems = true
            }
            
            
            for coupon in ti.coupons {
                let v = addCoupon(x: x, y: y, w: w, coupon: coupon, isOwn: isOwn)
                
                if idx % 2 == 0{
                    x += w
                }
                else{
                    x = 0
                    y += 90
                }
                idx += 1
                
                contentView.addSubview(v)
                hasItems = true
            }
            contentView.frame.size.height = CGFloat(Int((idx + 1) / 2) * 90)
            scrollView.backgroundColor = ti.isAccepted ? UIColor(red: 106, green: 244, blue: 88, alpha: 59) : .white
            
            if !isOwn {
                if ti.isAccepted {
                    acceptButton.setImage(UIImage(named: "chat-close"), for: .normal)
                    acceptButton.setTitle("chat_cancel".localized(), for: .normal)
                }
                else {
                    acceptButton.setImage(UIImage(named: "chat-accept"), for: .normal)
                    acceptButton.setTitle("chat_accept".localized(), for: .normal)
                }
            }
        }
        
        scrollView.contentSize = CGSize(width: contentView.frame.width, height: contentView.frame.height)
        
        if hasItems {
            CouponGoHelper.sharedInstance.removeNoResults(parent: scrollView)
        }
        else {
            CouponGoHelper.sharedInstance.addNoResults(parent: scrollView, text: (isOwn ? "chat_no_result_own" : "chat_no_result_partner").localized(), isVerticallyCentered: true, parentHeight: rightScrollView.frame.height)
        }
    }
    
    private func addKeyView(x: CGFloat, y: CGFloat, w: CGFloat, keyCount: Int) -> UIView{
        let v = UIView(frame: CGRect(x: x, y: y, width: w, height: 90))
        v.tag = Constants.TAG_CHAT_KEY_VIEW
        
        let img = UIImageView(frame: CGRect(x: (w - 38) / 2, y: 22.5, width: 38, height: 45))
        img.image = UIImage(named: "key")
        img.contentMode = .scaleToFill
        
        let cv = UIRoundedView(frame: CGRect(x: img.frame.origin.x + 10, y: 12.5, width: 20, height: 20))
        cv.cornerRadius = 10
        cv.backgroundColor = .black
        
        let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        lbl.text = String(format: "%d", keyCount)
        lbl.textColor = .white
        lbl.font = UIFont.systemFont(ofSize: 11, weight: .bold)
        lbl.textAlignment = .center
        
        v.addSubview(img)
        cv.addSubview(lbl)
        v.addSubview(cv)
        
        return v
    }
    
    private func removeKeyView(parent: UIView){
        for v in parent.subviews {
            if v.tag == Constants.TAG_CHAT_KEY_VIEW {
                v.removeFromSuperview()
                break
            }
        }
    }
    
    private func addCoupon(x: CGFloat, y: CGFloat, w: CGFloat, coupon: CGCoupon, isOwn: Bool) -> UIView{
        let v = UIView(frame: CGRect(x: x, y: y, width: w, height: 90))
        v.tag = Constants.TAG_CHAT_COUPON_VIEW
        
        let img = UIImageView(frame: CGRect(x: (w - 64) / 2, y: 5, width: 64, height: 80))
        img.image = UIImage(named: String(format: "coupon-%d", coupon.id % 6))
        img.contentMode = .scaleToFill
        
        let brandImg = UIRoundedImageView(frame: CGRect(x: 16, y: 9, width: 32, height: 32))
        brandImg.cornerRadius = 16
        brandImg.borderWidth = 1
        brandImg.borderColor = .black
        brandImg.backgroundColor = .white
        brandImg.download(from: coupon.brandImageUrl)
        brandImg.contentMode = .scaleAspectFit
        
        let lbl = UILabel(frame: CGRect(x: 4, y: 52, width: 56, height: 20))
        lbl.text = coupon.isFixed ? "chat_fix".localized() : coupon.title
        lbl.textColor = .black
        lbl.font = UIFont.systemFont(ofSize: 8, weight: .bold)
        lbl.textAlignment = .center
        lbl.numberOfLines = 2
        
        img.addSubview(brandImg)
        img.addSubview(lbl)
        v.addSubview(img)
        
        let tap = UICouponTapGestureRecognizer(target: self, action: #selector(self.couponTap(_:)))
        tap.coupon = coupon
        tap.isOwn = isOwn
        v.addGestureRecognizer(tap)
        
        return v
    }
    
    private func removeAllSubView(parent: UIView){
        for v in parent.subviews {
            v.removeFromSuperview()
        }
    }
    
    private func exchangeSuccess(){
        self.partnerTradeItems?.clear()
        self.ownTradeItems?.clear()
        
        DataManager.sharedInstance.clearCouponTable()
        CouponGoHelper.sharedInstance.updateUser()
        self.refresh(updateBoxes: true, updateMessages: false)
    }
    
    private func addNewMessage(_ msg: CGChatMessage){
        chat.append(msg)
        chatTableView.reloadData()
        
        chatTableViewHeightConstraint.constant = CGFloat(chat.count) * chatTableView.rowHeight
        chatTableViewTopConstraint.constant = chatScrollView.frame.height - infoView.frame.height - chatTableViewHeightConstraint.constant
        if chatTableViewTopConstraint.constant < 0.0 {
            chatTableViewTopConstraint.constant = 0.0
        }
        chatScrollView.contentSize = CGSize(width: chatScrollView.contentSize.width, height: infoView.frame.height + chatTableViewTopConstraint.constant + chatTableViewHeightConstraint.constant)
        chatScrollView.scrollToBottom(animated: false)
        self.updateViewConstraints()
    }
    
    func chatMessageReceived(message: CGChatMessage){
        if message.userId != partnerUserId {
            return
        }
        
        DispatchQueue.main.async {
            if message.isSystem && "exchange_successful" == message.message {
                self.exchangeSuccess()
            }
            
            self.addNewMessage(message)
        }
    }
    
    func chatBoxesReceived(userId: Int, ownTradeItems: CGTradeItems?, partnerTradeItems: CGTradeItems?) {
        if userId != partnerUserId {
            return
        }
        self.ownTradeItems = ownTradeItems
        if self.ownTradeItems != nil {
            fillTradeView(true)
        }
        
        self.partnerTradeItems = partnerTradeItems
        if self.partnerTradeItems != nil {
            fillTradeView(false)
        }
    }
    
    func setTradeItems(items: CGTradeItems){
        ownTradeItems = items
        fillTradeView(true)
        
        let couponIds = items.coupons.map({ "\($0.id)" }).joined(separator: ",")
        
        let p: [String: String] = ["partner_user_id" : String(format: "%d", partnerUserId),
                                   "keys": String(format: "%d", items.keyCount),
                                   "coupons": couponIds]
        
        CouponGoHelper.sharedInstance.commManager.request(method: "POST", url: "/message/box", queryData: nil, postData: p) { (result, error) in
            if result != nil {
                
            }
            else if error != nil {
                CouponGoHelper.sharedInstance.showAlert(viewController: self, message: "error_server_error".localized(), animated: true, isError: true)
            }
        }
    }
    
    @objc func couponTap(_ sender: UICouponTapGestureRecognizer) {
        guard let coup = sender.coupon else { return }
        let url = String(format: (sender.isOwn ? "/coupon/%d/own" : "/coupon/%d/info"), coup.id)
        CouponGoHelper.sharedInstance.commManager.request(method: "GET", url: url, queryData: nil, postData: nil) { (result, error) in
            if let result = result {
                let dict = result as NSDictionary
                let obj = dict.value(forKey: "coupon") as! NSDictionary
                
                let coupon = CouponGoHelper.sharedInstance.jsonToCoupon(obj)
                
                DispatchQueue.main.async {
                    self.needRefresh = false
                    let vc = CouponGoHelper.sharedInstance.getViewController(viewName: Constants.VC_COUPON) as! CouponViewController
                    vc.coupon = coupon
                    vc.isOwn = sender.isOwn
                    self.present(vc, animated: true, completion: nil)
                }
            }
            else if error != nil {
                CouponGoHelper.sharedInstance.showAlert(viewController: self, message: "error_server_error".localized(), animated: true, isError: true)
            }
        }
    }
    
    @IBAction func infoTouch(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        if blockedByCurrentUser {
            alert.addAction(UIAlertAction(title: "chat_unblock_user".localized(args: partnerNickname), style: .default, handler: {action in
                self.blockUser()
            }))
        }
        else {
            alert.addAction(UIAlertAction(title: "chat_block_user".localized(args: partnerNickname), style: .default, handler: {action in
                self.blockUser()
            }))
        }
        alert.addAction(UIAlertAction(title: "chat_alert_cancel".localized(), style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "chat_delete_conversation".localized(), style: .destructive, handler: {action in
            self.deleteConversation()
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    private func blockUser(){
        print("block")
        let parameters: [String: String] = ["user_id": String(format: "%d", partnerUserId)]
        
        CouponGoHelper.sharedInstance.commManager.request(method: "POST", url: "/user/block/add", queryData: nil, postData: parameters) { (result, error) in
            if result != nil {
                self.blockedByCurrentUser = true
            } else if error != nil {
            }
        }
    }
    
    private func unblockUser(){
        print("unblock")
        let parameters: [String: String] = ["user_id": String(format: "%d", partnerUserId)]
        
        CouponGoHelper.sharedInstance.commManager.request(method: "POST", url: "/user/block/revoke", queryData: nil, postData: parameters) { (result, error) in
            if result != nil {
                self.blockedByCurrentUser = false
            } else if error != nil {
            }
        }
    }
    
    private func deleteConversation(){
        print("delete")
        self.delegate?.deleteConversationHappened(partnerUserId)
    }
}
