//
//  LoginVC.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 02. 14..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit
import SafariServices

class LoginViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate {

    @IBOutlet weak var eyeButton: UIButton!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UIPaddingTextField!
    @IBOutlet weak var lostPasswordTextFiled: UITextView!
    @IBOutlet weak var inputBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var loginButton: UIRoundedButton!
    @IBOutlet weak var lblTxt: UILabel!
    
    
    private var defaultBottomConstraint: CGFloat = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        eyeButton.isHidden = true
        emailField.delegate = self
        passwordField.delegate = self
        loginButton.AddshadowOnButton()
        lblTxt.text  = "Go_get_lost_in_the_coupan_jungle".localized()
        
        defaultBottomConstraint = inputBottomConstraint.constant
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        lostPasswordTextFiled.hyperlink(str: "login_lost_password".localized(), args: "https://app.shoppiego.hu/elfelejtett-jelszo", underline: true)
        lostPasswordTextFiled.delegate = self
        
        emailField.changePlaceHolder(placeHolderTxt: "E-mail address", color: UIColor.darkGray)
        passwordField.changePlaceHolder(placeHolderTxt: "Password", color: UIColor.darkGray)
        
        emailField.AddRoundLine()
        passwordField.AddRoundLine()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField as? UIPaddingTextField != nil {
            textField.resignFirstResponder()
        }
        else {
            passwordField.becomeFirstResponder()
        }
        
        return false
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        let svc = SFSafariViewController(url: URL)
        present(svc, animated: true, completion: nil)
        
        return false
    }
    
    @objc func keyboardWillShow(notification: NSNotification){
        if let userInfo = notification.userInfo as? Dictionary<String, AnyObject>{
            let frame = userInfo[UIResponder.keyboardFrameEndUserInfoKey]
            let keyboardRect = frame?.cgRectValue
            if let keyboardHeight = keyboardRect?.height {
                self.inputBottomConstraint.constant = keyboardHeight
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification){
        self.inputBottomConstraint.constant = defaultBottomConstraint
    }
    
    @IBAction func backTouch(_ sender: Any) {
        CouponGoHelper.sharedInstance.showViewController(viewName: Constants.VC_START)
    }
    
    @IBAction func eyeTouch(_ sender: Any) {
        let btn = sender as! UICheckbox
        passwordField.isSecureTextEntry = btn.checked
    }
    
    private func enableControls(_ val: Bool = true){
        emailField.isEnabled = val
        passwordField.isEnabled = val
        eyeButton.isEnabled = val
        loginButton.isEnabled = val
    }
    
    @IBAction func loginTouch(_ sender: Any) {
        
        let parameters: [String: String] = ["email": emailField.text!,
                                            "password": passwordField.text!]
        
//        let parameters: [String: String] = ["email":"d.lencses@marquardmedia.hu",
//            "password": "Sinozuka82"]
        
//        let parameters: [String: String] = ["email":"work4jas@gmail.com",
//                                            "password": "ltrix123"]
        
        
        enableControls(false)
        
        CouponGoHelper.sharedInstance.commManager.request(method: "POST", url: "/auth/login", queryData: nil, postData: parameters) { (result, error) in
            if let result = result {
                print("result",result)
                let token = result.value(forKey: "access_token") as! String
                
                DispatchQueue.main.async {
                    CouponGoHelper.sharedInstance.accessToken = token
                    DataManager.sharedInstance.setSession(Constants.SESSION_USER_ACCESS_TOKEN, value: token)
                    CouponGoHelper.sharedInstance.updateData()
                    self.enableControls()
                    CouponGoHelper.sharedInstance.showMainTabBar()
                }
            } else if error != nil {
                DispatchQueue.main.async {
                    self.enableControls()
                }
                CouponGoHelper.sharedInstance.showAlert(viewController: self, title: "", message: "login_bad_username_or_password".localized())
            }
        }
    }
    
    @IBAction func passwordChanged(_ sender: Any) {
        if passwordField.text == "" {
            eyeButton.isHidden = true
            passwordField.paddingRight = 0
        }
        else{
            eyeButton.isHidden = false
            passwordField.paddingRight = eyeButton.frame.width + 8
        }
    }
}



extension UITextField{
    
    func AddRoundLine(){
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.darkGray.cgColor
    }
}

