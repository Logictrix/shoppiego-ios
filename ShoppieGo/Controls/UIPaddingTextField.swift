//
//  UIPaddingTextField.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 02. 19..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

class UIPaddingTextField: UITextField {
    
    @IBInspectable var paddingLeft: CGFloat = 8
    @IBInspectable var paddingRight: CGFloat = 8
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + paddingLeft, y: bounds.origin.y,
                      width: bounds.size.width - paddingLeft - paddingRight, height: bounds.size.height);
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
}
