//
//  UICheckbox.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 02. 28..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

@IBDesignable class UICheckbox: UIButton {
    @IBInspectable var checked:Bool = false{
        didSet{
            updateDisplay()
        }
    }
    
    @IBInspectable var onImage:UIImage! = nil{
        didSet{
            updateDisplay()
        }
    }
    
    @IBInspectable var offImage:UIImage! = nil{
        didSet{
            updateDisplay()
        }
    }
    
    func updateDisplay(){
        if checked {
            if let onImage = onImage{
                setImage(onImage, for: .normal)
            }
        } else {
            if let offImage = offImage{
                setImage(offImage, for: .normal)
            }
        }
    }
    
    override func endTracking(_ touch: UITouch?, with event: UIEvent?) {
        super.endTracking(touch, with: event)
        checked = !checked
    }
}
