//
//  UIContentSizedCollectionView.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 14..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

final class UIContentSizedCollectionView: UICollectionView {
    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        return self.contentSize
    }
    
    override var contentSize: CGSize {
        didSet{
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override func reloadData() {
        super.reloadData()
        self.invalidateIntrinsicContentSize()
    }
}
