//
//  UIRoundedButton.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 02. 28..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

@IBDesignable class UIRoundedButton: UIBorderedButton {
    @IBInspectable var cornerRadius: CGFloat = 0{
        didSet{
            layoutSubviews()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = cornerRadius
    }
}
