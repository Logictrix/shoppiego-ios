//
//  UIRoundedTextField.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 02. 28..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

@IBDesignable class UIRoundedTextField: UITextField {
    @IBInspectable var cornerRadius: CGFloat = 0{
        didSet{
            layoutSubviews()
        }
    }
    
    @IBInspectable var paddingLeft: CGFloat = 0{
        didSet{
            invalidateIntrinsicContentSize()
        }
    }
    
    @IBInspectable var paddingRight: CGFloat = 0{
        didSet{
            invalidateIntrinsicContentSize()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = cornerRadius
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + paddingLeft, y: bounds.origin.y + 1,
                      width: bounds.size.width - paddingLeft - paddingRight, height: bounds.size.height - 1);
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
}
