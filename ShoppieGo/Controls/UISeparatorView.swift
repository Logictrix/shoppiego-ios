//
//  UISeparatorView.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 11..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

@IBDesignable class UISeparatorView: UIView {
    
    @IBInspectable var shadowColor: UIColor? = .black {
        didSet {
            layoutSubviews()
        }
    }
    
    private func getSeparatorCorner(x: CGFloat, y: CGFloat, radius: CGFloat) -> UIView{
        let view = UIRoundedView(frame: CGRect(x: x, y: y, width: radius * 2, height: radius * 2))
        view.cornerRadius = radius
        view.backgroundColor = .white
        
        return view
    }
    
    private func buildSeparator(){
        let radius = (self.frame.height - 1) / 2
        
        let vAll = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        vAll.backgroundColor = shadowColor
        vAll.clipsToBounds = true
        
        let vTopLeft = getSeparatorCorner(x: 0, y: -1 * radius, radius: radius)
        let vTopRight = getSeparatorCorner(x: vAll.frame.width - (2 * radius), y: -1 * radius, radius: radius)
        let vBottomLeft = getSeparatorCorner(x: 0, y: vAll.frame.height - radius, radius: radius)
        let vBottomRight = getSeparatorCorner(x: vAll.frame.width - (2 * radius), y: vAll.frame.height - radius, radius: radius)
        let vCenter = UIView(frame: CGRect(x: radius, y: 0, width: vAll.frame.width - (2 * radius), height: vAll.frame.height))
        vCenter.backgroundColor = .white
        let vLine = UIImageView(frame: CGRect(x: radius, y: radius, width: vAll.frame.width - (2 * radius), height: 1))
        vLine.image = UIImage(named: "dashed-separator")
        if (vLine.image == nil) {
            vLine.backgroundColor = shadowColor
        }
        
        vAll.addSubview(vTopLeft)
        vAll.addSubview(vTopRight)
        vAll.addSubview(vBottomLeft)
        vAll.addSubview(vBottomRight)
        vAll.addSubview(vCenter)
        vAll.addSubview(vLine)
        
        self.addSubview(vAll)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        buildSeparator()
    }
}
