//
//  UIContentSizedTableView.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 07..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

final class UIContentSizedTableView: UITableView {
    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        return self.contentSize
    }
    
    override var contentSize: CGSize {
        didSet{
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override func reloadData() {
        super.reloadData()
        self.invalidateIntrinsicContentSize()
    }
}
