//
//  UIShadowedView.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 02. 28..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

@IBDesignable class UIBorderedView: UIView {

    @IBInspectable var bottomColor: UIColor = UIColor.clear
    @IBInspectable var bottomWidth: CGFloat = 0
    
    override func draw(_ rect: CGRect) {
        let bottomBorder = CALayer()
        bottomBorder.backgroundColor = bottomColor.cgColor
        bottomBorder.frame = CGRect(x: 0, y: self.frame.size.height - bottomWidth, width: self.frame.size.width, height: bottomWidth)
        self.layer.addSublayer(bottomBorder)
    }
}
