//
//  UIRotateLabel.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 13..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

@IBDesignable class UIRotatedLabel: UILabel {
    
    @IBInspectable var angle:CGFloat = 0.0{
        didSet{
            layoutSubviews()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        transform = CGAffineTransform(rotationAngle: CGFloat.pi * angle / 180.0)
    }
}
