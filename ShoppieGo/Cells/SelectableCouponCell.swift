//
//  SelectableCouponCell.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 14..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

class SelectableCouponCell: UICollectionViewCell {
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var selectedImage: UIRoundedImageView!
    @IBOutlet weak var brandImage: UIImageView!
    @IBOutlet weak var discount: UILabel!
    @IBOutlet weak var validDate: UILabel!
}
