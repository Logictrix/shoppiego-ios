//
//  ChatPartnerTableViewCell.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 15..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

class ChatPartnerCell: UITableViewCell {

    @IBOutlet weak var messageLabel: UIRoundedLabel!
    @IBOutlet weak var avatarImage: UIRoundedImageView!

}
