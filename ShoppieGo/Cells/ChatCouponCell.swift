//
//  ChatCouponCell.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 14..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

class ChatCouponCell: UICollectionViewCell {
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var brandImage: UIImageView!
    @IBOutlet weak var discount: UILabel!
}
