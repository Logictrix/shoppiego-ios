//
//  RedeemedCouponCell.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 30..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

class RedeemedCouponSimpleCell: UITableViewCell {

    @IBOutlet weak var couponCode: UILabel!
    @IBOutlet weak var redeemDate: UILabel!    

}
