//
//  CouponCell.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 02. 28..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

class CouponCell: UICollectionViewCell {
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var alertView: UIRoundedView!
    @IBOutlet weak var countView: UIRoundedView!
    @IBOutlet weak var brandImage: UIImageView!
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var buttomImg: UIImageView!
    @IBOutlet weak var discount: UILabel!
    @IBOutlet weak var validDate: UILabel!
    @IBOutlet weak var count: UILabel!
}
