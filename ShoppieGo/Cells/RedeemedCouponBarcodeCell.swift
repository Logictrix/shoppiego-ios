//
//  RedeemedCouponBarcodeCell.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 04. 16..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

class RedeemedCouponBarcodeCell: UITableViewCell {

    @IBOutlet weak var couponCode: UILabel!
    @IBOutlet weak var redeemDate: UILabel!
    @IBOutlet weak var barcodeImage: UIImageView!

}
