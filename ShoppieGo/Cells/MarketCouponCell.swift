//
//  MarketCouponCell.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 07..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

class MarketCouponCell: UITableViewCell {

    @IBOutlet weak var brandImage: UIRoundedImageView!
    @IBOutlet weak var brandName: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var validLabel: UILabel!
    @IBOutlet weak var detailButton: UIRoundedButton!
    
}
