//
//  NotificationTableViewCell.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 07..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {

    @IBOutlet weak var notificationImage: UIRoundedImageView!
    @IBOutlet weak var notificationMessage: UILabel!
    @IBOutlet weak var notificationTime: UILabel!
    

}
