//
//  MeesageCell.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 16..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {
    
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var nicknameLabel: UILabel!
    @IBOutlet weak var newLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var datetimeLabel: UILabel!
    
}
