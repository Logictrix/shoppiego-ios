//
//  CharSystemCell.swift
//  ShoppieGo
//
//  Created by Kaizer Jeno Kornel on 2019. 04. 21..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import UIKit

class ChatSystemCell: UITableViewCell {
    
    @IBOutlet weak var messageLabel: UIRoundedLabel!
    
}
