//
//  CouponGoHelper.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 02. 19..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    /*static let URL_APP = "https://api.shoppiego.hu"
    static let URL_CHAT = "https://chat.shoppiego.hu"
    static let URL_CDN = "https://cdn.shoppiego.hu"*/
    //static let URL_APP = "https://testapi.shoppiego.hu"
    //static let URL_CHAT = "https://testchat.shoppiego.hu"
    static let URL_APP = "https://dev-api.shoppiego.hu"
    static let URL_CHAT = "https://dev-chat.shoppiego.hu"
    static let URL_CDN = "https://testcdn.shoppiego.hu"
    static let URL_HELPCENTER = "https://shoppiego.com"
    static let URL_REPORT_AN_ERROR = "mailto:mds@marquardmedia.hu"
    static let URL_ABOUT_US = "https://shoppiego.com"
 //   static let URL_TERMS = "https://shoppiego.com/adatkezelesi-tajekoztato.html"
    static let URL_TERMS = "https://shoppiego.com/terms"
    static let URL_PRIVACY = "https://shoppiego.com/privacy"
    static let MAX_DISTANCE = 30 // méterben
    static let MIN_ZOOM: Float = 14.0
    static let MAX_ZOOM: Float = 20.5
    static let DEFAULT_LAT = 47.33401369307872
    static let DEFAULT_LONG = 19.044256322103585
    static let DEFAULT_ZOOM = 17.0
    static let TILT = 45
    static let POPUP_ASYNC_AFTER_TIME = 200

    static let VC_SLIDER = "SliderViewController"
    static let VC_START = "StartViewController"
    static let VC_LOGIN = "LoginViewController"
    static let VC_REGISTRATION = "RegistrationViewController"
    static let VC_KEYSTORE = "KeyStoreViewController"
    static let VC_COUPON = "CouponViewController"
    static let VC_COUPON_REDEEM = "CouponRedeemViewController"
    static let VC_COUPON_OFFER = "CouponOfferViewController"
    static let VC_COUPON_SUCCESS = "CouponSuccessViewController"
    static let VC_PROFILE = "ProfileViewController"
    static let VC_SETTINGS = "SettingsViewController"
    static let VC_LANGUAGE = "LanguageViewController"
    static let VC_MAP_TOOFAR = "MapTooFarViewController"
    static let VC_MAP_WANT_IT = "MapWantItViewController"
    static let VC_MAP_SURE = "MapSureViewController"
    static let VC_MAP_ACQUIRE = "MapAcquireViewController"
    static let VC_CHAT = "ChatViewController"
    static let VC_CHAT_ITEMS = "ChatItemsViewController"
    static let VC_ALERT = "AlertViewController"
    static let VC_CHANGE_NAME = "ChangeNameViewController"
    //static let TEST_VC = "TestViewController"
    
    static let TB_MAIN = "MainTabBarController"
    static let TB_MAP = 0
    static let TB_COUPONS = 1
    static let TB_MARKET = 2
    static let TB_MESSAGES = 3
    static let TB_PROFILE = 4
    
    static let APP_MAX_DISTANCE = "app.max_distance"
    static let APP_MAX_ZOOM = "app.max_zoom"
    static let APP_MIN_ZOOM = "app.min_zoom"
    static let APP_DEFAULT_ZOOM = "app.default_zoom"
    static let APP_SOUNDEFFECT = "app.soundeffect"
    static let APP_VIBRATION = "app.vibration"
    static let APP_PUSHNOTIFICATION = "app.pushnotification"
    
    static let SESSION_SETTINGS_NOTIFICATIONS = "settings.category_notifications"
    static let SESSION_SETTINGS_LANGUAGE = "settings.language"
    static let SESSION_USER_ID = "user.id"
    static let SESSION_USER_NICKNAME = "user.nickname"
    static let SESSION_USER_EMAIL = "user.email"
    static let SESSION_USER_GENDER = "user.gender"
    static let SESSION_USER_BIRTHDAY = "user.birthday"
    static let SESSION_USER_PROFILE_PICTURE_URL = "user.profile_picture_url"
    static let SESSION_USER_ACCESS_TOKEN = "user.access_token"
    static let SESSION_USER_KEYCOUNT = "user.key_count"
    static let SESSION_FCM_TOKEN = "fcm.token"
    static let SESSION_LAST_LOCATION_DATE = "location.last_date"
    static let SESSION_LAST_LOCATION_LAT = "location.last_lat"
    static let SESSION_LAST_LOCATION_LNG = "location.last_lng"
    static let SESSION_AR_MODE = "ar_mode"

    static let TAG_NO_RESULTS = -999
    static let TAG_LOAD_ANIMATION = -1000
    static let TAG_CHAT_KEY_VIEW = -1001
    static let TAG_CHAT_COUPON_VIEW = -1002
    
    static let VALID_DAYS_FROM_NOW = 5
    static let PAGE_COUNT_MARKET = 5//20
    static let PAGE_COUNT_OWNCOUPONS = 12
    
    static let COLOR_NORMAL = 0xBD10E0
    static let COLOR_GOLD = 0xFFCB26
    
    static let SEARCH_DELAY = 0.5
}

class CouponGoHelper: NSObject {
    
    static let sharedInstance = CouponGoHelper()
    var commManager: CommManager!
    var locationManager: CLLocationManager!
    private var dateFormatter : DateFormatter!
    private var dateTimeFormatter : DateFormatter!
    private var dateTimeFormatterTZ : DateFormatter!
    
    public var mapUserLocation: CLLocation?
    public var launchParameters: Any? = nil
    //public var couponSelected: CGCoupon? = nil
    //public var couponFilterStr : String = ""
    /*public var languageSelected = "English"
    public var keyCount : Int = 4*/
    public var accessToken = ""
    private let updateGroup = DispatchGroup()
    
    override init(){
        super.init()
        
        commManager = CommManager()
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        dateTimeFormatter = DateFormatter()
        dateTimeFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.locale = Locale.init(identifier: "hu_HU")
        
        dateTimeFormatterTZ = DateFormatter()
        dateTimeFormatterTZ.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    }
    
    func getStoryboard(storyboardName: String) -> UIStoryboard{
        return UIStoryboard(name: storyboardName, bundle: nil)
    }
    
    func getMainStoryboard() -> UIStoryboard{
        return getStoryboard(storyboardName: "Main")
    }
    
    func getViewController(viewName: String) -> UIViewController?{
        var vc : UIViewController?
        
        if(Constants.VC_START == viewName){
            vc = getMainStoryboard().instantiateViewController(withIdentifier: viewName) as! StartViewController
        }
        else if(Constants.VC_SLIDER == viewName){
            vc = getMainStoryboard().instantiateViewController(withIdentifier: viewName) as! SliderViewController
        }
        else if(Constants.VC_LOGIN == viewName){
            vc = getMainStoryboard().instantiateViewController(withIdentifier: viewName) as! LoginViewController
        }
        else if(Constants.VC_REGISTRATION == viewName){
            vc = getMainStoryboard().instantiateViewController(withIdentifier: viewName) as! RegistrationViewController
        }
        else if Constants.VC_KEYSTORE == viewName{
            vc = getMainStoryboard().instantiateViewController(withIdentifier: viewName) as! KeyStoreViewController
        }
        else if Constants.VC_COUPON == viewName{
            vc = getMainStoryboard().instantiateViewController(withIdentifier: viewName) as! CouponViewController
        }
        else if Constants.VC_COUPON_REDEEM == viewName{
            vc = getMainStoryboard().instantiateViewController(withIdentifier: viewName) as! CouponRedeemViewController
        }
        else if Constants.VC_COUPON_OFFER == viewName{
            vc = getMainStoryboard().instantiateViewController(withIdentifier: viewName) as! CouponOfferViewController
        }
        else if Constants.VC_COUPON_SUCCESS == viewName{
            vc = getMainStoryboard().instantiateViewController(withIdentifier: viewName) as! CouponSuccessViewController
        }
        else if Constants.VC_PROFILE == viewName{
            vc = getMainStoryboard().instantiateViewController(withIdentifier: viewName) as! ProfileViewController
        }
        else if Constants.VC_SETTINGS == viewName{
            vc = getMainStoryboard().instantiateViewController(withIdentifier: viewName) as! SettingsViewController
        }
        else if Constants.VC_MAP_TOOFAR == viewName{
            vc = getMainStoryboard().instantiateViewController(withIdentifier: viewName) as! MapTooFarViewController
        }
        
        switch viewName {
        case Constants.VC_MAP_WANT_IT:
            vc = getMainStoryboard().instantiateViewController(withIdentifier: viewName) as! MapWantItViewController
            break
        case Constants.VC_MAP_SURE:
            vc = getMainStoryboard().instantiateViewController(withIdentifier: viewName) as! MapSureViewController
            break
        case Constants.VC_MAP_ACQUIRE:
            vc = getMainStoryboard().instantiateViewController(withIdentifier: viewName) as! MapAcquireViewController
            break
        case Constants.VC_CHAT:
            vc = getMainStoryboard().instantiateViewController(withIdentifier: viewName) as! ChatViewController
            break
        case Constants.VC_CHAT_ITEMS:
            vc = getMainStoryboard().instantiateViewController(withIdentifier: viewName) as! ChatItemsViewController
            break
        case Constants.VC_ALERT:
            vc = getMainStoryboard().instantiateViewController(withIdentifier: viewName) as! AlertViewController
            break
        case Constants.VC_CHANGE_NAME:
            vc = getMainStoryboard().instantiateViewController(withIdentifier: viewName) as! ChangeNameViewController
            break
        /*case Constants.TEST_VC:
            vc = getMainStoryboard().instantiateViewController(withIdentifier: viewName) as! TestViewController
            break*/
        default:
            break
        }
        
        return vc ?? nil
    }
    
    private func getAppdelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func showViewController(viewController: UIViewController){
        let appDelegate = getAppdelegate()
        appDelegate.window?.rootViewController = viewController
        appDelegate.window?.makeKeyAndVisible();
    }
    
    func showViewController(viewName: String){
        let appDelegate = getAppdelegate()
        
        let vc = getViewController(viewName: viewName)
        
        if vc != nil {
            appDelegate.window?.rootViewController = vc
        }
        appDelegate.window?.makeKeyAndVisible();
    }
    
    func presentViewController(parent: UIViewController, viewName: String, animated: Bool = false){
        let vc = getViewController(viewName: viewName)
        parent.present(vc!, animated: animated)
    }
    
    func showAlert(viewController: UIViewController, title: String = "", message: String, animated: Bool = false, isError: Bool = false){
        DispatchQueue.main.async {
            let vc = self.getViewController(viewName: Constants.VC_ALERT) as! AlertViewController
            vc.isError = isError
            vc.alertTitle = title
            vc.alertMessage = message
            
            viewController.present(vc, animated: animated)
        }
    }
    
    func showMainTabBar(selectedIdx: Int = 0){
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let vc = getMainStoryboard().instantiateViewController(withIdentifier: Constants.TB_MAIN) as! MainTabBarController
        vc.selectedIndex = selectedIdx
        
        appDelegate.window?.rootViewController = vc
        appDelegate.window?.makeKeyAndVisible();
    }
    
    func convertDateToString(_ date: Date?) -> String{
        return date == nil ? "" : dateFormatter.string(from: date!)
    }
    
    func convertStringToDate(string: String?) -> Date?{
        if (string ?? "").isEmpty {
            return nil
        }
        
        return dateFormatter.date(from: string!)
    }
    
    func convertDateTimeToString(datetime: Date?) -> String{
        return datetime == nil ? "" :  dateTimeFormatter.string(from: datetime!)
    }
    
    func convertStringToDateTime(string: String?) -> Date?{
        if (string ?? "").isEmpty {
            return nil
        }
        
        var d = dateTimeFormatter.date(from: string!)
        if d == nil {
            d = dateTimeFormatterTZ.date(from: string!)
        }
        
        return d
    }
    
    func getDaysFromNow(date: Date?) -> Int {
        if date == nil {
            return 0
        }
        
        let calendar = Calendar.current
        let date1 = calendar.startOfDay(for: date!)
        let date2 = calendar.startOfDay(for: Date())
        let components = calendar.dateComponents([.day], from: date2, to: date1)
        
        return components.day ?? 0
    }
    
    func getSecondsFromNow(date: Date?) -> Int {
        if date == nil {
            return 0
        }
        
        let calendar = Calendar.current
        let components = calendar.dateComponents([.second], from: Date(), to: date!)
        
        return components.second ?? 0
    }
    
    func getTime(date: Date) -> String{
        let calendar = Calendar.current
        let h = calendar.component(.hour, from: date)
        let m = calendar.component(.minute, from: date)
        
        return String(format: "%02d:%02d", h, m)
    }
    
    func getMonthsAndDays(date: Date) -> String{
        let calendar = Calendar.current
        let m = calendar.component(.month, from: date)
        let d = calendar.component(.day, from: date)
        
        return String(format: "%02d.%02d.", m, d)
    }
    
    func updateConfig(){
        let group = DispatchGroup()
        group.enter()
        
        CouponGoHelper.sharedInstance.commManager.request(method: "GET", url: "/config", queryData: nil, postData: nil) { (result, error) in
            if let result = result {
                DispatchQueue.global().async {
                    let dm = DataManager.sharedInstance
                    
                    let config = result.value(forKey: "config") as! NSDictionary
                    dm.setApp(Constants.APP_MAX_DISTANCE, value: CouponGoHelper.sharedInstance.jsonToInt(config.value(forKey: "maxDistanceFromCoupon")))
                    dm.setApp(Constants.APP_MAX_ZOOM, value: CouponGoHelper.sharedInstance.jsonToFloat(config.value(forKey: "maxZoom")))
                    dm.setApp(Constants.APP_MIN_ZOOM, value: CouponGoHelper.sharedInstance.jsonToFloat(config.value(forKey: "minZoom")))
                    dm.setApp(Constants.APP_DEFAULT_ZOOM, value: CouponGoHelper.sharedInstance.jsonToFloat(config.value(forKey: "defaultZoom")))
        
                    group.leave()
                }
            }
            else if error != nil {
                group.leave()
            }
        }
        
        group.wait()
    }
    
    func updateUser(_ obj: Any?){
            let dm = DataManager.sharedInstance
            
            let dict = obj as! NSDictionary
            let user = dict.value(forKey: "user") as! NSDictionary
            dm.setSession(Constants.SESSION_USER_ID, value: CouponGoHelper.sharedInstance.jsonToInt(user.value(forKey: "id")))
            dm.setSession(Constants.SESSION_USER_NICKNAME, value: CouponGoHelper.sharedInstance.jsonToString(user.value(forKey: "nickname")))
            dm.setSession(Constants.SESSION_USER_EMAIL, value: CouponGoHelper.sharedInstance.jsonToString(user.value(forKey: "email")))
            dm.setSession(Constants.SESSION_USER_GENDER, value: CouponGoHelper.sharedInstance.jsonToString(user.value(forKey: "gender")))
            dm.setSession(Constants.SESSION_USER_BIRTHDAY, value: CouponGoHelper.sharedInstance.jsonToString(user.value(forKey: "birthday")))
            dm.setSession(Constants.SESSION_USER_PROFILE_PICTURE_URL, value: CouponGoHelper.sharedInstance.jsonToString(user.value(forKey: "profile_picture_url")))
            dm.setSession(Constants.SESSION_USER_KEYCOUNT, value: CouponGoHelper.sharedInstance.jsonToInt(user.value(forKey: "key_count")))
            
            let settings = user.value(forKey: "settings") as! NSDictionary
            let notifications = settings.value(forKey: "category_notifications") as! NSArray
            dm.setSession(Constants.SESSION_SETTINGS_NOTIFICATIONS, value: notifications.map{ ($0 as! NSNumber).stringValue }.joined(separator : ","))
    }
    
    func updateUser(){
        let group = DispatchGroup()
        group.enter()
        
        CouponGoHelper.sharedInstance.commManager.request(method: "GET", url: "/user", queryData: nil, postData: nil) { (result, error) in
            if let result = result {
                DispatchQueue.global().async {
                    self.updateUser(result)
                    
                    group.leave()
                }
            }
            else if error != nil {
                group.leave()
            }
        }
        
        group.wait()
    }
    
    func updateCategory(){
        let group = DispatchGroup()
        group.enter()
        
        CouponGoHelper.sharedInstance.commManager.request(method: "GET", url: "/category", queryData: nil, postData: nil) { (result, error) in
            if let result = result {
                DispatchQueue.global().async {
                    do {
                        //let regex = try NSRegularExpression(pattern: "^category_*.png$")
                        let regex = try NSRegularExpression(pattern: "^*\\.png$")
                        let dir = self.getDocumentDir()
                        self.removeFiles(regEx: regex, path: dir!)
                    }
                    catch { }
                    
                    DataManager.sharedInstance.clearCategoryTable()
                    
                    let categories = result.value(forKey: "category_list") as! NSArray
                    for category in categories {
                        let obj = category as! NSDictionary
                        let id = CouponGoHelper.sharedInstance.jsonToInt(obj.value(forKey: "id"))
                        let title = CouponGoHelper.sharedInstance.jsonToString(obj.value(forKey: "title"))
                        let iconUrl = CouponGoHelper.sharedInstance.jsonToString(obj.value(forKey: "icon_url"))
                        
                        DataManager.sharedInstance.addCategory(id, title: title, iconUrl: iconUrl)
                    }
        
                    group.leave()
                }
            }
            else if error != nil {
                group.leave()
            }
        }
        
        group.wait()
    }
    
    func updateData(){
        updateConfig()
        updateUser()
        updateCategory()
    }
    
    private func getViewWithTag(parent: UIView, tag: Int) -> UIView?{
        for v in parent.subviews {
            if v.tag == tag {
                return v
            }
        }
        
        return nil
    }
    
    private func removeViewWithTag(parent: UIView, tag: Int){
        for v in parent.subviews {
            if v.tag == tag {
                v.removeFromSuperview()
                break
            }
        }
    }
    
    func addNoResults(parent: UIView, yOffset: CGFloat = 0.0, text: String, isVerticallyCentered: Bool = false, parentHeight: CGFloat = 0.0){
        if getViewWithTag(parent: parent, tag: Constants.TAG_NO_RESULTS) != nil {
            return
        }
        
        let lbl = UILabel(frame: CGRect(x: 5, y: yOffset, width: parent.frame.width - 5, height: 30))
        lbl.text = text
        lbl.font = UIFont.systemFont(ofSize: 12)
        lbl.textAlignment = .center
        lbl.textColor = .black
        lbl.numberOfLines = 0
        lbl.setCorrectLabelFrameHeight()

        let newY = (isVerticallyCentered ? (parentHeight - lbl.frame.height) / 2 : 0)
        let v = UIView(frame: CGRect(x: 0, y: newY, width: lbl.frame.width, height: lbl.frame.height))
        v.tag = Constants.TAG_NO_RESULTS
        v.addSubview(lbl)
        
        parent.addSubview(v)
    }
    
    func removeNoResults(parent: UIView){
        removeViewWithTag(parent: parent, tag: Constants.TAG_NO_RESULTS)
    }
    
    func addLoadAnimation(parent: UIView){
        let v = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        v.tag = Constants.TAG_LOAD_ANIMATION
        v.center = parent.center
        v.hidesWhenStopped = false
        v.startAnimating()
        parent.addSubview(v)
    }
    
    func removeLoadAnimation(parent: UIView){
            removeViewWithTag(parent: parent, tag: Constants.TAG_LOAD_ANIMATION)
    }
    
    func jsonToString(_ obj: Any?) -> String{
        return (obj as? String) ?? ""
    }
    
    func jsonToInt(_ obj: Any?) -> Int {
        if obj == nil || obj is NSNull {
            return 0
        }
        if obj is NSNumber {
            return (obj as! NSNumber).intValue
        }
        else{
            return Int(obj as! String)!
        }
    }
    
    func jsonToFloat(_ obj: Any?) -> Float {
        if obj == nil || obj is NSNull {
            return 0.0
        }
        else if obj is NSNumber {
            return (obj as! NSNumber).floatValue
        }
        else{
            return Float(obj as! String) ?? 0.0
        }
    }
    
    func jsonToDouble(_ obj: Any?) -> Double {
        if obj == nil || obj is NSNull {
            return 0.0
        }
        else if obj is NSNumber {
            return (obj as! NSNumber).doubleValue
        }
        else{
            return Double(obj as! String) ?? 0.0
        }
    }
    
    func jsonToDate(_ obj: Any?) -> Date? {
        if obj == nil || obj is NSNull {
            return nil
        }
        else if (obj as! String).isEmpty {
            return nil
        }
        else {
            return convertStringToDate(string: obj as? String)
        }
    }
    
    func jsonToDateTime(_ obj: Any?) -> Date? {
        if obj == nil || obj is NSNull {
            return nil
        }
        else if (obj as! String).isEmpty {
            return nil
        }
        else {
            return convertStringToDateTime(string: obj as? String)
        }
    }
    
    func jsonToBool(_ obj: Any?, value: String = "") -> Bool {
        var val = ""
        if obj == nil || obj is NSNull {
            return false
        }
        else if obj is NSNumber {
            val = (obj as! NSNumber).stringValue
        }
        else {
            val = (obj as? String) ?? value
        }
        return (value == "" ? (val == "true" || val == "1" || val == "yes") : (value == val))
    }
    
    func jsonToCoupon(_ obj: Any?) -> CGCoupon {
        let coupon = CGCoupon()
        
        if let o = obj as? NSDictionary {
            coupon.id = jsonToInt(o.value(forKey: "id"))
            coupon.campaignId = jsonToInt(o.value(forKey: "campaign_id"))
            coupon.brandName = jsonToString(o.value(forKey: "brand_name"))
            coupon.brandImageUrl = jsonToString(o.value(forKey: "brand_image_url"))
            coupon.requiredKeyCount = jsonToInt(o.value(forKey: "required_key_count"))
            coupon.openAt = jsonToDate(o.value(forKey: "open_at"))
            coupon.lat = jsonToDouble(o.value(forKey: "lat"))
            coupon.lng = jsonToDouble(o.value(forKey: "long"))
            coupon.count = jsonToInt(o.value(forKey: "count"))
            coupon.freeCount = jsonToInt(o.value(forKey: "free_count"))
            coupon.redeemedCount = jsonToInt(o.value(forKey: "redeemed_count"))
            coupon.title = jsonToString(o.value(forKey: "value"))
            coupon.descriptionUrl = jsonToString(o.value(forKey: "description_url"))
            coupon.coverImageurl = jsonToString(o.value(forKey: "cover_image_url"))
            if coupon.title.isEmpty {
                coupon.title = jsonToString(o.value(forKey: "title"))
            }
            coupon.isFixed = jsonToBool(o.value(forKey: "discount_type"), value: "fix")
            coupon.isUnlimited = jsonToBool(o.value(forKey: "is_unlimited_coupon"))
            coupon.description = jsonToString(o.value(forKey: "description"))
            coupon.webshop = jsonToString(o.value(forKey: "webshop"))
            coupon.validDate = jsonToDateTime(o.value(forKey: "valid_date"))
            
            coupon.redeemedItems.removeAll()
            if let couponList = o.value(forKey: "redeemed_coupons") as? NSArray {
                for item in couponList {
                    if let couponObj = item as? NSDictionary {
                        let redeemedCoupon = CGCouponRedeemedItem()
                        redeemedCoupon.id = jsonToInt(couponObj.value(forKey: "id"))
                        redeemedCoupon.code = jsonToString(couponObj.value(forKey: "code"))
                        redeemedCoupon.codeImageUrl = jsonToString(couponObj.value(forKey: "code_image_url"))
                        redeemedCoupon.date = jsonToDateTime(couponObj.value(forKey: "redeemed_at"))
                        
                        coupon.redeemedItems.append(redeemedCoupon)
                    }
                }
            }

            coupon.availableItems.removeAll()
            if let list = o.value(forKey: "list") as? NSArray {
                for listItem in list {
                    if let item = listItem as? NSDictionary {
                        let availItem = CGCouponAvailableItem()
                        availItem.userId = jsonToInt(item.value(forKey: "user_id"))
                        availItem.isFree = jsonToBool(item.value(forKey: "is_free"))
                        availItem.profilePictureUrl = jsonToString(item.value(forKey: "profile_picture_url"))
                        availItem.nickname = jsonToString(item.value(forKey: "nickname"))
                        
                        coupon.availableItems.append(availItem)
                    }
                }
            }
        }
        
        return coupon
    }
    
    func jsonToKey(_ obj: Any?) -> CGKey {
        let key = CGKey()
        
        let o = obj as! NSDictionary
        key.id = jsonToInt(o.value(forKey: "id"))
        key.lat = jsonToDouble(o.value(forKey: "lat"))
        key.lng = jsonToDouble(o.value(forKey: "long"))
        key.count = jsonToInt(o.value(forKey: "count"))
        
        return key
    }
    
    func getDocumentDir() -> URL? {
        let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        return dir
    }
    
    func isFileExists(_ filename: String?) -> Bool {
        guard let fn = filename else { return false }
        let fileManager: FileManager = FileManager.default
        return fileManager.fileExists(atPath: fn)
    }
    
    func deleteFile(_ filename: String?) {
        guard let fn = filename else { return }
        let fileManager: FileManager = FileManager.default
        if fileManager.isDeletableFile(atPath: fn) {
            do {
                try fileManager.removeItem(atPath: fn)
            } catch _ {
                
            }
        }
    }
    
    func removeFiles(regEx:NSRegularExpression, path:URL) {
        let fileManager: FileManager = FileManager.default
        let filesEnumerator = fileManager.enumerator(at: path, includingPropertiesForKeys: nil)!
        for case let fileURL as URL in filesEnumerator {
            let file = fileURL.absoluteString
            let match = regEx.numberOfMatches(in: file, range: NSMakeRange(0, file.count))
            if match > 0 {
                do {
                    try fileManager.removeItem(at: fileURL)
                }
                catch _ {
                    
                }
            }
        }
    }
    
    func delayWithMilliseconds(_ milliseconds: Int, completion: @escaping () -> ()) {
        DispatchQueue.global().asyncAfter(deadline: .now() + .milliseconds(milliseconds)) {
            completion()
        }
    }
    
    func sendFCMToken(_ fcmToken: String){
        let p : [String : String] = ["token": fcmToken,
                                     "is_ios": "true"]
        commManager.request(method: "POST", url: "/user/firebase", queryData: nil, postData: p) { (result, error) in
            if result != nil {
                
            }
            else if error != nil {
                
            }
        }
    }
    
    func getDistance(from: CLLocationCoordinate2D, to: CLLocationCoordinate2D) -> CLLocationDistance{
        let from = CLLocation(latitude: from.latitude, longitude: from.longitude)
        let to = CLLocation(latitude: to.latitude, longitude: to.longitude)
        return from.distance(from: to)
    }
    
    func getTradeItems(_ json: Any?) -> CGTradeItems? {
        guard let dict = json as? NSDictionary else { return nil }
        
        let tradeItems = CGTradeItems()
        let coupons = dict.value(forKey: "coupons") as! NSArray
        for coupon in coupons {
            let obj = coupon as! NSDictionary
            let c = CGCoupon()
            c.id = CouponGoHelper.sharedInstance.jsonToInt(obj.value(forKey: "id"))
            c.isFixed = CouponGoHelper.sharedInstance.jsonToBool(obj.value(forKey: "discount_type"), value: "fix")
            c.title = CouponGoHelper.sharedInstance.jsonToString(obj.value(forKey: "discount"))
            c.brandImageUrl = CouponGoHelper.sharedInstance.jsonToString(obj.value(forKey: "brand_image_url"))
            c.coverImageurl = CouponGoHelper.sharedInstance.jsonToString(obj.value(forKey: "cover_image_url"))
            
            tradeItems.coupons.append(c)
        }
        tradeItems.keyCount = CouponGoHelper.sharedInstance.jsonToInt(dict.value(forKey: "keys"))
        tradeItems.isAccepted = CouponGoHelper.sharedInstance.jsonToBool(dict.value(forKey: "accepted"))
        
        return tradeItems
    }
    
    func showCouponDetails(viewController: UIViewController, coupon: CGCoupon, url: String){
        CouponGoHelper.sharedInstance.commManager.request(method: "GET", url: url, queryData: nil, postData: nil) { (result, error) in
            if let result = result {
                let dict = result as NSDictionary
                let newCoupon = CouponGoHelper.sharedInstance.jsonToCoupon(dict.value(forKey: "coupon"))
                
                DispatchQueue.main.async {
                    let vc = CouponGoHelper.sharedInstance.getViewController(viewName: Constants.VC_COUPON) as! CouponViewController
                    vc.delegate = viewController as? CouponCallbackDelegate
                    vc.coupon = newCoupon
                    viewController.present(vc, animated: true, completion: nil)
                }
            }
            else if error != nil {
                
            }
        }
    }
    
    func downloadAndCacheImage(link: String, completion: @escaping (UIImage) -> Void) {
        let lnk = link.replacingOccurrences(of: ".svg", with: ".png")
        if let url = URL(string: lnk) {
            var fn = url.lastPathComponent
            let lnkComponents = lnk.components(separatedBy: "/")
            if lnkComponents.count > 1 {
                fn = lnkComponents[lnkComponents.count - 2] + "_" + fn
            }
            let fileManager : FileManager = FileManager.default
            let dir = try? fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            var fileName: String? = nil
            if dir != nil {
                fileName = dir!.appendingPathComponent(fn).path
                if(fileManager.fileExists(atPath: fileName!)){
                    completion(UIImage(contentsOfFile: fileName!)!)
                }
            }
            
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                guard
                    let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                    let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                    let data = data, error == nil,
                    let image = UIImage(data: data)
                    else { return }
                do {
                    
                    if fileName != nil {
                        if let pngData = image.pngData() {
                            DispatchQueue.main.async() {
                                do {
                                    try pngData.write(to: URL(fileURLWithPath: fileName!))
                                }
                                catch { }
                            }
                        }
                        completion(image)
                    }
                }
                }.resume()
        }
    }
    
    func getImageFromCache(link: String) -> UIImage {
        let lnk = link.replacingOccurrences(of: ".svg", with: ".png")
        if let url = URL(string: lnk) {
            var fn = url.lastPathComponent
            let lnkComponents = lnk.components(separatedBy: "/")
            if lnkComponents.count > 1 {
                fn = lnkComponents[lnkComponents.count - 2] + "_" + fn
            }
            let fileManager : FileManager = FileManager.default
            let dir = try? fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            var fileName: String? = nil
            if dir != nil {
                fileName = dir!.appendingPathComponent(fn).path
                if(fileManager.fileExists(atPath: fileName!)){
                    return UIImage(contentsOfFile: fileName!)!
                }
            }
        }
        
        return UIImage()
    }
}
