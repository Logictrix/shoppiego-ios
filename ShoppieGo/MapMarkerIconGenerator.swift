//
//  MapMarkerIconGenerator.swift
//  ShoppieGo
//
//  Created by Kaizer Jeno Kornel on 2019. 05. 24..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import Foundation

class MapMarkerIconGenerator {
    public static func getCouponIcon(backgroundImage: UIImage, brandImage: UIImage, brandRect: CGRect, labelRect: CGRect? = nil, labelText: String? = nil, labelColor: UIColor? = nil) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(backgroundImage.size, false, 0.0)
        
        backgroundImage.draw(in: CGRect(x: 0, y: 0, width: backgroundImage.size.width, height: backgroundImage.size.height))
        brandImage.draw(in: brandRect)
        
        if(labelRect != nil && labelText != nil && labelColor != nil){
            let font = UIFont.systemFont(ofSize: 10, weight: .bold)
            let textStyle = NSMutableParagraphStyle()
            textStyle.alignment = NSTextAlignment.center
            let attributes=[
                NSAttributedString.Key.font: font,
                NSAttributedString.Key.paragraphStyle: textStyle,
                NSAttributedString.Key.foregroundColor: labelColor]
            
            labelText!.draw(in: labelRect!.integral, withAttributes: attributes as [NSAttributedString.Key : Any])
        }
        
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result!
    }
    
    public static func getKeyIcon(image: UIImage, text: String) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(image.size, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height))
        
        let font = UIFont.systemFont(ofSize: 10, weight: .bold)
        let textStyle = NSMutableParagraphStyle()
        textStyle.alignment = NSTextAlignment.center
        let textColor = UIColor.black
        let attributes=[
            NSAttributedString.Key.font: font,
            NSAttributedString.Key.paragraphStyle: textStyle,
            NSAttributedString.Key.foregroundColor: textColor]
        
        let textRect = CGRect(x: 0, y: 11, width: 33, height: 10)
        text.draw(in: textRect.integral, withAttributes: attributes as [NSAttributedString.Key : Any])
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result!
    }
}
