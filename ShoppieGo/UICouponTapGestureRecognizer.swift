//
//  UICouponTapGestureRecognizer.swift
//  ShoppieGo
//
//  Created by Kaizer Jeno Kornel on 2019. 04. 21..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import Foundation

class UICouponTapGestureRecognizer: UITapGestureRecognizer{
    var coupon: CGCoupon? = nil
    var isOwn = false
}
