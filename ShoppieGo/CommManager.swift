//
//  CommManager.swift
//  CouponGo
//
//  Created by Kaizer Jeno Kornel on 2019. 03. 16..
//  Copyright © 2019. Hov-9 Bt. All rights reserved.
//

import Foundation

class CommManager{
    
    private func getBodyString(params: [String: String]) -> String{
        return params.map{(key, value) in return key + "=" + value}.joined(separator : "&")
    }
    
    private func getQueryString(params: [String: String]) -> String{
        return "?" + params.map{(key, value) in return key + "=" + value}.joined(separator : "&")
    }
    
    func request(method: String, url: String, queryData: [String: String]?, postData: [String: String]?, delayInMilliseconds: Int = 0, completion: @escaping (NSDictionary?, Error?) -> Void){
        // create post request
        var baseUrl = Constants.URL_APP
        if url.hasPrefix("/list") || url.hasPrefix("/message") {
            baseUrl = Constants.URL_CHAT
        }
        let str = baseUrl + url + (method == "GET" && queryData != nil ? getQueryString(params: queryData!) : "")
        
        let url = URL(string: str)!
        print("request_url ==> \(url)")
        print("request_postData ==> \(String(describing: postData))")
        var request = URLRequest(url: url)
        
        if method == "POST" || method == "PUT"{
            request.httpMethod = method
            if postData != nil {
                request.httpBody = getBodyString(params: postData!).data(using: .utf8)
            }
        }
        
        if !(CouponGoHelper.sharedInstance.accessToken).isEmpty {
            request.addValue("Bearer " + CouponGoHelper.sharedInstance.accessToken, forHTTPHeaderField: "Authorization")
        }
        if var lngCode = Locale.current.languageCode {
            if "hu" != lngCode && "pl" != lngCode {
                lngCode = "en"
            }
            request.addValue(lngCode, forHTTPHeaderField: "Accept-Language")
        }
        
        print("request>>\(request)")
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                completion(nil, error)
                return
            }
            
            guard let data = data else {
                completion(nil, NSError(domain: "dataNilError", code: -100001, userInfo: nil))
                return
            }
            
            do {
                //create json object from data
                guard let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary else {
                //guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) else {
                    completion(nil, NSError(domain: "invalidJSONTypeError", code: -100009, userInfo: nil))
                    return
                }
                
                if json.value(forKey: "success") as! Bool {
                    completion(json, nil)
                }
                else{
                    //completion(nil, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : json["error"] as! String]))
                    completion(nil, NSError(domain: "", code: 0, userInfo: ["error" : CouponGoHelper.sharedInstance.jsonToString(json["error"]), "code": CouponGoHelper.sharedInstance.jsonToInt(json["code"])]))
                }
            } catch let error {
                completion(nil, error)
            }
        }
        
        /*CouponGoHelper.sharedInstance.delayWithMilliseconds(delayInMilliseconds) {
            task.resume()
        }*/
        task.resume()
    }
    
    
    func requestWithPUT(method: String, url: String, queryData: [String: String]?, postData: [String: String]?, delayInMilliseconds: Int = 0, completion: @escaping (NSDictionary?, Error?) -> Void){
        // create post request
        var baseUrl = Constants.URL_APP
        if url.hasPrefix("/list") || url.hasPrefix("/message") {
            baseUrl = Constants.URL_CHAT
        }
        var str = baseUrl + url + (method == "GET" && queryData != nil ? getQueryString(params: queryData!) : "")
        
      //  https://dev-api.shoppiego.hu/coupon/5/market/get-it-free?user_id=-1
        if postData != nil {
            let id = postData!["user_id"]
            str.append("?user_id=\(String(describing: id!))")
        }
        
        let url = URL(string: str)!
        print("request_url ==> \(url)")
        print("request_postData ==> \(String(describing: postData))")
        var request = URLRequest(url: url)
        
        if method == "POST" || method == "PUT"{
            request.httpMethod = method
        }
        
        if !(CouponGoHelper.sharedInstance.accessToken).isEmpty {
            request.addValue("Bearer " + CouponGoHelper.sharedInstance.accessToken, forHTTPHeaderField: "Authorization")
        }
        if var lngCode = Locale.current.languageCode {
            if "hu" != lngCode && "pl" != lngCode {
                lngCode = "en"
            }
            request.addValue(lngCode, forHTTPHeaderField: "Accept-Language")
        }
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                completion(nil, error)
                return
            }
            
            guard let data = data else {
                completion(nil, NSError(domain: "dataNilError", code: -100001, userInfo: nil))
                return
            }
            
            do {
                //create json object from data
                guard let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary else {
                    //guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) else {
                    completion(nil, NSError(domain: "invalidJSONTypeError", code: -100009, userInfo: nil))
                    return
                }
                
                if json.value(forKey: "success") as! Bool {
                    completion(json, nil)
                }
                else{
                    //completion(nil, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : json["error"] as! String]))
                    completion(nil, NSError(domain: "", code: 0, userInfo: ["error" : CouponGoHelper.sharedInstance.jsonToString(json["error"]), "code": CouponGoHelper.sharedInstance.jsonToInt(json["code"])]))
                }
            } catch let error {
                completion(nil, error)
            }
        }
        
        /*CouponGoHelper.sharedInstance.delayWithMilliseconds(delayInMilliseconds) {
         task.resume()
         }*/
        task.resume()
    }
    
    func uploadImage() {
        // TODO
    }
    
}
